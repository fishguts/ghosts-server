"""
A management command which deletes expired accounts (e.g.,
accounts which signed up but never activated) from the database.

Calls ``RegistrationProfile.objects.delete_expired_users()``, which
contains the actual logic for determining which accounts are deleted.

"""
from datetime import datetime
from optparse import make_option
from django.core.management.base import NoArgsCommand
from registration.models import RegistrationProfile


class Command(NoArgsCommand):
    # noinspection PyShadowingBuiltins
    help = 'Delete expired user registrations from the database'
    option_list = NoArgsCommand.option_list + (
        # being consistent with core commands
        make_option('-n', '--dry-run',
            action='store_true', dest='dry_run', default=False,
            help="Report only. No DB modifications."),
        )

    ''' Command overrides '''
    def handle_noargs(self, **options):
        self._set_options(**options)
        self._delete_expired()


    ''' Private interface '''
    def _set_options(self, **options):
        self.verbosity=int(options.get('verbosity', 1))
        self.dry_run=bool(options.get('dry_run', False))

    def _delete_expired(self):
        expired=RegistrationProfile.objects.get_expired_profiles()
        if self.verbosity>1:
            date=datetime.utcnow()
            self.stdout.write('{0} registration profiles expired as of {1}.\n'.format(expired.count(), date))

        if self.verbosity>2:
            for profile in expired:
                self.stdout.write('username="{0}", joined="{1}"\n'.format(profile.user.username, profile.user.date_joined))

        if not self.dry_run:
            if self.verbosity>1:
                self.stdout.write('deleting {0} profiles.\n'.format(expired.count()))
            RegistrationProfile.objects.delete_expired_profiles(expired)
