from django.contrib.auth.models import User
from django.forms import Field, EmailField
from django.test import TestCase
from ghosts.models import Reserved

from registration import forms


class RegistrationFormTests(TestCase):
    """
    Test the default registration forms.

    """

    def test_registration_form(self):
        """
        Test that ``RegistrationForm`` enforces username constraints
        and matching passwords.

        """
        # Create a user so we can verify that duplicate usernames aren't permitted.
        User.objects.create_user('alice', 'alice@example.com', 'secret')
        Reserved.objects.create(word='naughty')

        invalid_objects=[
            # White space user name
            {'data':{'username':'',
                     'email':'foo@example.com',
                     'password':'foo'},
             'error':('username', [Field.default_error_messages['required']])
            },
            # Empty email address
            {'data':{'username':'jolly12',
                     'email':'',
                     'password':'foo'},
             'error':('email', [Field.default_error_messages['required']])
            },
            # Non-alphanumeric username.
            {'data':{'username':'foo/bar',
                     'email':'fail@domain.com',
                     'password':'foo'},
             'error':('username', [u"A username may be a combination of letters, numbers and .@+-_."])
            },
            # Invalid email address
            {'data':{'username':'jolly12',
                     'email':'bad.com',
                     'password':'foo'},
             'error':('email', [EmailField.default_error_messages['invalid']])
            },
            # Already-existing username.
            {'data':{'username':'alice',
                     'email':'fail@domain.com',
                     'password':'secret'},
             'error':('username', [u"A user with that username already exists."])
            },
            # Naughty #1
            {'data':{'username':'naughty',
                     'email':'fail@domain.com',
                     'password':'secret'},
             'error':('username', [u"Username is reserved."])
            },
            {'data':{'username':'naughty_',
                     'email':'fail@domain.com',
                     'password':'foo'},
             'error':('username', [u"Username is reserved."])
            },
            {'data':{'username':'_naughty',
                     'email':'fail@domain.com',
                     'password':'foo'},
             'error':('username', [u"Username is reserved."])
            },
            {'data':{'username':'_naughty_',
                     'email':'fail@domain.com',
                     'password':'foo'},
             'error':('username', [u"Username is reserved."])
            },
            {'data':{'username':'-naughty-',
                     'email':'fail@domain.com',
                     'password':'foo'},
             'error':('username', [u"Username is reserved."])
            },
            {'data':{'username':'+naughty+',
                     'email':'fail@domain.com',
                     'password':'foo'},
             'error':('username', [u"Username is reserved."])
            },
            {'data':{'username':'@naughty@',
                     'email':'fail@domain.com',
                     'password':'foo'},
             'error':('username', [u"Username is reserved."])
            },
            {'data':{'username':'.naughty.',
                     'email':'fail@domain.com',
                     'password':'foo'},
             'error':('username', [u"Username is reserved."])
            },
        ]

        valid_objects=[
            {'username':'fool',
             'email':'success1@example.com',
             'password':'foo'
            },
            {'username':'unnaughty',
             'email':'success2@example.com',
             'password':'foo'
            },
            {'username':'naughtys',
             'email':'success3@example.com',
             'password':'foo'
            },
            {'username':'naught',
             'email':'success4@example.com',
             'password':'foo'
            },
        ]

        for invalid_object in invalid_objects:
            form=forms.RegistrationForm(data=invalid_object['data'])
            self.assertFalse(form.is_valid())
            self.assertEqual(len(form.errors), 1)
            self.assertEqual(form.errors[invalid_object['error'][0]], invalid_object['error'][1])

        for valid_object in valid_objects:
            form=forms.RegistrationForm(data=valid_object)
            self.assertTrue(form.is_valid())


    def test_registration_form_tos(self):
        """
        Test that ``RegistrationFormTermsOfService`` requires
        agreement to the terms of service.

        """
        form=forms.RegistrationFormTermsOfService(data={'username':'fool',
                                                        'email':'foo@example.com',
                                                        'password':'fool'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['tos'], [u"You must agree to the terms to register"])

        form=forms.RegistrationFormTermsOfService(data={'username':'fool',
                                                        'email':'fool@example.com',
                                                        'password':'fool',
                                                        'tos':'on'})
        self.assertTrue(form.is_valid())


    def test_registration_form_unique_email(self):
        """
        Test that ``RegistrationFormUniqueEmail`` validates uniqueness
        of email addresses.

        """
        # Create a user so we can verify that duplicate addresses
        # aren't permitted.
        User.objects.create_user('alice', 'alice@example.com', 'secret')

        form=forms.RegistrationForm(data={'username':'fool',
                                                     'email':'alice@example.com',
                                                     'password':'fool'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['email'],
                         [u"This email address is already in use. Please supply a different email address."])

        form=forms.RegistrationForm(data={'username':'fool',
                                                     'email':'fool@example.com',
                                                     'password':'fool'})
        self.assertTrue(form.is_valid())

