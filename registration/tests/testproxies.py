import datetime

from django.conf import settings
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.sites.models import Site
from django.core import mail
from django.core.handlers.wsgi import WSGIRequest
from django.test import Client
from django.test import TestCase
from common.exceptions import StatusException

from registration import RegistrationProxy
from registration.admin import RegistrationAdmin
from registration.models import RegistrationProfile


class _MockRequestClient(Client):
    """
    A ``django.test.Client`` subclass which can return mock
    ``HttpRequest`` objects.
    ~
    """


    def request(self, **data):
        """
        Rather than issuing a request and returning the response, this
        simply constructs an ``HttpRequest`` object and returns it.

        """
        environ={
            'HTTP_COOKIE':self.cookies,
            'PATH_INFO':'/',
            'QUERY_STRING':'',
            'REMOTE_ADDR':'127.0.0.1',
            'REQUEST_METHOD':'GET',
            'SCRIPT_NAME':'',
            'SERVER_NAME':'testserver',
            'SERVER_PORT':'80',
            'SERVER_PROTOCOL':'HTTP/1.1',
            'wsgi.version':(1, 0),
            'wsgi.url_scheme':'http',
            'wsgi.errors':self.errors,
            'wsgi.multiprocess':True,
            'wsgi.multithread':False,
            'wsgi.run_once':False,
            'wsgi.input':None,
        }
        environ.update(self.defaults)
        environ.update(data)
        request=WSGIRequest(environ)

        # WSGIRequest sets post and get to {}
        if 'POST' in data:
            request.POST=data['POST']

        # We have to manually add a session since we'll be bypassing
        # the middleware chain.
        session_middleware=SessionMiddleware()
        session_middleware.process_request(request)
        return request


def _mock_request(**request):
    """
    Construct and return a mock ``HttpRequest`` object; this is used
    in testing backend methods which expect an ``HttpRequest`` but
    which are not being called from views.

    """
    return _MockRequestClient().request(**request)


class RegistrationProxyTests(TestCase):
    """
    Test the default registration backend.

    Running these tests successfully will require two templates to be
    created for the sending of activation emails; details on these
    templates and their contexts may be found in the documentation for
    the default backend.

    """
    POST_DATA_BILL={
        'username':'bill',
        'email':'bill@example.com',
        'password':'secret'
    }
    POST_DATA_ALICE={
        'username':'alice',
        'email':'alice@example.com',
        'password':'swordfish'
    }


    def setUp(self):
        """
        Create an instance of the default backend for use in testing,
        and set ``REGISTRATION_ACTIVATION_DAYS`` if it's not set already.

        """
        self.old_activation=getattr(settings, 'REGISTRATION_ACTIVATION_DAYS', None)
        if self.old_activation is None:
            settings.REGISTRATION_ACTIVATION_DAYS=7 # pragma: no cover


    def tearDown(self):
        """
        Yank out ``REGISTRATION_ACTIVATION_DAYS`` back out if it wasn't
        originally set.

        """
        if self.old_activation is None:
            settings.REGISTRATION_ACTIVATION_DAYS=self.old_activation # pragma: no cover


    def test_registration(self):
        """
        Test the registration process: registration creates a new
        inactive account and a new profile with activation key,
        populates the correct account data and sends an activation
        email.

        """
        new_user=RegistrationProxy.register(_mock_request(POST=self.POST_DATA_BILL))

        # Details of the returned user must match what went in.
        self.assertEqual(new_user.username, 'bill')
        self.assertTrue(new_user.check_password('secret'))
        self.assertEqual(new_user.email, 'bill@example.com')

        # New user must not be active.
        self.assertFalse(new_user.is_active)

        # A registration profile was created, and an activation email
        # was sent.
        self.assertEqual(RegistrationProfile.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)


    def test_registration_no_sites(self):
        """
        Test that registration still functions properly when
        ``django.contrib.sites`` is not installed; the fallback will
        be a ``RequestSite`` instance.

        """
        Site._meta.installed=False
        new_user=RegistrationProxy.register(_mock_request(POST=self.POST_DATA_BILL))

        self.assertEqual(new_user.username, 'bill')
        self.assertTrue(new_user.check_password('secret'))
        self.assertEqual(new_user.email, 'bill@example.com')

        self.assertFalse(new_user.is_active)

        self.assertEqual(RegistrationProfile.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)

        Site._meta.installed=True


    def test_valid_activation(self):
        """
        Test the activation process: activating within the permitted
        window sets the account's ``is_active`` field to ``True`` and
        resets the activation key.

        """
        valid_user=RegistrationProxy.register(_mock_request(POST=self.POST_DATA_ALICE))
        valid_profile=RegistrationProfile.objects.get(user=valid_user)
        activated=RegistrationProxy.activate(_mock_request(), valid_profile.activation_key)
        self.assertEqual(activated.username, valid_user.username)
        self.assertTrue(activated.is_active)

        # Fetch the profile again to verify its activation key has
        self.assertRaises(Exception, RegistrationProfile.objects.get, user=valid_user)


    def test_invalid_activation(self):
        """
        Test the activation process: trying to activate outside the
        permitted window fails, and leaves the account inactive.

        """
        expired_user=RegistrationProxy.register(_mock_request(POST=self.POST_DATA_BILL))
        expired_user.date_joined=expired_user.date_joined-datetime.timedelta(days=settings.REGISTRATION_ACTIVATION_DAYS)
        expired_user.save()
        expired_profile=RegistrationProfile.objects.get(user=expired_user)
        self.assertRaises(StatusException, RegistrationProxy.activate, _mock_request(), activation_key=expired_profile.activation_key)
        self.assertTrue(expired_profile.activation_expired())


    def test_allow(self):
        """
        Test that the setting ``REGISTRATION_OPEN`` appropriately
        controls whether registration is permitted.

        """
        old_allowed=getattr(settings, 'REGISTRATION_OPEN', True)
        try:
            settings.REGISTRATION_OPEN=True
            self.assertTrue(RegistrationProxy.registration_allowed(_mock_request()))

            settings.REGISTRATION_OPEN=False
            self.assertFalse(RegistrationProxy.registration_allowed(_mock_request()))
        finally:
            settings.REGISTRATION_OPEN=old_allowed


    def test_email_send_action(self):
        """
        Test re-sending of activation emails via admin action.

        """
        admin_class=RegistrationAdmin(RegistrationProfile, admin.site)

        alice=RegistrationProxy.register(_mock_request(POST=self.POST_DATA_ALICE))
        admin_class.resend_activation_email(_mock_request(), RegistrationProfile.objects.all())
        self.assertEqual(len(mail.outbox), 2) # One on registering, one more on the resend.


    def test_email_send_action_no_sites(self):
        """
        Test re-sending of activation emails via admin action when
        ``django.contrib.sites`` is not installed; the fallback will
        be a ``RequestSite`` instance.

        """
        Site._meta.installed=False
        admin_class=RegistrationAdmin(RegistrationProfile, admin.site)

        try:
            alice=RegistrationProxy.register(_mock_request(POST=self.POST_DATA_ALICE))
            admin_class.resend_activation_email(_mock_request(), RegistrationProfile.objects.all())
            self.assertEqual(len(mail.outbox), 2) # One on registering, one more on the resend.
        finally:
            Site._meta.installed=True


    def test_activation_action(self):
        """
        Test manual activation of users view admin action.

        """
        admin_class=RegistrationAdmin(RegistrationProfile, admin.site)

        alice=RegistrationProxy.register(_mock_request(POST=self.POST_DATA_ALICE))
        admin_class.activate_users(_mock_request(), RegistrationProfile.objects.all())
        self.assertTrue(User.objects.get(username='alice').is_active)

