"""
Created: 11/3/12
@author: curt
"""

from django.utils import unittest
from registration.tests.testforms import RegistrationFormTests
from registration.tests.testmodels import RegistrationModelTests
from registration.tests.testproxies import RegistrationProxyTests
from registration.tests.testviews import RegistrationViewTests


def suite():
    """
    Queue up all tests in our suite.  Our motivation for building suite manually:
        - django test runner is very adamant about where test cases live and tests folder isn't one of them.
        - python unit test runner causes test to be run over our database. Django does a syncdb before each test which reverts back to the beginning of time.
    """
    suite=unittest.TestSuite()
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(RegistrationFormTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(RegistrationModelTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(RegistrationProxyTests))
    # 11/2012 - turning these guys off for now - our flow is a lot simpler
    # suite.addTest(unittest.TestLoader().loadTestsFromTestCase(RegistrationViewTests))
    return suite
