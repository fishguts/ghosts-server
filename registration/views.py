"""
Views which allow users to create and activate accounts.

"""
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from registration import RegistrationProxy
from registration.forms import RegistrationForm


def activate(request, **kwargs):
    """
    Activate a user's account.

    **Arguments**

    ``\*\*kwargs``
        Any keyword arguments captured from the URL, such as an
        activation key, which will be passed to the proxy's
        ``activate()`` method.

    **Context:**

    The context will be populated from the keyword arguments captured
    in the URL

    """
    try:
        RegistrationProxy.activate(request, **kwargs)
        return redirect('registration_activation_complete')
    except:
        # core will have logged it.  Fall through to failed redirect
        pass
    context=RequestContext(request)
    return render_to_response('registration/activation_failed.html', kwargs, context_instance=context)


def register(request):
    """
    Allow a new user to register an account.

    """
    if not RegistrationProxy.registration_allowed(request):
        return redirect('registration_disallowed')

    if request.method=='POST':
        data=request.POST
    else:
        data=request.GET

    form=RegistrationForm(data=data, files=request.FILES)
    try:
        RegistrationProxy.register(request=request, form=form, data=data)
        return redirect('registration_complete')
    except:
        pass

    context=RequestContext(request)
    return render_to_response('registration/registration_form.html', {'form':form}, context_instance=context)

