import datetime
import hashlib
import random
import re
import logging

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from common.decorators import methodBooleanPropertyTrue
from common.utils import StringXT
from ghosts import contact

SHA1_RE = re.compile('^[a-f0-9]{40}$')


class RegistrationManager(models.Manager):
    """
    Custom manager for the ``RegistrationProfile`` model.

    The methods defined here provide shortcuts for account creation
    and activation (including generation and emailing of activation
    keys), and for cleaning out expired inactive accounts.

    """
    def user_to_registration(self, user):
        try:
            return self.get(user=user)
        except:
            return None


    @transaction.commit_on_success
    def create_inactive_user(self, username, email, password, site, send_email=True):
        """
        Create a new, inactive ``User``, generate a
        ``RegistrationProfile`` and email its activation key to the
        ``User``, returning the new ``User``.

        By default, an activation email will be sent to the new
        user. To disable this, pass ``send_email=False``.

        """
        new_user = User.objects.create_user(username, email, password)
        new_user.is_active = False
        new_user.save()

        registration_profile = self.create_profile(new_user)

        if send_email:
            registration_profile.send_activation_email(site)

        return new_user


    @transaction.commit_on_success
    def activate_user(self, activation_key, site, send_email=True, override_expiration=False):
        """
        Validate an activation key and activate the corresponding
        ``User`` if valid.

        If the key is valid and has not expired, return the ``User``
        after activating.

        If the key is not valid or has expired, return ``False``.

        If the key is valid but the ``User`` is already active,
        return ``False``.
        """
        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
        if not SHA1_RE.search(activation_key):
            # note: see whether
            logging.getLogger(__name__).warn("Invalid activation key: key={0:.40} len={1}".format(StringXT.safe(activation_key, '[null]'), StringXT.safe_len(activation_key)))
            raise Exception('Invalid activation key')

        try:
            registration_profile = self.get(activation_key=activation_key)
        except self.model.DoesNotExist:
            logging.getLogger(__name__).warn("Profile does not exist: key={0}".format(activation_key))
            raise Exception('Profile does not exist')

        if not override_expiration and registration_profile.activation_expired():
            raise Exception('Activation expired')

        registered_user = registration_profile.user
        registered_user.is_active = True
        registered_user.save()

        if send_email:
            try:
                registration_profile.send_activated_email(site)
            except:
                # it's been logged. Let the rest of registration complete normally.
                pass

        registration_profile.delete()
        return registered_user


    def create_profile(self, user):
        """
        Create a ``RegistrationProfile`` for a given
        ``User``, and return the ``RegistrationProfile``.

        The activation key for the ``RegistrationProfile`` will be a
        SHA1 hash, generated from a combination of the ``User``'s
        username and a random salt.

        """
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        username = user.username
        if isinstance(username, unicode):
            username = username.encode('utf-8')
        activation_key = hashlib.sha1(salt+username).hexdigest()
        return self.create(user=user, activation_key=activation_key)


    @transaction.commit_on_success
    def delete_profile(self, profile):
        """
        Deletes profile as well as associated user is user is not active
        """
        user=None
        try:
            user = profile.user
            if user.is_active:
                logging.getLogger(__name__).warn("Profile shouldn't exist for active user '{0}'. Deleting.".format(user))
                profile.delete()
            else:
                profile.delete()
                user.delete()
        except Exception as exception:
            logging.getLogger(__name__).error("Failed to delete '{0}': {1}".format(user, exception))
            # rethrow so that our commit is rolled back and so they know what happened back home
            raise exception


    def get_expired_profiles(self):
        """
        Returns collection of expired profiles
        """
        expiration_date = datetime.datetime.utcnow()-datetime.timedelta(days=settings.REGISTRATION_ACTIVATION_DAYS)
        return self.filter(user__date_joined__lte=expiration_date)


    def delete_expired_profiles(self, expired=None):
        """
        Remove expired instances of ``RegistrationProfile`` and their
        associated ``User``s.

        Accounts to be deleted are identified by searching for
        instances of ``RegistrationProfile`` with expired activation
        keys, and then checking to see if their associated ``User``
        instances have the field ``is_active`` set to ``False``; any
        ``User`` who is both inactive and has an expired activation
        key will be deleted.
        """
        if expired is None:
            expired = self.get_expired_profiles()
        for profile in expired:
            try:
                self.delete_profile(profile)
            except:
                # note: it will have been logged
                pass


class RegistrationProfile(models.Model):
    """
    A simple profile which stores an activation key for use during
    user account registration.

    Generally, you will not want to interact directly with instances
    of this model; the provided manager includes methods
    for creating and activating new accounts, as well as for cleaning
    out accounts which have never been activated.

    While it is possible to use this model as the value of the
    ``AUTH_PROFILE_MODULE`` setting, it's not recommended that you do
    so. This model's sole purpose is to store data temporarily during
    account registration and activation.

    """
    user = models.ForeignKey(User, unique=True, verbose_name=_('user'))
    activation_key = models.CharField(_('activation key'), unique=True, max_length=40)

    objects = RegistrationManager()

    class Meta:
        verbose_name = _('registration profile')
        verbose_name_plural = _('registration profiles')
        db_table = 'registration_profile'


    def __unicode__(self):
        return u"Registration information for %s"%self.user


    def registration_date(self):
        return self.user.date_joined

    def activation_expiration(self):
        return self.user.date_joined+datetime.timedelta(days=getattr(settings, 'REGISTRATION_ACTIVATION_DAYS', 7))

    @methodBooleanPropertyTrue
    def activation_expired(self):
        """
        Determine whether this ``RegistrationProfile``'s activation
        window has passed: date > (date_joined+REGISTRATION_ACTIVATION_DAYS)
        """
        return self.activation_expiration()<=datetime.datetime.utcnow()


    def send_activation_email(self, site):
        """
        Forwarding requests to main application to keep contact central
        """
        contact.send_activation_email(self.user, self.activation_key, site)

    def send_activated_email(self, site):
        """
        Forwarding requests to main application to keep contact central
        """
        contact.send_activated_email(self.user, site)
