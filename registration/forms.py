"""
Forms and validation code for user registration.

"""
from django.conf import settings

from django.contrib.auth.models import User
from django import forms
from django.utils.translation import ugettext_lazy
from ghosts.security import username_contains_flagged_content


attrs_dict={'class':'required'}

class RegistrationForm(forms.Form):
    """
    Form for registering a new user account.

    Validates that the requested username is not already in use, and
    requires the password to be entered twice to catch typos.

    Subclasses should feel free to add any additional validation they
    need, but should avoid defining a ``save()`` method -- the actual
    saving of collected user data is delegated to the active
    registration backend.
    """
    username=forms.RegexField(widget=forms.TextInput(attrs=attrs_dict),
        regex=r'^[\w.@+-]+$',
        min_length=getattr(settings, 'USERNAME_MIN_LENGTH', 4),
        max_length=getattr(settings, 'USERNAME_MAX_LENGTH', 30),
        label=ugettext_lazy("Username"),
        error_messages={'invalid':ugettext_lazy("A username may be a combination of letters, numbers and .@+-_.")})

    email=forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict, maxlength=getattr(settings, 'EMAIL_MAX_LENGTH', 50))),
        label=ugettext_lazy("E-mail"))

    password=forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
        label=ugettext_lazy("Password"))

    def clean_username(self):
        """
        Validate that the username is alphanumeric and is not already
        in use.
        """
        cleaned_username=self.cleaned_data['username']
        existing=User.objects.filter(username__iexact=cleaned_username)
        if existing.exists():
            raise forms.ValidationError(ugettext_lazy("A user with that username already exists."))
        elif username_contains_flagged_content(cleaned_username):
            raise forms.ValidationError(ugettext_lazy("Username is reserved."))
        return cleaned_username

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the site.
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(ugettext_lazy("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email']



class RegistrationFormTermsOfService(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which adds a required checkbox
    for agreeing to a site's Terms of Service.

    """
    tos=forms.BooleanField(widget=forms.CheckboxInput(attrs=attrs_dict),
        label=ugettext_lazy(u'I have read and agree to the Terms of Service'),
        error_messages={'required':ugettext_lazy("You must agree to the terms to register")})
