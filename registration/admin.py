from django.contrib import admin, messages
from django.utils.translation import ugettext_lazy as _
from common.db import get_project_site

from registration.models import RegistrationProfile


class RegistrationAdmin(admin.ModelAdmin):
    # list display properties
    list_display = ('user', 'activation_expired', 'registration_date', 'activation_expiration', 'activation_key')
    search_fields = ('user__username', 'activation_key')
    actions = ['activate_users', 'resend_activation_email', 'delete_profiles']
    # edit view properties
    readonly_fields = ('user', 'activation_key')


    ''' actions '''
    def activate_users(self, request, queryset):
        """
        Activates the selected users, if they are not already activated.
        Ignores expiration as this is viewed as an exception.
        """
        success_count = 0; error_count = 0
        for profile in queryset:
            try:
                RegistrationProfile.objects.activate_user(profile.activation_key, site=get_project_site(request), override_expiration=True)
                success_count += 1
            except:
                error_count += 1
        self._send_action_result(request, success_count, error_count, 'Account activation: success count={0}, failure count={1}')


    def resend_activation_email(self, request, queryset):
        """
        Re-sends activation emails for the selected users.
        """
        success_count = 0; error_count = 0
        site = get_project_site()
        for profile in queryset:
            try:
                if profile.activation_expired():
                    # preventing these guys. Force operator to change joined date.
                    error_count+=1
                else:
                    profile.send_activation_email(site)
                    success_count+=1
            except:
                error_count+=1
        self._send_action_result(request, success_count, error_count, 'Resend activation emails: success count={0}, failure count={1}')


    def delete_profiles(self, request, queryset):
        """
        Deletes profiles in queryset.  Deletes their associated
        User's as well if the user is not active
        """
        success_count = 0; error_count = 0
        for profile in queryset:
            try:
                RegistrationProfile.objects.delete_profile(profile)
                success_count += 1
            except:
                error_count += 1
        self._send_action_result(request, success_count, error_count, 'Profiles deleted: success count={0}, failure count={1}')


    ''' Private api '''
    @staticmethod
    def _send_action_result(request, success_count, error_count, message):
        message=message.format(success_count, error_count)
        if error_count>0:
            messages.warning(request, message, fail_silently=True)
        elif success_count>0:
            messages.info(request, message, fail_silently=True)


    ''' attach metadata '''
    activate_users.short_description = _("Activate selected Users (overrides expiration)")
    resend_activation_email.short_description = _("Re-send activation emails to selection")
    delete_profiles.short_description = _("Delete selected Profiles and Users not active")


admin.site.register(RegistrationProfile, RegistrationAdmin)
