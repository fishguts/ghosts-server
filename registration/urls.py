"""
URLconf for registration and activation, using django-registration's
default backend.

If the default behavior of these views is acceptable to you, simply
use a line like this in your root URLconf to set up the default URLs
for registration::

    (r'^profile/', include('registration.urls')),

"""

from django.conf.urls import patterns, url
from django.views.generic.simple import direct_to_template

from registration.views import activate
from registration.views import register


urlpatterns=patterns('',
    url(r'^activate/complete/$', direct_to_template, {'template':'registration/page_activation_complete.html'}, name='registration_activation_complete'),
    url(r'^activate/failed/$', direct_to_template, {'template':'registration/page_activation_failed.html'}, name='registration_activation_failed'),
    # Activation keys get matched by \w+ instead of the more specific [a-fA-F0-9]{40} because a bad activation key should still get to the view;
    # that way it can return a sensible "invalid key" message instead of a confusing 404.
    url(r'^activate/(?P<activation_key>\w+)/$', activate, name='registration_activate'),
    url(r'^register/$', register, name='registration_register'),
    url(r'^register/complete/$', direct_to_template, {'template':'registration/page_registration_complete.html'}, name='registration_complete'),
    url(r'^register/closed/$', direct_to_template, {'template':'registration/page_registration_closed.html'}, name='registration_disallowed'),
)
