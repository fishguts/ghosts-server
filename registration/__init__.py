from django.conf import settings

from common import constants
from common.db import get_project_site
from common.exceptions import StatusException
from common.objects import Status

from registration.forms import RegistrationForm
from registration.models import RegistrationProfile


class RegistrationProxy(object):
    """
    A registration backend which follows a simple workflow:

    1. User signs up, inactive account is created.

    2. Email is sent to user with activation link.

    3. User clicks activation link, account is now active.

    Using this backend requires that

    * ``registration`` be listed in the ``INSTALLED_APPS`` setting
      (since this backend makes use of models defined in this
      application).

    * The setting ``REGISTRATION_ACTIVATION_DAYS`` be supplied, specifying
      (as an integer) the number of days from registration during
      which a user may activate their account (after that period
      expires, activation will be disallowed).

    Additionally, registration can be temporarily closed by adding the
    setting ``REGISTRATION_OPEN`` and setting it to
    ``False``. Omitting this setting, or setting it to ``True``, will
    be interpreted as meaning that registration is currently open and
    permitted.

    Internally, this is accomplished via storing an activation key in
    an instance of ``registration.models.RegistrationProfile``. See
    that model and its custom manager for full documentation of its
    fields and supported operations.

    """

    @staticmethod
    def register(request, form=None, data=None, files=None):
        """
        Given a username, email address and password, register a new
        user account, which will initially be inactive.

        Along with the new ``User`` object, a new
        ``registration.models.RegistrationProfile`` will be created,
        tied to that ``User``, containing the activation key which
        will be used for this account.

        An email will be sent to the supplied email address; this
        email should contain an activation link. The email will be
        rendered using two templates. See the documentation for
        ``RegistrationProfile.send_activation_email()`` for
        information about these templates and the contexts provided to
        them.

        After the ``User`` and ``RegistrationProfile`` are created and
        the activation email is sent, the signal
        ``registration.signals.user_registered`` will be sent, with
        the new ``User`` as the keyword argument ``user`` and the
        class of this backend as the sender.

        """
        if not RegistrationProxy.registration_allowed(request):
            raise StatusException(Status(constants.RESPONSE_CODE_FAIL, 'Registration temporarily disabled'))

        if data is None:
            data=request.POST
        if files is None:
            files=request.FILES
        if form is None:
            form=RegistrationForm(data=data, files=files)

        if not form.is_valid():
            errors=[error[0] for error in form.errors.values()]
            raise StatusException(Status(constants.RESPONSE_CODE_FAIL, '\n'.join(errors)))

        site=get_project_site(request)
        try:
            user=RegistrationProfile.objects.create_inactive_user(username=form.cleaned_data['username'],
                                                                  email=form.cleaned_data['email'],
                                                                  password=form.cleaned_data['password'],
                                                                  site=site)
        except Exception as error:
            # translate into a status exception
            raise StatusException(Status(constants.RESPONSE_CODE_FAIL, error.message))

        return user


    @staticmethod
    def activate(request, activation_key):
        """
        Given an an activation key, look up and activate the user
        account corresponding to that key (if possible).

        After successful activation, the signal
        ``registration.signals.user_activated`` will be sent, with the
        newly activated ``User`` as the keyword argument ``user`` and
        the class of this backend as the sender.

        """
        site=get_project_site(request)
        try:
            user=RegistrationProfile.objects.activate_user(activation_key, site=site)
        except Exception as error:
            raise StatusException(Status(constants.RESPONSE_CODE_FAIL, error.message))

        return user


    @staticmethod
    def registration_allowed(request):
        """
        Indicate whether account registration is currently permitted,
        based on the value of the setting ``REGISTRATION_OPEN``. This
        is determined as follows:

        * If ``REGISTRATION_OPEN`` is not specified in settings, or is
          set to ``True``, registration is permitted.

        * If ``REGISTRATION_OPEN`` is both specified and set to
          ``False``, registration is not permitted.

        """
        return getattr(settings, 'REGISTRATION_OPEN', True)
