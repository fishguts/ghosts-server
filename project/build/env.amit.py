"""
@author: curt
@created: 8/2012
Constants unique to local build
"""
import os

DEBUG = True

# environment constant should remain text.
ENVIRONMENT = 'local'

PROJECT_NAME = 'goggles'
# amit todo: set PROJECT_ROOT to point to the app directory on your machine
PROJECT_ROOT = '/Users/curt/Develop/projects/server/ghosts/app/'
PROJECT_HOST = 'localhost:8000'
STATIC_ROOT = ''

EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = 0
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''

WEBSITE_HOST='localhost'
WEBSITE_ROOT='file://localhost/Users/curt/Develop/projects/server/ghosts/website/'

DATABASES = {
    'default':{
        'ENGINE':'django.db.backends.sqlite3',
        'NAME':os.path.join(PROJECT_ROOT, 'sqlite.db'),
        'USER':'', # Not used with sqlite3.
        'PASSWORD':'', # Not used with sqlite3.
        'HOST':'', # Not used with sqlite3.
        'PORT':'', # Not used with sqlite3.
    }
}
