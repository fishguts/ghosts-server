"""
@author: curt
@created: 12/2012
Constants unique to staging build
"""

DEBUG = True

# environment constant should remain text.  See constants in project level constants
ENVIRONMENT = 'staging'

PROJECT_NAME = 'goggles'
PROJECT_ROOT = '/home/fishingtom/webapps/ghosts_app/server/'
PROJECT_HOST = 'ghosts.furrylab.com'
STATIC_ROOT = '/home/fishingtom/webapps/ghosts_static'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'ghosts'
EMAIL_HOST_PASSWORD = 'W1llyW0nka'

WEBSITE_HOST='furrylab.com'
WEBSITE_ROOT='http://furrylab.com'

DATABASES = {
    'default':{
        'ENGINE':'django.db.backends.mysql',
        'NAME':'fishingtom_ghosts',
        'USER':'fishingtom_ghost',
        'PASSWORD':'W1llyW0nka',
        'HOST':'localhost',
        'PORT':'3306',
    }
}


