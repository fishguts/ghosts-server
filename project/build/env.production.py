"""
@author: curt
@created: 12/2012
Constants unique to production
"""

DEBUG = False

# environment constant should remain text.  See constants in project level constants
ENVIRONMENT = 'production'

PROJECT_NAME = 'goggles'
PROJECT_ROOT = '/home/fishingtom/webapps/goggles_app/server/'
PROJECT_HOST = 'goggles.xraymen.com'
STATIC_ROOT = '/home/fishingtom/webapps/goggles_static'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'goggles'
EMAIL_HOST_PASSWORD = 'W1llyW0nka'
EMAIL_ADMINS = (
    ('curt', 'curt.elsasser@gmail.com'),
)

WEBSITE_HOST='xraymen.com'
WEBSITE_ROOT='http://xraymen.com'

DATABASES = {
    'default':{
        'ENGINE':'django.db.backends.mysql',
        'NAME':'fishingtom_goggles',
        'USER':'fishingtom_ghost',
        'PASSWORD':'W1llyW0nka',
        'HOST':'localhost',
        'PORT':'3306',
    }
}
