"""
@author: curt
@created: 8/2012
Constants unique to local build
"""

DEBUG = True
DEBUG_SUSPEND_ADMIN_READONLY=True

# environment constant should remain text.
ENVIRONMENT = 'local'

PROJECT_NAME = 'goggles'
PROJECT_ROOT = '/Users/curt/Develop/projects/server/ghosts/app/'
PROJECT_HOST = 'localhost:8000'
STATIC_ROOT = ''

EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_HOST = ''
EMAIL_PORT = 0
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''

WEBSITE_HOST='localhost'
WEBSITE_ROOT='file://localhost/Users/curt/Develop/projects/server/ghosts/website/'

DATABASES = {
    'default':{
        'ENGINE':'django.db.backends.mysql',
        'NAME':'ghost',  # note: 16 character limit
        'USER':'root',
        'PASSWORD':'',
        'HOST':'localhost',
        'PORT':'3306',
    }
}
