from django.conf.urls import patterns, include, url
from django.contrib import admin
from ghosts.views import settings as views_settings
from ghosts.views import message as views_message
from ghosts.views import profile as views_profile
from ghosts.views import support as views_support
from ghosts.views import admin as views_admin


admin.autodiscover()

'''
Note concerning registration and activation: we do not use registration's url mapping. His urls are tailored for website registration.
Ours are pretty tailored.  We redirect activation status to our static website's success and failure pages and do so here. I messed
around with customized overrides and they worked, but ultimately we don't want any of his routings should any of his urls be matched.
So his url patterns are dead to us!
'''
urlpatterns = patterns('',
    url(r'^settings/get/json$', views_settings.process_get),
    url(r'^messages/get/json$', views_message.process_get),
    url(r'^messages/put/json$', views_message.process_put),
    url(r'^messages/rate/json$', views_message.process_rate),
    url(r'^messages/flag/json$', views_message.process_flag),
    url(r'^messages/delete/json$', views_message.process_delete),
    url(r'^profile/login/json$', views_profile.process_login),
    url(r'^profile/logout/json$', views_profile.process_logout),
    url(r'^profile/update/json$', views_profile.process_update),
    url(r'^profile/delete/json$', views_profile.process_delete),
    url(r'^profile/register/json$', views_profile.process_register),
    # Activation keys get matched by \w+ instead of the more specific [a-fA-F0-9]{40} because a bad activation key should still get to the view;
    # that way it can return a sensible "invalid key" message instead of a confusing 404.
    url(r'^profile/activate/(?P<activation_key>\w+)$', views_profile.process_activate),
    url(r'^support/feedback/json$', views_support.process_feedback),
    url(r'^support/contact/form$', views_support.process_contact),
    # todo: moved admin to internal so that people have trouble finding it.  Need to find a more secure solution.
    url(r'^internal/admin/messages/get/json$', views_admin.process_get),
    url(r'^internal/admin/', include(admin.site.urls)),
)
