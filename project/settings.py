# Django settings for ghosts project.
import env
import os
import project


''' general settings '''
SERVER_VERSION = '1.0.0'
PROJECT_NAME = env.PROJECT_NAME
PROJECT_ROOT = env.PROJECT_ROOT
PROJECT_HOST = env.PROJECT_HOST
WEBSITE_HOST = env.WEBSITE_HOST
WEBSITE_ROOT = env.WEBSITE_ROOT
PROJECT_LOG = project.get_log_path(project_root=PROJECT_ROOT, file_name=PROJECT_NAME)

DEBUG = env.DEBUG
TEMPLATE_DEBUG = env.DEBUG
DEBUG_SUSPEND_ADMIN_READONLY=getattr(env, 'DEBUG_SUSPEND_ADMIN_READONLY', False)


USERNAME_MIN_LENGTH=4
USERNAME_MAX_LENGTH=30
EMAIL_MAX_LENGTH=50
MESSAGE_MIN_LENGTH=2
MESSAGE_MAX_LENGTH=512
FEEDBACK_MIN_LENGTH=4
FEEDBACK_MAX_LENGTH=1250
CONTACT_NAME_MIN_LENGTH=2
CONTACT_NAME_MAX_LENGTH=50
PERIMETER_QUERY_RADIUS_MIN=1


''' registration settings '''
REGISTRATION_OPEN = True
REGISTRATION_ACTIVATION_DAYS = 7      # window of opportunity before activation expires
MESSAGE_POST_EXPIRATION_DAYS = 7      # days expired messages are kept before they are deleted


''' database configuration '''
DATABASES = env.DATABASES


''' email address and host configuration '''
EMAIL_ADDRESSES = {
    # product email addresses
    'default':'support@'+WEBSITE_HOST,
    'registration':'support@'+WEBSITE_HOST,
    'noreply':'noreply@'+WEBSITE_HOST,
    # internal email addresses
    'admin':'admin@'+PROJECT_HOST,
    'server':'server@'+PROJECT_HOST,
}

# people/email addresses who get code error notifications
ADMINS = (('admin', EMAIL_ADDRESSES['admin']),) + getattr(env, 'EMAIL_ADMINS', tuple())
# called by mail_managers
MANAGERS = ADMINS
# used by mail_admins and mail_managers
SERVER_EMAIL = EMAIL_ADDRESSES['server']
DEFAULT_FROM_EMAIL = EMAIL_ADDRESSES['default']

# mail host/account settings
EMAIL_HOST = env.EMAIL_HOST
EMAIL_PORT = env.EMAIL_PORT
EMAIL_HOST_USER = env.EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = env.EMAIL_HOST_PASSWORD
EMAIL_BACKEND = env.EMAIL_BACKEND       # note: django test runner overrides this to use memory based EmailBackend
EMAIL_FILE_PATH = project.get_log_root(PROJECT_ROOT)


''' misc stock stuff '''
SITE_ID = 1         # Database 'Sites' key.

TIME_ZONE = 'America/New_York'
LANGUAGE_CODE = 'en-us'
USE_I18N = True     # If you set this to False, Django will make some optimizations so as not to load the internationalization machinery.
USE_L10N = True     # If you set this to False, Django will not format dates, numbers and calendars according to the current locale.
USE_TZ = False      # If you set this to False, Django will not use timezone-aware datetimes.

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Note: store your static files in apps' "static/" subdirectories and in STATICFILES_DIRS.
STATIC_ROOT = env.STATIC_ROOT

# URL prefix for static files. Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
# Put strings here, like "/home/html/static" or "C:/www/django/static".
# Always use forward slashes, even on Windows.
# Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'resources/templates'),
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '!*ue1#2wo-@ugtckv^a1ob&amp;t)g_$#fj&amp;v047p#za3-ix64pw#='

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'project.urls'
AUTH_PROFILE_MODULE = 'ghosts.Profile'     # this is our one to one profile: [appName].[moduleName]

# used by Django's runserver.
WSGI_APPLICATION = 'project.wsgi.application'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'ghosts',
    'registration',
)

# logging configuration
LOGGING = {
    'version':1,
    'disable_existing_loggers':False,

    'formatters':{
        'verbose':{
            'format':'%(levelname)s|%(asctime)s|%(name)s.%(funcName)s|%(message)s',
            'datefmt':'%Y%m%d %H:%M:%S',
        },
        'simple':{
            'format':'%(levelname)s|%(name)s.%(funcName)s|%(message)s',
        },
    },

    'handlers':{
        'null':{
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter':'simple'
        },
        'file':{
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename':PROJECT_LOG,
            'maxBytes':512000,
            'backupCount':9,
            'formatter':'verbose'
        },
        'mail_admins':{
            # note: This level is how we filter what goes to mail even though it's included
            # in our loggers at any level.  That took a while for me to sort out.  Silly rabbit.
            'level':'ERROR',
            'class':'django.utils.log.AdminEmailHandler',
            'include_html':True,
            # default configuration filters on debug.  Turn that behavior off
            'filters':[],
        }
    },

    'loggers':{
        # Leaving on for now so that I can get stack traces of errors
        'django.request':{
            'handlers':['file', 'mail_admins'],
            'level':'ERROR',
            'propagate':True,
        },
        # mail_admins? yes - it's filtered at the handler level. We cannot have duplicates at the
        # logger level.  Hence the filter at the handler level, I'm guessing.
        'ghosts':{
            'handlers':['console', 'file', 'mail_admins'],
            'level':'DEBUG',
            'propagate':True,
        },
        'registration':{
            'handlers':['console', 'file', 'mail_admins'],
            'level':'DEBUG',
            'propagate':True,
        },
    },
}
