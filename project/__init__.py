"""

"""
import os


def get_log_root(project_root):
    """
    Ensures that the path is good to go and returns log file name
    """
    path=os.path.join(project_root, 'logs')
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def get_log_path(project_root, file_name):
    """
    Ensures that the path is good to go and returns log file name
    """
    log_root=get_log_root(project_root)
    return os.path.join(log_root, file_name + '.log')
