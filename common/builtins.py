"""
Created on Jul 25, 2012

@author: curt
"""
import logging


''' Functional handlers and asserts '''
def raise_if_true(condition, exception, text):
    if condition:
        raise exception(text)

def raise_if_false(condition, exception, text):
    if not condition:
        raise exception(text)

def raise_if_none(condition, exception, text):
    if condition is None:
        raise exception(text)

def fail_to_debug(text, module=__name__):
    logging.getLogger(module).debug(text)

def fail_to_info(text, module=__name__):
    logging.getLogger(module).info(text)

def fail_to_warn(text, module=__name__):
    logging.getLogger(module).warn(text)

def fail_to_error(text, module=__name__):
    logging.getLogger(module).error(text)

def fail_to_value_error(text, module=None):
    raise ValueError(text)


''' Extensions '''
def is_string(value):
    _type=type(value)
    return _type==str or _type==unicode

def allf(function, iterable):
    for item in iterable:
        if not function(item):
            return False
    return True

def anyf(function, iterable):
    for item in iterable:
        if function(item):
            return True
    return False

def unique(iterable):
    result=set()
    for item in iterable:
        result.add(item)
    return result

def for_each(function, iterable):
    """
    Calls function for every value in iterable
    """
    for value in iterable:
        function(value)

def apply_each(function, iterable):
    """
    Calls function for every value in iterable and assigns it back to iterable.
    @returns: iterable
    """
    for index, value in enumerate(iterable):
        iterable[index]=function(value)
    return iterable

def fopen(path, mode, callback, *args):
    '''
    Attempts to open path. If all goes well then calls callback with *args.
    Makes sure the file handle is callback returns.
    '''
    instance=None
    try:
        instance=open(path, mode)
        callback(instance, *args)
    finally:
        if instance:
            instance.close()


