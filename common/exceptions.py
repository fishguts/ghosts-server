"""
Created on Jul 27, 2012

@author: curt
"""

class StatusException(Exception):

    def __init__(self, status):
        Exception.__init__(self, status.text)
        self.status=status

    def __unicode__(self):
        return self.status
