"""
Created: 11/3/12
@author: curt
"""

from django.http import HttpResponseForbidden

def process_forbidden(request):
    return HttpResponseForbidden()
