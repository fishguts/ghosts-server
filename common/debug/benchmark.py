"""
created: 8/9/13
@author: curt
"""
from datetime import datetime

def time_method(iterations, callback, *args, **kwargs):
    index=0
    stamp_start=datetime.now()
    while index<iterations:
        callback(*args, **kwargs)
        index+=1
    stamp_stop=datetime.now()
    elapsed=stamp_stop-stamp_start
    print 'Benchmark complete: seconds={0}, callback="{1}", iterations={2}'.format(elapsed.total_seconds(), callback.__name__, iterations)
    return elapsed
