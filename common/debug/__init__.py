"""
created: 7/19/13
@author: curt
"""

import traceback
import sys
from django.conf import settings


''' Public Interface '''
def cassert(condition, message=None):
    if not settings.DEBUG:
        return
    if not condition:
        _write_assertion('Assertion failed', message)


''' Private Interface '''
def _write_assertion(prefix, message):
    sys.stderr.write(prefix)
    if message:
        sys.stderr.write(' {0}'.format(message))
    sys.stderr.write(':\n'.format(message))
    traceback.print_stack(limit=5)
