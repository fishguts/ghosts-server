"""
created: 7/19/13
@author: curt
"""

from common.test.testcases import TestCaseDebug


''' Functional testing support '''
def passthru_outter(*args, **kwargs):
    '''
    Calls down to passthru_inner. Useful for proxying callbacks:
        - point to this guy as callback
        - proxy passthru_inner
    '''
    passthru_inner(args, kwargs)


def passthru_inner(*args, **kwargs):
    pass
