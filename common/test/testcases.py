"""
created: 7/19/13
@author: curt
"""
import sys
import importlib
from StringIO import StringIO

from django.conf import settings
from django.test import TestCase


class TestCaseDebug(TestCase):
    """
    Makes sure that debug is set before tests are run and restores initial value on teardown
    """
    def setUp(self):
        super(TestCaseDebug, self).setUp()
        self._debug_state=getattr(settings, 'DEBUG', False)
        setattr(settings, 'DEBUG', True)
        # save std_err so that we can swap him out freely
        self._std_err=sys.stderr

    def tearDown(self):
        super(TestCaseDebug, self).tearDown()
        setattr(settings, 'DEBUG', self._debug_state)
        sys.stderr=self._std_err


    def setupStdErr(self, io=None):
        if io is None:
            io=StringIO()
        sys.stderr=io
        return io

    def restoreStdErr(self):
        sys.stderr=self._std_err


    ''' Test Extension '''
    def assertIsCalled(self, callback_path, execute_method, *args, **kwargs):
        """
        Asserts that callback is called
        :param callback_path: full package.method path: module.method
        :param execute_method: executes this guy once callback is proxied
        :param args: args passed to execute_method
        :param kwargs: kwargs passed to execute_method
        """
        self._performAssertCalled(True, callback_path, execute_method, *args, **kwargs)

    def assertNotCalled(self, callback_path, execute_method, *args, **kwargs):
        """
        Asserts that callback is not called
        :param callback_path: full package.method path: module.method
        :param execute_method: executes this guy once callback is proxied
        :param args: args passed to execute_method
        :param kwargs: kwargs passed to execute_method
        """
        self._performAssertCalled(False, callback_path, execute_method, *args, **kwargs)


    ''' Private '''
    def _assertCalledProxy(self, *args, **kwargs):
        self._called=True

    def _performAssertCalled(self, called, callback_path, execute_method, *args, **kwargs):
        self.assertIsNone(getattr(self, '_called', None), "We do not support re-entry")
        self._called=False
        # pick apart our path into package and method
        method_delimiter=callback_path.rfind('.')
        if method_delimiter>-1:
            package=importlib.import_module(callback_path[0:method_delimiter])
            method_name=callback_path[method_delimiter+1:]
        else:
            package=importlib.import_module('__builtin__')
            method_name=callback_path
        method_old=getattr(package, method_name)
        try:
            setattr(package, method_name, self._assertCalledProxy)
            execute_method(*args, **kwargs)
            self.assertEqual(self._called, called)
        finally:
            setattr(package, method_name, method_old)
            del self._called

