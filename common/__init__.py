"""
Created on Dec 7, 2011

@author: curt
"""

''' Types '''
class StaticClass(object):
    """
    Makes sure an instance is not instantiated
    """
    def __init__(self):
        raise Exception("Don't instantiate")


class Singleton(object):
    """
    Derive from it and through __new__ this guy ensures that only a single instance of a class
    type will be instantiated.
    """
    _instances = {}

    def __new__(cls, *args):
        instance=Singleton._instances.get(cls)
        if instance is None:
            instance=object.__new__(cls, *args)
            Singleton._instances[cls]=instance
        return instance
