"""
created: 4/12/13
@author: curt
"""

''' Crude methods to set function properties (clues to admintool) '''
def methodBooleanPropertyTrue(function):
    function.boolean=True
    return function

def methodBooleanPropertyFalse(function):
    function.boolean=False
    return function
