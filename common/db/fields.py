"""
created: 1/13/13
@author: curt
"""
from django.db.models import CharField

BLANK_CHOICE = [("", "None")]

class EnumField(CharField):
    """
    We have a default blank option (see above).  If you don't like it then supply your own
    and set suppress_blank to True.
    """
    def __init__(self, suppress_blank_label=False, *args, **kwargs):
        super(EnumField, self).__init__(*args, **kwargs)
        self.suppress_blank_label=suppress_blank_label


    def get_choices(self, include_blank=True, blank_choice=BLANK_CHOICE):
        if self.suppress_blank_label:
            blank_choice=None
        return super(EnumField, self).get_choices(include_blank, blank_choice)

