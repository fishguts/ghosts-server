"""
created: 1/13/13
@author: curt
"""
from django.contrib.sites.models import Site, RequestSite


def get_project_site(request=None):
    try:
        site=Site.objects.get_current()
    except:
        site=RequestSite(request)
    return site
