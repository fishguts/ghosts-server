"""
Created on Jul 25, 2012

@author: curt
"""

class Status(object):
    def __init__(self, code, text):
        self.code = code
        self.text = text
