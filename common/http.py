"""
Created on Jul 27, 2012

@author: curt
"""
from django.http import HttpResponse

class HttpResponseJson(HttpResponse):

    def __init__(self, json):
        HttpResponse.__init__(self, content=json, content_type='application/json; charset=utf-8')
