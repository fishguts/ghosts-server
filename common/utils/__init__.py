"""
Created on Dec 7, 2011

@author: curt
"""
from common import StaticClass


class ArrayXT(StaticClass):
    """
    Static class of array utils
    """
    @staticmethod
    def safe_len(array):
        return len(array) if array else 0

    @staticmethod
    def safe_value(array, index, default=None):
        return array[index] if ArrayXT.safe_len(array)>index else default


class StringXT(StaticClass):
    """
    Static class of string utilities
    """
    @staticmethod
    def is_empty(value):
        return value is None or len(value)==0

    @staticmethod
    def is_not_empty(value):
        return value is not None and len(value)>0

    @staticmethod
    def safe(value, dfault=''):
        return value if value is not None else dfault

    @staticmethod
    def safe_len(value):
        return len(value) if value is not None else 0

    @staticmethod
    def ellipsis(value, total_length, dfault='.'):
        return value if len(value)<=total_length else value[0:max(total_length-3, 0)] + dfault*3
