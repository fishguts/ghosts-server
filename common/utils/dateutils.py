"""
created: 7/22/2013

@author: curt
"""

from datetime import datetime, timedelta


''' Formatters '''
def date_to_YMDHM(date):
    return date.strftime('%Y/%m/%d %H:%M')

def date_to_YMDHMS(date):
    return date.strftime('%Y/%m/%d %H:%M:%S')

def delta_to_HMS(delta):
    # cannot figure out format spec so for now just strip micros if they are there
    if delta.microseconds:
        delta=timedelta(days=delta.days, seconds=delta.seconds)
    return str(delta)


''' Parsers '''
def text_to_date(text, dfault=None):
    """
    Attempts to be smart and supports the a myriad of formats:
    - Y/M/D H:M or Y/M/D H:M:S
    - M/D/Y H:M or M/D/Y H:M:S
    - Y/M/D or M/D/Y [dfault time]
    - [dfault date] H:M or H:M:S
    """
    try:
        if text.find('/')>2:
            return YMDHM_or_YMDHMS_to_date(text)
        else:
            return MDYHM_or_MDYHMS_to_date(text)
    except Exception as exception:
        if not dfault:
            raise exception
        try:
            return YMD_or_MDY_to_date(text, time=dfault.time())
        except:
            return HM_or_HMS_to_time(text, date=dfault.date())


def YMD_or_MDY_to_date(text, time=None):
    """
    Attempts to be smart by looking for first / delimiter: 2 or 4 characters.
    :param text: parse text
    :param time: optional time to slip into datetime
    :return:
    """
    if text.find('/')>2:
        date=datetime.strptime(text, '%Y/%m/%d')
    else:
        date=datetime.strptime(text, '%m/%d/%Y')
    return datetime.combine(date=date, time=time) if time else date

def HM_or_HMS_to_time(text, date=None):
    """
    Picks apart time text
    :param text: parse text
    :param date: optional date to combine with time
    :return:
    """
    if text.count(':')==1:
        time=datetime.strptime(text, '%H:%M')
    else:
        time=datetime.strptime(text, '%H:%M:%S')
    return datetime.combine(date=date, time=time.time()) if time else time


def YMDHM_to_date(text):
    return datetime.strptime(text, '%Y/%m/%d %H:%M')

def YMDHMS_to_date(text):
    return datetime.strptime(text, '%Y/%m/%d %H:%M:%S')

def YMDHM_or_YMDHMS_to_date(text):
    if text.count(':')==1:
        return YMDHM_to_date(text)
    else:
        return YMDHMS_to_date(text)


def MDYHM_to_date(text):
    return datetime.strptime(text, '%m/%d/%Y %H:%M')

def MDYHMS_to_date(text):
    return datetime.strptime(text, '%m/%d/%Y %H:%M:%S')

def MDYHM_or_MDYHMS_to_date(text):
    if text.count(':')==1:
        return MDYHM_to_date(text)
    else:
        return MDYHMS_to_date(text)

