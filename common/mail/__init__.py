"""
created: 2/17/13
@author: curt
"""
import logging
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


def render_to_subject(template, context):
    result=render_to_string(template, context).strip()
    lines=result.splitlines()
    if len(lines)>1:
        logging.getLogger(__name__).error('Email subject template "{0}" has more than one line')
        result=''.join(lines)
    return result


def send_multipart_email(from_email, to_emails, subject, body_txt, body_html, fail_silently=False):
    try:
        mail = EmailMultiAlternatives(subject=subject, body=body_txt, from_email=from_email, to=to_emails)
        mail.attach_alternative(body_html, 'text/html')
        mail.send(fail_silently=fail_silently)
    except BaseException as exception:
        logging.getLogger(__name__).error('Mail server: {0}'.format(exception))
        if isinstance(exception, IOError):
            raise Exception('Attempt to send email failed: {0}'.format(exception.strerror))
        else:
            raise Exception('Attempt to send email failed: {0}'.format(exception))
