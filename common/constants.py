"""
Created: 11/2/2012
@author: curt
"""

from common.objects import Status


RESPONSE_CODE_OKAY='okay'
RESPONSE_CODE_FAIL='fail'
RESPONSE_CODE_AUTH='auth'

STATUS_OKAY=Status(RESPONSE_CODE_OKAY, '')

