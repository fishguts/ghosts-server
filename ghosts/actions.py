"""
created: 4/12/13
@author: curt

Actions - probably as they mostly pertain to admin stuff but not
    tightly coupled to them.
"""
import logging
from django.db import transaction
from ghosts import constants, contact
from ghosts import models as model_ghosts


''' Public Interface '''
@transaction.commit_on_success
def feedback_update(feedback, flag=None, action=None):
    try:
        if flag:
            feedback.flag = flag
        if action:
            feedback.action = action
        feedback.save()
    except Exception as exception:
        logging.getLogger(__name__).error("Failed to update feedback '{0}': {1}".format(feedback.user, exception))
        raise exception


@transaction.commit_on_success
def feedback_cancel_account(feedback, send_email=True):
    try:
        status_changed = _user_cancel_account(user=feedback.user, reason='User initiated cancel', flag=False)
        if status_changed and send_email:
            contact.send_account_canceled_email(user=feedback.user)
        if feedback.action!=constants.ACTION_CLOSED:
            feedback.action = constants.ACTION_CLOSED
            feedback.save()
    except Exception as exception:
        logging.getLogger(__name__).error("Failed to cancel user '{0}': {1}".format(feedback.user, exception))
        raise exception


@transaction.commit_on_success
def user_cancel_account(user, send_email=True):
    try:
        status_changed = _user_cancel_account(user=user, reason='User initiated cancel', flag=False)
        if status_changed and send_email:
            contact.send_account_canceled_email(user=user)
    except Exception as exception:
        logging.getLogger(__name__).error("Failed to cancel user '{0}': {1}".format(user, exception))
        raise exception


@transaction.commit_on_success
def user_revoke_account(user, send_email=True):
    try:
        status_changed = _user_cancel_account(user=user, reason='Revoked by Goggles', flag=True)
        if status_changed and send_email:
            contact.send_account_revoked_email(user=user)
    except Exception as exception:
        logging.getLogger(__name__).error("Failed to revoke user '{0}': {1}".format(user, exception))
        raise exception


@transaction.commit_on_success
def message_flag_apply(flag):
    try:
        _message_flag_set_status(flag=flag, status=constants.MODEL_STATUS_FLAG)
    except Exception as exception:
        logging.getLogger(__name__).error("Failed to apply message flag '{0}': {1}".format(flag.id, exception))
        raise exception

@transaction.commit_on_success
def message_flag_apply_and_send_warning(flag):
    try:
        status_changed=_message_flag_set_status(flag=flag, status=constants.MODEL_STATUS_FLAG)
        if status_changed and flag.message.user.is_active:
            model_ghosts.Profile.get_user_profile(flag.message.user).add_note('Warned due to flagged content', save=True)
            contact.send_account_warning_email(user=flag.message.user)
    except Exception as exception:
        logging.getLogger(__name__).error("Failed to apply message flag and send warning '{0}': {1}".format(flag.id, exception))
        raise exception

@transaction.commit_on_success
def message_flag_apply_and_revoke_account(flag):
    try:
        _message_flag_set_status(flag=flag, status=constants.MODEL_STATUS_FLAG)
        if _user_cancel_account(user=flag.message.user, reason='Revoked by Goggles', flag=True):
            contact.send_account_revoked_email(user=flag.message.user)
    except Exception as exception:
        logging.getLogger(__name__).error("Failed to apply message flag and revoke account '{0}': {1}".format(flag.id, exception))
        raise exception

@transaction.commit_on_success
def message_flag_reject(flag):
    try:
        _message_flag_set_status(flag, constants.MODEL_STATUS_OKAY)
    except Exception as exception:
        logging.getLogger(__name__).error("Failed to reject message flag '{0}': {1}".format(flag.id, exception))
        raise exception


''' Private Interface '''
def _user_cancel_account(user, reason, flag=False):
    status_changed = user.is_active
    if status_changed:
        profile = model_ghosts.Profile.get_user_profile(user)
        user.is_active = False
        profile.add_note(reason)
        if flag:
            profile.status = constants.MODEL_STATUS_FLAG
        profile.save()
        user.save()
    return status_changed


def _message_flag_set_status(flag, status):
    message = flag.message
    status_changed = message.status!=status
    if status_changed:
        message.status = status
        message.save()
    for flag in message.flagged_set.all():
        if not flag.processed:
            flag.processed = True
            flag.save()
    return status_changed


