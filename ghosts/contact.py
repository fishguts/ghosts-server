"""
created: 6/3/13
@author: curt
"""
from django.conf import settings
from django.template.loader import render_to_string
from common.db import get_project_site
from common.mail import send_multipart_email, render_to_subject


def send_activation_email(user, activation_key, site):
    ctx_dict = {
        'username':user.username,
        'activation_key':activation_key,
        'expiration_days':getattr(settings, 'REGISTRATION_ACTIVATION_DAYS', 7),
        'site':site
    }
    subject = render_to_subject('registration/email_subject_registered.txt', ctx_dict)
    body_txt = render_to_string('registration/email_body_registered.txt', ctx_dict)
    body_html = render_to_string('registration/email_body_registered.html', ctx_dict)
    send_multipart_email(from_email=settings.EMAIL_ADDRESSES['registration'], to_emails=(user.email,), subject=subject, body_txt=body_txt, body_html=body_html)

def send_activated_email(user, site):
    ctx_dict = {
        'username':user.username,
        'site':site
    }
    subject = render_to_subject('registration/email_subject_activated.txt', ctx_dict)
    body_txt = render_to_string('registration/email_body_activated.txt', ctx_dict)
    body_html = render_to_string('registration/email_body_activated.html', ctx_dict)
    send_multipart_email(from_email=settings.EMAIL_ADDRESSES['registration'], to_emails=(user.email,), subject=subject, body_txt=body_txt, body_html=body_html)


def send_account_canceled_email(user):
    ctx_dict = {
        'user':user,
        'site':get_project_site()
    }
    subject = render_to_subject('user/email_subject_canceled.txt', ctx_dict)
    body_txt = render_to_string('user/email_body_canceled.txt', ctx_dict)
    body_html = render_to_string('user/email_body_canceled.html', ctx_dict)
    send_multipart_email(from_email=settings.EMAIL_ADDRESSES['registration'], to_emails=(user.email,), subject=subject, body_txt=body_txt, body_html=body_html)


def send_account_warning_email(user):
    ctx_dict = {
        'user':user,
        'site':get_project_site()
    }
    subject = render_to_subject('user/email_subject_warning.txt', ctx_dict)
    body_txt = render_to_string('user/email_body_warning.txt', ctx_dict)
    body_html = render_to_string('user/email_body_warning.html', ctx_dict)
    send_multipart_email(from_email=settings.EMAIL_ADDRESSES['registration'], to_emails=(user.email,), subject=subject, body_txt=body_txt, body_html=body_html)


def send_account_revoked_email(user):
    ctx_dict = {
        'user':user,
        'site':get_project_site()
    }
    subject = render_to_subject('user/email_subject_revoked.txt', ctx_dict)
    body_txt = render_to_string('user/email_body_revoked.txt', ctx_dict)
    body_html = render_to_string('user/email_body_revoked.html', ctx_dict)
    send_multipart_email(from_email=settings.EMAIL_ADDRESSES['registration'], to_emails=(user.email,), subject=subject, body_txt=body_txt, body_html=body_html)
