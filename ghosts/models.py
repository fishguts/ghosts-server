"""
Created Jul 2012
@author: curt

General note concerning enums and null fields. Ultimately, don't use them.  The admintool
writes NULL (as a space saver, I'm guessing) for blank keys. This will cause problems with
filtering and such.  Hence, don't allow null for such enums and the admintool will write ''
"""

from datetime import datetime
from django.conf import settings
from django.contrib.auth import models as model_auth
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from common.db import fields
from common.decorators import methodBooleanPropertyTrue
from common.utils import StringXT
import ghosts.constants as constants


class ViewSettings(models.Model):
    name = models.CharField(max_length=8, primary_key=True)
    radius_horizontal = models.FloatField()    # in meters
    radius_vertical = models.FloatField()    # in meters
    min_query_interval = models.IntegerField()
    max_query_interval = models.IntegerField()
    max_messages_per_query = models.IntegerField()

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = 'ghosts_settings'
        verbose_name_plural = 'view settings'


class Image(models.Model):
    type = models.CharField(max_length=1,
        choices=[
            (constants.IMAGE_TYPE_SMALL[0], 'Small'),
            (constants.IMAGE_TYPE_LARGE[0], 'Large')
        ])
    url = models.URLField()

    def __unicode__(self):
        return u'%s %s' % (self.type, self.url)

    class Meta:
        db_table = 'ghosts_image'


class Profile(models.Model):
    # this one-to0one reference is how our user is tied to the core authentication user
    user = models.OneToOneField(model_auth.User)
    notes = models.CharField(max_length=512, null=True, blank=True)

    image_thumb = models.ForeignKey(Image, null=True, blank=True, related_name='profile_images_thumb')
    image_full = models.ForeignKey(Image, null=True, blank=True, related_name='profile_images_full')

    # these are "current" coordinates
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)
    altitude = models.FloatField(null=True, blank=True)

    # the following are for marking and noting profiles that we are most likely blacklisting (flag). The note is just a reminder.
    # note: User.is_active should be used in queries.  These just provide ancillary information
    status = models.CharField(max_length=1, default=constants.MODEL_STATUS_OKAY,
        choices=[
            (constants.MODEL_STATUS_OKAY, 'Okay'),
            (constants.MODEL_STATUS_FLAG, 'Flagged')
        ])

    def __unicode__(self):
        return self.user.username

    class Meta:
        db_table = 'ghosts_profile'

    ''' Public Interface '''
    @staticmethod
    def get_user_profile(user):
        """
        User's method throws exception if it does not exist. This guy
        will create an instance if it does not exist already. Does not save!
        """
        try:
            return user.get_profile()
        except ObjectDoesNotExist:
            profile=Profile()
            profile.user=user
        return profile

    def add_note(self, note, save=False):
        if StringXT.is_empty(self.notes):
            self.notes = note
        else:
            self.notes += ('|' + note)
        if save:
            self.save()


class Message(models.Model):
    """
    Notes:
    - coordinates and NULL: we support NULL for our special messages that "follow" users.
        For example - first time messages, notifications, advertisements, etc.
    """
    user = models.ForeignKey(model_auth.User)
    text = models.CharField(max_length=getattr(settings, 'MESSAGE_MAX_LENGTH', 512))
    image_thumb = models.ForeignKey(Image, null=True, blank=True, related_name='message_images_thumb')
    image_full = models.ForeignKey(Image, null=True, blank=True, related_name='message_images_full')
    color = models.PositiveIntegerField(null=True, blank=True)
    latitude = models.FloatField(db_index=True, null=True, blank=True)
    longitude = models.FloatField(db_index=True, null=True, blank=True)
    altitude = models.FloatField(db_index=True, null=True, blank=True)
    created = models.DateTimeField()
    expiration = models.DateTimeField(null=True, blank=True)
    type = fields.EnumField(max_length=1, blank=True, suppress_blank_label=True, default=constants.MESSAGE_TYPE_USER[0],
        choices=[
            (constants.MESSAGE_TYPE_USER[0], 'User'),
            (constants.MESSAGE_TYPE_WELCOME[0], 'Welcome'),
            (constants.MESSAGE_TYPE_NOTIFICATION[0], 'Notification'),
            (constants.MESSAGE_TYPE_ADVERTISEMENT[0], 'Advertisement')
        ])
    status = fields.EnumField(max_length=1, default=constants.MODEL_STATUS_OKAY,
        choices=[
            (constants.MODEL_STATUS_OKAY, 'Okay'),
            (constants.MODEL_STATUS_PENDING, 'Pending'),
            (constants.MODEL_STATUS_DEAD, 'Deleted'),
            (constants.MODEL_STATUS_FLAG, 'Flagged')
        ])

    def __unicode__(self):
        return self.text

    def text_ellipsed(self, count=64):
        return StringXT.ellipsis(self.text, count)

    class Meta:
        db_table = 'ghosts_message'

    @methodBooleanPropertyTrue
    def expired(self):
        if self.expiration:
            # note: using datetime directly as opposed to factory 'cause of import looping problem
            return self.expiration <= datetime.utcnow()
        else:
            return False


class Rating(models.Model):
    message = models.ForeignKey(Message)
    user = models.ForeignKey(model_auth.User)
    value = models.SmallIntegerField(choices=[(1,'Like'), (-1,'Dislike')])

    def __unicode__(self):
        return unicode(self.value)

    class Meta:
        db_table = 'ghosts_rating'
        # note: is not possible to reference foreign objects (sql constraint). We will do in code anyway
        # unique_together = ['message', 'profile']


class Flagged(models.Model):
    message = models.ForeignKey(Message)
    user = models.ForeignKey(model_auth.User)
    processed = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.username

    class Meta:
        db_table = 'ghosts_flagged'
        verbose_name_plural = 'flagged'


class Feedback(models.Model):
    """
    Feedback table facilitates both feedback we get from applications as well as feedback/support
    requests that we get from the website:
    App - user has to be logged in and user will be populated
    Website - user will not be logged in but must provide an email
    """
    text = models.CharField(max_length=getattr(settings, 'FEEDBACK_MAX_LENGTH', 1250))
    user = models.ForeignKey(model_auth.User, null=True, blank=True)
    name = models.CharField(max_length=getattr(settings, 'CONTACT_NAME_MAX_LENGTH', 50), null=True, blank=True)
    email = models.EmailField(max_length=getattr(settings, 'EMAIL_MAX_LENGTH', 50), null=True, blank=True)
    device = models.CharField(max_length=50, null=True, blank=True)
    created = models.DateTimeField()
    origin = fields.EnumField(max_length=1, blank=True, suppress_blank_label=True, default=constants.FEEDBACK_ORIGIN_UNKNOWN,
        choices=[
            (constants.FEEDBACK_ORIGIN_UNKNOWN, 'Unknown'),
            (constants.FEEDBACK_ORIGIN_APP, 'Application'),
            (constants.FEEDBACK_ORIGIN_WEB, 'Website'),
        ])
    flag = fields.EnumField(max_length=1, blank=True, suppress_blank_label=True, default=constants.FLAG_NONE,
        choices=[
            (constants.FLAG_NONE, 'None'),
            (constants.FLAG_SUSPECT_CONTENT, 'Content?'),
            (constants.FLAG_SUSPECT_ORIGIN, 'Origin?'),
            (constants.FLAG_SUSPECT_SPAM, 'Spam?'),
            (constants.FLAG_CONFIRMED_CONTENT, 'Content!'),
            (constants.FLAG_CONFIRMED_ORIGIN, 'Origin!'),
            (constants.FLAG_CONFIRMED_SPAM, 'Spam!'),
        ])
    action = fields.EnumField(max_length=1, blank=True, suppress_blank_label=True, default=constants.ACTION_NONE,
        choices=[
            (constants.ACTION_NONE, 'None'),
            (constants.ACTION_REPLIED, 'Replied'),
            (constants.ACTION_FLAGGED, 'Flagged'),
            (constants.ACTION_CLOSED, 'Closed'),
        ])

    def __unicode__(self):
        return self.text

    class Meta:
        db_table = 'ghosts_feedback'
        verbose_name_plural = 'feedback'


class Reserved(models.Model):
    word = models.CharField(max_length=32, null=False, blank=False)

    def __unicode__(self):
        return self.word

    class Meta:
        db_table = 'ghosts_reserved'
        verbose_name_plural = 'reserved'


''' Sketches '''
class Campaign(models.Model):
    """
    This is a concept at the moment and a sort of sketch.  The idea is an ad campaign.
    A campaign contains campaign meta data and one or more messages.
    """
    # note: Message will carry a ForeignKey(Campaign)
    # note: Campaign will carry link to a company
    name = models.CharField(max_length=32)
    description = models.CharField(max_length=1024, null=True, blank=True)
    code = models.CharField(max_length=32)
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)
    altitude = models.FloatField(null=True, blank=True)
    radius = models.FloatField(null=True, blank=True)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()

    def __unicode__(self):
        return u'%s: %s' % (self.name, StringXT.ellipsis(self.description, 100))

    class Meta:
        db_table = 'ghosts_campaign'

