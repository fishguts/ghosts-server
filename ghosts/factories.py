"""
created: 3/24/13
@author: curt
"""
from datetime import datetime, timedelta
from django.core.exceptions import ObjectDoesNotExist
from ghosts import models as model_ghosts
from ghosts import constants as app_constants
from ghosts.forms import ContactForm
from ghosts.security import text_contains_flagged_content


''' Primitive Factories '''
def create_timestamp():
    return datetime.utcnow()

def create_clone_name(name):
    return name + ' clone'


''' Intermediate data object representations to model objects '''
def data_to_message_post(data, current_user):
    message=model_ghosts.Message()
    message.user=current_user
    message.text=data['text']
    message.longitude=data['longitude']
    message.latitude=data['latitude']
    message.altitude=data['altitude']
    message.created=create_timestamp()
    if 'expiration' in data:
        message.expiration=message.created+timedelta(seconds=data['expiration'])
    return message


def data_to_message_delete(data, current_user):
    """
    Technically this isn't a factory. But it's a model operation and they like to be together.
    """
    message=model_ghosts.Message.objects.get(id=data['messageId'])
    if message.user.id!=current_user.id:
        raise Exception('Message delete attempt failed: not owner {0}', current_user.username)
    return message


def data_to_rating(data, current_user):
    message_id=data['messageId']
    value=1 if data['rating']>0 else -1
    try:
        rating=model_ghosts.Rating.objects.get(message__pk=message_id, user=current_user)
    except ObjectDoesNotExist:
        rating=model_ghosts.Rating(message_id=message_id, user=current_user)
    rating.value=value
    return rating


def data_to_flag(data, current_user):
    message_id=data['messageId']
    try:
        flag=model_ghosts.Flagged.objects.get(message__pk=message_id, user=current_user)
    except ObjectDoesNotExist:
        flag=model_ghosts.Flagged(message_id=message_id, user=current_user)
    return flag


def data_to_feedback(data, current_user):
    feedback=model_ghosts.Feedback()
    feedback.user=current_user
    feedback.text=data['text']
    feedback.device=data['device']
    feedback.origin=app_constants.FEEDBACK_ORIGIN_APP
    feedback.created=create_timestamp()
    if text_contains_flagged_content(feedback.text):
        feedback.flag=app_constants.FLAG_SUSPECT_CONTENT
    return feedback


''' form to object deserializers '''
def form_to_contact(request):
    form = ContactForm(request.POST)
    if not form.is_valid():
        raise ValueError('Attempt to validate contact data failed')
        # we are good - proceed with creating feedback record
    try:
        feedback=model_ghosts.Feedback()
        feedback.name=form.cleaned_data['name']
        feedback.email=form.cleaned_data['email']
        feedback.text=form.cleaned_data['message']
        feedback.origin=app_constants.FEEDBACK_ORIGIN_WEB
        feedback.created=create_timestamp()
        if text_contains_flagged_content(feedback.text):
            feedback.flag=app_constants.FLAG_SUSPECT_CONTENT
        return feedback
    except Exception as exception:
        raise ValueError('Failed to convert json to feedback: {0}'.format(exception))

