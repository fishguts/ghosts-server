"""
Created on Jul 23, 2012

@author: curt
"""
from django.contrib import auth
from common.exceptions import StatusException
from common.objects import Status
from common import constants
from registration import RegistrationProfile


''' Public Interface '''
def validate_unauthorized_request(request):
    """
    Ensures that request is from a trusted user agent
    :param request: HTTPRequest object
    :raise: Exception
    """
    _validate_request_user_agent(request)


def validate_authorized_request(request):
    """
    Validates the user agent and authenticates user
    :param request: HTTPRequest object
    :raise: Exception, StatusException
    """
    _validate_request_user_agent(request)
    _validate_request_authorized(request)


def validate_staff_request(request):
    """
    Validates that user is logged in and is registered as as a staff member
    :param request: HTTPRequest object
    :raise: Exception, StatusException
    """
    _validate_request_authorized(request)
    if not request.user.is_staff:
        raise StatusException(Status(code=constants.RESPONSE_CODE_AUTH, text='User does not have sufficient permissions'))


def login_user(username, password, request):
    user=auth.authenticate(username=username, password=password)
    if user is None:
        raise StatusException(Status(code=constants.RESPONSE_CODE_FAIL, text='Invalid username or password'))
    elif user.is_active:
        auth.login(request, user)
    else:
        registration=RegistrationProfile.objects.user_to_registration(user)
        if not registration:
            raise StatusException(Status(code=constants.RESPONSE_CODE_FAIL, text='User is no longer active'))
        elif registration.activation_expired():
            raise StatusException(Status(code=constants.RESPONSE_CODE_FAIL, text='Activation window has expired'))
        else:
            raise StatusException(Status(code=constants.RESPONSE_CODE_FAIL, text='User has not been activated'))

    return user


def logout_user(username, request):
    auth.logout(request)


''' Private Interface '''
def _validate_request_user_agent(request):
    user_agent=request.META['HTTP_USER_AGENT']
    if not user_agent.startswith('Goggles/'):
        # limit max report chars to X just in case we are being messed with
        raise Exception("bad useragent '{0:.30}'".format(user_agent))


def _validate_request_authorized(request):
    if request.user is None or request.user.is_authenticated()==False:
        raise StatusException(Status(code=constants.RESPONSE_CODE_AUTH, text='User is not logged in'))
