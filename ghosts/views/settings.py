"""
Created on Jul 22, 2012

@author: curt
"""
import logging

from django.http import HttpResponseBadRequest
from django.views.decorators.http import require_POST

from common import constants
from common.http import HttpResponseJson
from common.exceptions import StatusException

from ghosts import models
from ghosts.serializers import json as serializer
from ghosts.views import _authentication


@require_POST
def process_get(request):
    try:
        _authentication.validate_unauthorized_request(request)
        # we prefix our internally used view settings with '_'
        settings = models.ViewSettings.objects.all().exclude(name__startswith='_')
        response = serializer.from_settings(settings=settings, status=constants.STATUS_OKAY)
        return HttpResponseJson(response)
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()
