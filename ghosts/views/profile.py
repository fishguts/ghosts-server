"""
Created on Jul 22, 2012

@author: curt
"""
import logging
import os

from django.http import HttpResponse, HttpResponseBadRequest, HttpResponsePermanentRedirect
from django.views.decorators.http import require_POST
from common import constants
from common.http import HttpResponseJson
from common.exceptions import StatusException
from ghosts import validators
from ghosts.views import _authentication
from ghosts.serializers import json as serializer
from project import settings
from registration import RegistrationProxy


@require_POST
def process_register(request):
    try:
        _authentication.validate_unauthorized_request(request)
        data=serializer.to_object(request.body)
        user=RegistrationProxy.register(request, data=data)
        return HttpResponseJson(serializer.from_user(user, constants.STATUS_OKAY))
    except StatusException as error:
        return HttpResponse(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


def process_activate(request, **kwargs):
    """
    A little note concerning why we route results to static webpages. It's pretty simple - our website is static.
    If we choose to use a template then it's going to live in our subdomain and it adds complication
    to how we manage our pages.  So, for now it's no nonsense.
    """
    if settings.DEBUG:
        HttpResponsePermanentRedirect.allowed_schemes.append('file')
    try:
        RegistrationProxy.activate(request, **kwargs)
        return HttpResponsePermanentRedirect(os.path.join(settings.WEBSITE_ROOT, 'activation_success.html'))
    except StatusException as error:
        pass    # core will have logged it. Let it go.
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponsePermanentRedirect(os.path.join(settings.WEBSITE_ROOT, 'activation_failure.html'))


@require_POST
def process_login(request):
    try:
        _authentication.validate_unauthorized_request(request)
        data=serializer.to_object(request.body)
        validators.validate_login_object(data)
        user=_authentication.login_user(data['username'], data['password'], request)
        return HttpResponseJson(serializer.from_user(user, constants.STATUS_OKAY))
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


@require_POST
def process_logout(request):
    try:
        _authentication.validate_authorized_request(request)
        data=serializer.to_object(request.body)
        validators.validate_logout_object(data)
        _authentication.logout_user(data['username'], request)
        return HttpResponseJson(serializer.from_status(constants.STATUS_OKAY))
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


@require_POST
def process_update(request):
    try:
        _authentication.validate_authorized_request(request)
        return HttpResponseJson("In progress")
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


@require_POST
def process_delete(request):
    try:
        _authentication.validate_authorized_request(request)
        return HttpResponseJson("In progress")
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()

