"""
created: 12/2/12
@author: curt
"""
import logging

from django.http import HttpResponseBadRequest, HttpResponse
from django.views.decorators.http import require_POST
from common.objects import Status
from common import constants
from common.exceptions import StatusException
from common.http import HttpResponseJson
from ghosts import factories, validators
from ghosts.serializers import json as serializer
from ghosts.views import _authentication


@require_POST
def process_feedback(request):
    try:
        _authentication.validate_authorized_request(request)
        data = serializer.to_object(request.body)
        validators.validate_feedback_object(data)
        feedback = factories.data_to_feedback(data, request.user)
        feedback.save()
        return HttpResponseJson(serializer.from_status(constants.STATUS_OKAY))
    except StatusException as error:
        return HttpResponse(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


@require_POST
def process_contact(request):
    try:
        feedback = factories.form_to_contact(request)
        feedback.save()
        response=HttpResponseJson(serializer.from_status(constants.STATUS_OKAY))
    except StatusException as error:
        response=HttpResponse(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))
        response=HttpResponse(serializer.from_status(Status(constants.RESPONSE_CODE_FAIL, 'Sorry, we are unable to process contact submissions at this time.')))
    # chrome (at least) is not very good with dealing with subdomains.
    # We can create a submask if necessary, but I can't see the security issues here.
    response['Access-Control-Allow-Origin']='*'
    return response
