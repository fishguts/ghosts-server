"""
created: 9/9/13
@author: curt
"""

import logging

from django.http import HttpResponseBadRequest
from django.views.decorators.http import require_POST
from common.http import HttpResponseJson
from common import constants
from common.exceptions import StatusException
from ghosts import queries, validators, conditioners
from ghosts.serializers import json as serializer
from ghosts.views import _authentication


@require_POST
def process_get(request):
    try:
        _authentication.validate_staff_request(request)
        data=serializer.to_object(request.body)
        validators.validate_perimeter_query_object(data)
        conditioners.condition_perimeter_messages_object(data)
        messages=queries.get_perimeter_messages(data)
        response=serializer.from_messages(messages=messages, current_user=request.user, status=constants.STATUS_OKAY)
        return HttpResponseJson(response)
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()

