"""
Created on Jul 22, 2012

@author: curt
"""
import logging

from django.http import HttpResponseBadRequest
from django.views.decorators.http import require_POST
from common.http import HttpResponseJson
from common import constants
from common.exceptions import StatusException
from ghosts import queries, validators, factories, conditioners
from ghosts.serializers import json as serializer
from ghosts.views import _authentication


@require_POST
def process_get(request):
    try:
        _authentication.validate_unauthorized_request(request)
        data=serializer.to_object(request.body)
        validators.validate_message_query_object(data)
        messages=queries.get_request_messages(data)
        response=serializer.from_messages(messages=messages, current_user=request.user, status=constants.STATUS_OKAY,
            latitude=data['latitude'], longitude=data['longitude'], altitude=data['altitude'])
        return HttpResponseJson(response)
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


@require_POST
def process_put(request):
    try:
        _authentication.validate_authorized_request(request)
        data=serializer.to_object(json_text=request.body)
        data=conditioners.condition_message_post_object(data)
        validators.validate_message_post_object(data)
        message=factories.data_to_message_post(data=data, current_user=request.user)
        message.save()
        response=serializer.from_message(message=message, current_user=request.user, status=constants.STATUS_OKAY)
        return HttpResponseJson(response)
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


@require_POST
def process_rate(request):
    try:
        _authentication.validate_authorized_request(request)
        data=serializer.to_object(json_text=request.body)
        validators.validate_message_rate_object(data)
        rating=factories.data_to_rating(data=data, current_user=request.user)
        rating.save()
        response=serializer.from_message(message=rating.message, current_user=request.user, status=constants.STATUS_OKAY)
        return HttpResponseJson(response)
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


@require_POST
def process_flag(request):
    try:
        _authentication.validate_authorized_request(request)
        data=serializer.to_object(json_text=request.body)
        validators.validate_message_flag_object(data)
        flag=factories.data_to_flag(data=data, current_user=request.user)
        flag.save()
        # in keeping with our convention send back the message.
        response=serializer.from_message(message=flag.message, current_user=request.user, status=constants.STATUS_OKAY)
        return HttpResponseJson(response)
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()


@require_POST
def process_delete(request):
    try:
        _authentication.validate_authorized_request(request)
        data=serializer.to_object(json_text=request.body)
        validators.validate_message_delete_object(data)
        message=factories.data_to_message_delete(data=data, current_user=request.user)
        message.delete()
        response=serializer.from_status(status=constants.STATUS_OKAY)
        return HttpResponseJson(response)
    except StatusException as error:
        return HttpResponseJson(serializer.from_status(error.status))
    except Exception as error:
        logging.getLogger(__name__).error('Exception: {0}'.format(error))

    return HttpResponseBadRequest()

