"""
created: 12/16/12
@author: curt
"""
from datetime import timedelta
from optparse import make_option
import os

from django.conf import settings
from django.contrib.auth import models as models_auth
from django.core.mail import mail_admins
from django.core.management.base import NoArgsCommand, CommandError
from django.template.loader import render_to_string

from common.db import get_project_site
from common.mail import render_to_subject
from ghosts import constants, queries, factories
from ghosts import models as models_ghost
from registration import models as models_registration


class Command(NoArgsCommand):
    """
    Command that reports usage summary:
    """
    # noinspection PyShadowingBuiltins
    help='Reports usage summary for period specified.'
    option_list=NoArgsCommand.option_list+(
        make_option('-n', '--newer',
            action='store', dest='hours', type='int', default=24,
            help="Newer than this value in hours. Defaults to 24."),
        make_option('-e', '--email',
            action='store_true', dest='email', default=False,
            help="Email admins.")
    )

    ''' Command overrides '''
    def handle(self, *args, **options):
        try:
            self._set_options(*args, **options)
            self._build_results()
            self._present_results()
        except CommandError as exception:
            raise exception
        except BaseException as exception:
            raise CommandError(str(exception))


    ''' Utils '''
    def _get_file_size(self, path):
        try:
            return os.path.getsize(path)
        except Exception as error:
            self.stdout.write('reportusage: unable to open {0}: {1}.\n'.format(path, error.message))
        return '[not found]'


    ''' Private interface '''
    def _set_options(self, *args, **options):
        self._verbosity=int(options.get('verbosity', 1))
        self._hours=int(options.get('hours', 24))
        self._email=bool(options.get('email', False))

    def _build_results(self):
        date_to=factories.create_timestamp()
        date_from=date_to-timedelta(hours=self._hours)
        messages=models_ghost.Message.objects.filter(created__gte=date_from)
        messages_suspicious=messages.filter(status=constants.MODEL_STATUS_FLAG)
        messages_expired=queries.get_expired_messages(date_to)
        messages_flagged=models_ghost.Flagged.objects.filter(processed=False)
        feedback=models_ghost.Feedback.objects.filter(created__gte=date_from)
        feedback_suspicious=feedback.exclude(flag=constants.FLAG_NONE)
        feedback_app=feedback.filter(origin=constants.FEEDBACK_ORIGIN_APP)
        feedback_web=feedback.filter(origin=constants.FEEDBACK_ORIGIN_WEB)
        users=models_auth.User.objects.all()
        users_new=users.filter(date_joined__gte=date_from)
        registration_all=models_registration.RegistrationProfile.objects.all()
        registration_expired=models_registration.RegistrationProfile.objects.get_expired_profiles()
        log_error_path=getattr(settings, 'PROJECT_LOG', 'unknown')
        log_error_size=self._get_file_size(log_error_path)
        # noinspection PyTypeChecker
        context={
            'date_from':date_from,
            'date_to':date_to,
            'site':get_project_site(),
            'message_count_total':messages.count(),
            'message_count_flagged':messages_flagged.count(),
            'message_count_suspicious':messages_suspicious.count(),
            'message_count_expired':messages_expired.count(),
            'feedback_count_total':feedback.count(),
            'feedback_count_suspicious':feedback_suspicious.count(),
            'feedback_count_app':feedback_app.count(),
            'feedback_count_web':feedback_web.count(),
            'user_count_total':users.count(),
            'user_count_new':users_new.count(),
            'registration_count_open':registration_all.count()-registration_expired.count(),
            'registration_count_expired':registration_expired.count(),
            'log_error_path':log_error_path,
            'log_error_size':log_error_size
        }
        self.result_title=render_to_subject('ghosts/email_subject_usage.txt', context)
        self.result_details=render_to_string('ghosts/email_body_usage.txt', context)


    def _present_results(self):
        self.stdout.write(self.result_details)
        if self._email:
            try:
                mail_admins(self.result_title, self.result_details)
            except BaseException as exception:
                raise CommandError('Failed to email admins: {0}'.format(exception))

