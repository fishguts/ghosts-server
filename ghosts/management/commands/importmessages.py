"""
created: 8/13/13
@author: curt
"""
from optparse import make_option
from django.contrib.auth import models as model_auth
from django.core.management.base import NoArgsCommand, CommandError
from common import builtins
from common.utils import dateutils, StringXT
from ghosts import factories, conditioners, validators
from ghosts import models as model_ghosts
from ghosts.serializers import csv as serializer

class Command(NoArgsCommand):
    """
	ImportMessagesCommand - Imports messages from CSV file specified in param
	"""

    # noinspection PyShadowingBuiltins
    help="Import messages from csv file.  Format is as follows: user, message, latitude, longitude, altitude, type [,created, expired]."
    option_list=NoArgsCommand.option_list+(
        make_option('-n', '--dry-run',
            dest="dry_run", action="store_true", default=False,
            help="Perform dry run and dump results to console (use verbose)."),
        make_option('-d', '--delete',
            dest="delete", action="store_true", default=False,
            help="Performs delete on matching records."),
        make_option('-f', '--file',
            dest='file', type="string",
            help="CSV file from which will import messages."),
        make_option('-c', '--created',
            dest='created', type="string",
            help="Create date (M/D/Y H:M[:S] or Y/M/D H:M[:S]) to defer to if none is not indicated in CSV. Default is utcnow()."),
    )

    ''' Command overrides '''

    def handle(self, *args, **options):
        self._map=[]
        self._matches={}
        self._set_options(*args, **options)
        if self._validate_options():
            try:
                if self._verbosity>1:
                    self.stdout.write('parsing "{0}"\n'.format(self._path))
                builtins.fopen(path=self._path, mode='r', callback=self._import_data)
                self._build_matches()
                if self._delete:
                    self._delete_messages()
                else:
                    self._import_messages()
            except CommandError as exception:
                raise exception
            except BaseException as exception:
                raise CommandError(str(exception))


    ''' Private interface '''

    def _set_options(self, *args, **options):
        self._verbosity=int(options.get('verbosity', 1))
        self._dry_run=bool(options.get('dry_run'))
        self._delete=bool(options.get('delete'))
        self._path=options.get('file')
        self._created=options.get('created')

    def _validate_options(self):
        if not self._path:
            self.stderr.write('Must specify file to import (-f path).\n')
            return False
        if self._created:
            self._created=dateutils.text_to_date(self._created)
        return True


    def _import_data(self, _file):
        errors=[]   # we will collect errors and raise all of them at once
        self._map=serializer.to_messages(rows=_file, header=True)
        for data in self._map:
            try:
                conditioners.condition_message_post_object(data)
                validators.validate_message_post_object(data)
                data['user']=model_auth.User.objects.get(username=data['ownerId'])
            except Exception as exception:
                errors.append("'{0}' - {1}".format(StringXT.ellipsis(data['text'], 64), exception))
        if len(errors)>0:
            raise Exception('\n'.join(errors))


    def _build_matches(self):
        for data in self._map:
            messages=model_ghosts.Message.objects.filter(user=data['user'], text=data['text'], latitude=data['latitude'], altitude=data['altitude'])
            if len(messages)>0:
                self._matches[id(data)]=messages[0]
                if len(messages)>1:
                    self.stderr.write('WARNING: {0} messages exist in db for user={1}, text="{2}"\n'.format(
                        len(messages), data['ownerId'], StringXT.ellipsis(data['text'], 64)))


    def _import_messages(self):
        if self._verbosity>1:
            pending_action='eligible' if self._dry_run else 'queued'
            self.stdout.write("{0} messages {1} for addition. {2} are duplicates\n".format(len(self._map)-len(self._matches), pending_action, len(self._matches)))
        for data in self._map:
            message=self._matches.get(id(data), None)
            if message:
                if self._verbosity>2:
                    self.stdout.write('Bypassing as duplicate: user={0}, text="{1}"\n'.format(message.user.username, message.text_ellipsed(64)))
            else:
                if self._verbosity>2:
                    pending_action='Eligible for addition' if self._dry_run else 'Adding'
                    self.stdout.write('{0}: user={1}, text="{2}"\n'.format(pending_action, data['ownerId'], StringXT.ellipsis(data['text'], 64)))
                if not self._dry_run:
                    message=factories.data_to_message_post(data=data, current_user=data['user'])
                    message.type=data['type']
                    if data.has_key('created'):
                        message.created=data['created']
                    elif self._created:
                        message.created=self._created
                    message.save()


    def _delete_messages(self):
        if self._verbosity>1:
            pending_action='eligible' if self._dry_run else 'queued'
            self.stdout.write("{0} messages {1} for deletion.\n".format(len(self._matches), pending_action))
        for message in self._matches.values():
            if self._verbosity>2:
                pending_action='Eligible for deletion' if self._dry_run else 'Deleting'
                self.stdout.write('{0}: user={1}, text="{2}"\n'.format(pending_action, message.user.username, message.text_ellipsed(64)))
            if not self._dry_run:
                message.delete()
