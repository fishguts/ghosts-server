"""
created: 12/16/12
@author: curt
"""
from datetime import timedelta
from optparse import make_option

from django.conf import settings
from django.core.management.base import NoArgsCommand, CommandError

from ghosts import queries, factories


class Command(NoArgsCommand):
    """
    Command that cleans up our dead messages:
        - messages that have expired
        - flagged for deletion
    """
    # noinspection PyShadowingBuiltins
    help = 'Deletes messages that have expired {0} or more days ago or are flagged for deletion.'.format(getattr(settings, 'MESSAGE_POST_EXPIRATION_DAYS', 7))
    option_list = NoArgsCommand.option_list + (
        # being consistent with core commands
        make_option('-n', '--dry-run',
            action='store_true', dest='dry_run', default=False,
            help="Report only. No DB modifications."),
        )

    ''' Command overrides '''
    def handle_noargs(self, **options):
        self._set_options(**options)
        self._delete_expired()
        self._delete_flagged()


    ''' Private interface '''
    def _set_options(self, **options):
        self._verbosity=int(options.get('verbosity', 1))
        self._dry_run=bool(options.get('dry_run', False))

    def _delete_expired(self):
        death_row_days=getattr(settings, 'MESSAGE_POST_EXPIRATION_DAYS', 7)
        expiration_date=factories.create_timestamp()-timedelta(days=death_row_days)
        query_set=queries.get_expired_messages(expiration_date)

        if self._verbosity>1:
            self.stdout.write('{0} messages expired as of {1}.\n'.format(query_set.count(), expiration_date))

        if self._verbosity>2:
            for message in query_set:
                self.stdout.write('username="{0}", expiration="{1}", text="{2}"\n'.format(message.user.username, message.expiration, message.text_ellipsed(20)))

        if not self._dry_run:
            try:
                if self._verbosity>1:
                    self.stdout.write('deleting {0} messages.\n'.format(query_set.count()))
                query_set.delete()
            except Exception as exception:
                raise CommandError('Error thrown while deleting expired messages: {0}'.format(exception))


    def _delete_flagged(self):
        # todo: makes sense that we eventually get rid of them.  Determine how long we want to keep them around
        pass

