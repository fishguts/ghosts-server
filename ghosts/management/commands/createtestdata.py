"""
created: 12/16/12
@author: curt
"""
from datetime import timedelta
from optparse import make_option

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import CommandError, NoArgsCommand

from common.db import get_project_site
from ghosts import factories
from ghosts.models import Feedback
from ghosts.tests._testfactories import GhostsTestFactories
from registration import RegistrationProfile


class Command(NoArgsCommand):
    """
    Command that reports usage summary:
    """
    # noinspection PyShadowingBuiltins
    help = 'Creates data for testing.'
    option_list = NoArgsCommand.option_list+(
        make_option('-r', '--registration', default=False,
                    action='store_true', dest='registration',
                    help="Create registration profiles."),
        make_option('-p', '--profiles', default=False,
                    action='store_true', dest='profiles',
                    help="Create user profiles."),
        make_option('-m', '--messages', default=False,
                    action='store_true', dest='messages',
                    help="Create messages."),
        make_option('-f', '--feedback', default=False,
                    action='store_true', dest='feedback',
                    help="Create feedback."),
        make_option('-l', '--flagged', default=False,
                    action='store_true', dest='flagged',
                    help="Create flagged messages."),
        make_option('-d', '--delete', default=False,
                    action='store_true', dest='delete',
                    help="Delete test data."),
    )

    ''' Command overrides '''
    def handle(self, *args, **options):
        if not getattr(settings, 'DEBUG'):
            raise CommandError('Should not be run in non-DEBUG environment.')

        self._profiles = []
        self._registration = []
        self._messages = []
        self._set_options(*args, **options)
        if self._validate_options():
            try:
                if self._build_registration:
                    self._build_registration_objects()
                if self._build_profiles:
                    self._build_profile_objects()
                if self._build_messages:
                    self._build_message_objects()
                if self._build_feedback:
                    self._build_feedback_objects()
                if self._build_flagged:
                    self._build_flagged_objects()
                if self._delete_data:
                    self._delete_data_objects()
            except CommandError as exception:
                raise exception
            except BaseException as exception:
                raise CommandError(str(exception))


    ''' Private interface '''
    def _set_options(self, *args, **options):
        self._verbosity = int(options.get('verbosity', 1))
        self._build_registration = bool(options.get('registration', False))
        self._build_profiles = bool(options.get('profiles', False))
        self._build_messages = bool(options.get('messages', False))
        self._build_feedback = bool(options.get('feedback', False))
        self._build_flagged = bool(options.get('flagged', False))
        self._delete_data = bool(options.get('delete', False))

    def _validate_options(self):
        if not (self._build_registration or self._build_profiles or self._build_messages or self._build_flagged or self._build_feedback):
            if self._delete_data:
                return True
            else:
                self.stderr.write('Must specify which object types you want to build.\n')
                return False
        if self._delete_data:
            self.stderr.write('Delete is mutually exclusive.\n')
            return False
        if self._build_messages and not self._build_profiles:
            self.stderr.write('Must create profiles when creating messages.\n')
            return False
        if self._build_feedback and not self._build_profiles:
            self.stderr.write('Must create profiles when creating feedback.\n')
            return False
        if self._build_flagged and not self._build_messages:
            self.stderr.write('Must create messages when creating flagged.\n')
            return False
        return True


    ''' Model factories '''
    def _build_registration_objects(self):
        """
        Creates 10 registration profiles: 5 pending and 5 expired
        """
        site=get_project_site()
        if self._verbosity>1:
            self.stdout.write('Creating 10 registration profiles: 5 pending and 5 expired.\n')

        for index in range(0, 10):
            username = 'test_registration_{0:02}'.format(index)
            email = '{0}@test.com'.format(username)
            if not User.objects.filter(username=username).exists():
                user=RegistrationProfile.objects.create_inactive_user(username=username, email=email, password='password', site=site, send_email=False)
                self._registration.append(user)
                if index>=5:
                    user.date_joined=factories.create_timestamp()-timedelta(days=(getattr(settings, 'REGISTRATION_ACTIVATION_DAYS', 7)+1))
                    user.save()
                if self._verbosity>2:
                    self.stdout.write('Created registration: username="{0}", email="{1}", joined="{2}"\n'.format(user.username, user.email, user.date_joined))


    def _build_profile_objects(self):
        if self._verbosity>1:
            self.stdout.write('Creating 10 user profiles.\n')

        for index in range(0, 10):
            username = 'test_profile_{0:02}'.format(index)
            email = '{0}@test.com'.format(username)
            if not User.objects.filter(username=username).exists():
                user = GhostsTestFactories.create_user_object(name=username, email=email)
                self._profiles.append(user)
                if self._verbosity>2:
                    self.stdout.write('Created profile: username="{0}", email="{1}", joined="{2}"\n'.format(user.username, user.email, user.date_joined))


    def _build_message_object(self, user, text, expires=None):
        message=GhostsTestFactories.create_message_object(user=user, text=text, expires=expires)
        self._messages.append(message)
        if self._verbosity>2:
            self.stdout.write('Created message: username="{0}", text="{1}", expiration="{2}"\n'.format(message.user.username, message.text, message.expiration))
        return message


    def _build_message_objects(self):
        if self._verbosity>1:
            self.stdout.write('Creating 3 messages for every profile created: no-expiration, will-expire, expired.\n')

        expiration_future=factories.create_timestamp()+timedelta(days=1)
        expiration_past=factories.create_timestamp()-timedelta(days=getattr(settings, 'REGISTRATION_ACTIVATION_DAYS', 7)+1)
        for profile in self._profiles:
            self._build_message_object(profile, text='test: no expiration')
            self._build_message_object(profile, text='test: will expire', expires=expiration_future)
            self._build_message_object(profile, text='test: expired', expires=expiration_past)


    def _build_feedback_objects(self):
        profiles_app=self._profiles[0:len(self._profiles)/2]
        profiles_web=self._profiles[len(profiles_app):]
        if self._verbosity>1:
            self.stdout.write('Creating {0} application feedback objects and {1} website feedback objects.\n'.format(len(profiles_app), len(profiles_web)))

        # application
        for profile in profiles_app:
            feedback= GhostsTestFactories.create_feedback_app_object(user=profile)
            if self._verbosity>2:
                self.stdout.write('Created app feedback: username="{0}", text="{1}", expiration="{2}"\n'
                    .format(feedback.user.username, feedback.text, feedback.origin))
        # website
        for profile in profiles_web:
            feedback= GhostsTestFactories.create_feedback_web_object(name=profile.username, email=profile.email)
            if self._verbosity>2:
                self.stdout.write('Created web feedback: name="{0}", email="{1}", text="{2}", expiration="{3}"\n'
                    .format(feedback.name, feedback.email, feedback.text, feedback.origin))


    def _build_flagged_objects(self):
        messages=self._messages[0:len(self._messages)/2]
        if self._verbosity>1:
            self.stdout.write('Creating {0} Flagged objects.\n'.format(len(messages)))

        user_index=0
        for message in messages:
            flag= GhostsTestFactories.create_flag_object(user=self._profiles[user_index%len(self._profiles)], message=message)
            if self._verbosity>2:
                self.stdout.write('Created flagged: username="{0}", message="{1}""\n'.format(flag.user.username, flag.message.text))
            user_index+=1


    def _delete_data_objects(self):
        if self._verbosity>1:
            self.stdout.write('Deleting all test data.\n')

        for index in range(0, 10):
            # registration
            username='test_registration_{0:02}'.format(index)
            try:
                profile=RegistrationProfile.objects.get(user__username=username)
                if self._verbosity>1:
                    self.stdout.write('Deleting registration profile: username="{0}".\n'.format(username))
                RegistrationProfile.objects.delete_profile(profile)
            except ObjectDoesNotExist:
                pass

            # profile
            username='test_profile_{0:02}'.format(index)
            try:
                profile=User.objects.get(username=username)
                if self._verbosity>1:
                    self.stdout.write('Deleting user profile: username="{0}".\n'.format(username))
                profile.delete()
            except ObjectDoesNotExist:
                pass

            # feedback
            feedbacks=Feedback.objects.filter(name=username)
            for feedback in feedbacks:
                if self._verbosity>1:
                    self.stdout.write('Deleting feedback: username="{0}".\n'.format(feedback.name))
                feedback.delete()
