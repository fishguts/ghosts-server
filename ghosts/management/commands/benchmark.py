"""
created: 12/16/12
@author: curt
"""
from datetime import timedelta
from optparse import make_option
import random

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import CommandError, NoArgsCommand
from common.debug import benchmark

from ghosts import factories, constants, queries
from ghosts.tests._testfactories import GhostsTestFactories


class Command(NoArgsCommand):
    """
    Command that reports usage summary:
    """
    # noinspection PyShadowingBuiltins
    help = 'Creates data for testing.'
    option_list = NoArgsCommand.option_list+(
        make_option('-m', '--message-read', default=False,
                    action='store_true', dest='message_read',
                    help="Benchmark message read performance."),
        make_option('-c', '--count', type="int", default=1000,
                    action='store', dest='count',
                    help="Number of model objects to create."),
        make_option('-i', '--iterations', type="int", default=1000,
                    action='store', dest='iterations',
                    help="Number of times to query the model."),
    )

    ''' Command overrides '''
    def handle(self, *args, **options):
        if not getattr(settings, 'DEBUG'):
            raise CommandError('Should not be run in non-DEBUG environment.')

        self._build_messages=False
        self._set_options(*args, **options)
        if self._validate_options():
            try:
                if self._build_messages:
                    self._build_message_objects()
                if self._benchmark_message_read:
                    self._perform_message_read()
            except CommandError as exception:
                raise exception
            except BaseException as exception:
                raise CommandError(str(exception))
            finally:
                self._delete_data_objects()


    ''' Private interface '''
    def _set_options(self, *args, **options):
        self._verbosity = int(options.get('verbosity', 1))
        self._object_count = int(options.get('count'))
        self._benchmark_iterations = int(options.get('iterations'))
        self._benchmark_message_read = bool(options.get('message_read', False))

    def _validate_options(self):
        if not self._benchmark_message_read:
            self.stderr.write('Must specify which object types you want to benchmark.\n')
            return False
        # for now all benchmarks includes messages. Modify if this changes.
        self._build_messages=True
        return True


    ''' Model factories '''
    def _build_profile_object(self, username):
        email=username+"@domain.com"
        if self._verbosity>2:
            self.stdout.write('Creating user: username="{0}", email="{1}".\n'.format(username, email))
        return GhostsTestFactories.create_user_object(name=username, email=email)


    def _build_message_object(self, user, type, text, index, expires=None):
        ratio=float(index)/float(self._object_count)
        latitude=30+ratio*1.0
        longitude=-40*ratio*1.0
        altitude=0+100*ratio
        if self._verbosity>2:
            self.stdout.write('Creating message: username="{0}", type="{1}", text="{2}", expiration="{3}"\n'.format(user.username, type, text, expires))
        message=GhostsTestFactories.create_message_typed_object(user=user, type=type, text=text, expires=expires, latitude=latitude, longitude=longitude, altitude=altitude)
        return message


    def _build_message_objects(self):
        index=0
        if self._verbosity>1:
            self.stdout.write('Creating test messages: count={0}.\n'.format(self._object_count))

        profile=self._build_profile_object('benchmark_message_user')
        expiration_future=factories.create_timestamp()+timedelta(days=1)
        expiration_past=factories.create_timestamp()-timedelta(days=getattr(settings, 'REGISTRATION_ACTIVATION_DAYS', 7)+1)
        while index<self._object_count:
            if index==0:
                self._build_message_object(user=profile, type=constants.MESSAGE_TYPE_WELCOME[0], text='benchmark: welcome message', index=index)
            elif index==1 or not index%50:
                self._build_message_object(user=profile, type=constants.MESSAGE_TYPE_NOTIFICATION[0], text='benchmark: notification message', index=index)
            elif index==2 or not index%51:
                self._build_message_object(user=profile, type=constants.MESSAGE_TYPE_ADVERTISEMENT[0], text='benchmark: advertisement message', index=index)
            elif not index%10:
                self._build_message_object(user=profile, type=constants.MESSAGE_TYPE_USER[0], text='benchmark: user message - past expiration', index=index, expires=expiration_past)
            elif not index%11:
                self._build_message_object(user=profile, type=constants.MESSAGE_TYPE_USER[0], text='benchmark: user message - future expiration', index=index, expires=expiration_future)
            else:
                self._build_message_object(user=profile, type=constants.MESSAGE_TYPE_USER[0], text='benchmark: user message', index=index)
            index+=1


    def _perform_message_read(self):
        self._message_read_index=0
        if self._verbosity>1:
            self.stdout.write('Benchmarking message read: message_count={0}, read_count={1}.\n'.format(self._object_count, self._benchmark_iterations))
        def callback():
            ratio=float(self._message_read_index)/float(self._benchmark_iterations)
            latitude=30.0+0.0*ratio
            longitude=-40+0.0*ratio
            altitude=0.0+0*ratio
            request_data={
                'view':'radial',
                'latitude':latitude,
                'longitude':longitude,
                'altitude':altitude,
            }
            if not self._message_read_index%20:
                request_data['welcome']=True
            queries.get_request_messages(request_data)
            self._message_read_index+=1
        benchmark.time_method(iterations=self._benchmark_iterations, callback=callback)

    def _delete_data_objects(self):
        if self._verbosity>1:
            self.stdout.write('Deleting test data.\n')

        users=User.objects.all().filter(username__in=['benchmark_message_user'])
        for user in users:
            try:
                if self._verbosity>1:
                    self.stdout.write('Deleting user: username="{0}".\n'.format(user.username))
                user.delete()
            except ObjectDoesNotExist:
                self.stderr('Failed to delete user: username="{0}.\n'.format(user.username))
