"""
created: 12/16/12
@author: curt
"""
from optparse import make_option

from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import NoArgsCommand, CommandError
from common.db import get_project_site

from ghosts import contact


class Command(NoArgsCommand):
    # noinspection PyShadowingBuiltins
    help = "Renders templates for review. May optionally email them to an address."
    option_list = NoArgsCommand.option_list+(
        make_option('-e', '--email', default=None, dest='email',
                    help="Email address to which email templates are sent."),
    )

    ''' Command overrides '''
    def handle(self, *args, **options):
        if not getattr(settings, 'DEBUG'):
            raise CommandError('Should not be run in non-DEBUG environment.')

        self._set_options(*args, **options)
        try:
            if self._email:
                self._render_email_templates()
        except CommandError as exception:
            raise exception
        except BaseException as exception:
            raise CommandError(str(exception))


    ''' Private interface '''
    def _set_options(self, *args, **options):
        self._verbosity = int(options.get('verbosity', 1))
        self._email = options.get('email')


    ''' Model factories '''
    def _render_email_templates(self):
        site=get_project_site()
        if self._verbosity>1:
            self.stdout.write("Generating email templates and sending to 'testcase': {0}.\n".format(self._email))

        # use existing test user. We will temporarily modify his email address
        user=User.objects.get(username='testcase')
        user.email=self._email

        # render templates
        if self._verbosity>1:
            self.stdout.write("Generating activation email.\n")
        contact.send_activation_email(user, '[dummykey]', site)

        if self._verbosity>1:
            self.stdout.write("Generating activated email.\n")
        contact.send_activated_email(user, site)

        if self._verbosity>1:
            self.stdout.write("Generating account warning email.\n")
        contact.send_account_warning_email(user)

        if self._verbosity>1:
            self.stdout.write("Generating account canceled email.\n")
        contact.send_account_canceled_email(user)

        if self._verbosity>1:
            self.stdout.write("Generating account revoked email.\n")
        contact.send_account_revoked_email(user)
