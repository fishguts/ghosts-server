"""
created: 11/12/12
@author: curt

Collection of functionality that listens for manage.py signals
and makes updates to our model.
"""
from django.conf import settings
from django.contrib.sites import models as model_site
from django.contrib.auth import models as model_auth
from django.db.models import signals
from ghosts import models as model_ghosts
from ghosts.management import _data


def update_default_site(created_models, verbosity, db, **kwargs):
    """
    Modifies the default site record written by django.contrib.sites.management
    """
    if verbosity>0:
        print "Project: updating our site record"
    if model_site.Site in created_models:
        name = getattr(settings, 'PROJECT_NAME')
        host = getattr(settings, 'PROJECT_HOST')
        try:
            site = model_site.Site.objects.get(pk=settings.SITE_ID)
            if verbosity>0:
                print "Project: updating default site - {0}|{1}".format(name, host)
            site.name = name
            site.domain = host
            site.save(using=db)
        except:
            print "WARNING: creating default site? Should be created by site management"
            model_site.Site(pk=settings.SITE_ID, domain=host, name=name).save(using=db)

    model_site.Site.objects.clear_cache()


def create_default_setting(verbosity, **kwargs):
    if verbosity>0:
        print "Project: updating view settings"
    for view in _data.DEFAULT_VIEW_SETTINGS:
        try:
            row=model_ghosts.ViewSettings.objects.filter(name=view['name'])
            if row.exists():
                if verbosity>1:
                    print "Project: view setting '{0}' already exists".format(view['name'])
            else:
                if verbosity>0:
                    print "Project: creating view settings '{0}'".format(view['name'])
                row=model_ghosts.ViewSettings()
                row.name=view['name']
                row.radius_horizontal=view['radius_horizontal']
                row.radius_vertical=view['radius_vertical']
                row.min_query_interval=view['min_query_interval']
                row.max_query_interval=view['max_query_interval']
                row.max_messages_per_query=view['max_messages_per_query']
                row.save()
        except Exception, ex:
            print "WARNING: failed to create view setting '{0}' - {1}".format(view['name'], ex)


def create_reserved_users(verbosity, **kwargs):
    if verbosity>0:
        print "Project: updating reserved users"
    for user in _data.RESERVED_USERS:
        try:
            row=model_auth.User.objects.filter(username__iexact=user['username'])
            if row.exists():
                if verbosity>1:
                    print "Project: user '{0}' already exists".format(user['username'])
            else:
                if verbosity>0:
                    print "Project: creating user '{0}'".format(user['username'])
                row=model_auth.User.objects.create_user(username=user['username'], email=user.get('email'), password=user.get('password'))
                row.first_name=user.get('first_name', '')
                row.last_name=user.get('last_name', '')
                row.is_staff=user.get('admin', False)
                row.is_active=user.get('active', False)
                row.is_superuser=user.get('superuser', False)
                row.save()
        except Exception, ex:
            print "WARNING: failed to create user '{0}' - {1}".format(user['username'], ex)


def create_reserved_words(verbosity, **kwargs):
    if verbosity>0:
        print "Project: updating reserved words"
    for word in _data.RESERVED_WORDS:
        try:
            row=model_ghosts.Reserved.objects.filter(word__iexact=word)
            if row.exists():
                if verbosity>1:
                    print "Project: reserved word '{0}' already exists".format(word)
            else:
                if verbosity>0:
                    print "Project: adding reserved word '{0}'".format(word)
                row=model_ghosts.Reserved.objects.create(word=word)
        except Exception, ex:
            print "WARNING: failed to create reserved word '{0}' - {1}".format(word, ex)



# hook us up
signals.post_syncdb.connect(receiver=update_default_site, sender=model_site)
signals.post_syncdb.connect(receiver=create_default_setting, sender=model_ghosts)
signals.post_syncdb.connect(receiver=create_reserved_users, sender=model_auth)
signals.post_syncdb.connect(receiver=create_reserved_words, sender=model_ghosts)
