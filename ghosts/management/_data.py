"""
created: 1/20/13
@author: curt

Default data for pre-population of our model
"""
from django.conf import settings

DEBUG=getattr(settings, 'DEBUG', False)
PASSWORD_INTERNAL='_1nternal_'

RESERVED_USERS=[
    # Our admin site is not very secure. We secure him through obscurity and minimizing the number of admin accounts.

    # production admins: for now, the following are the only production admin accounts
    { 'username':'xrayman', 'password':PASSWORD_INTERNAL, 'email':'xrayman@xraymen.com', 'active':True, 'admin':True, 'superuser':True },

    # local and staging admins:
    { 'username':'curt', 'password':'donkey', 'email':'curt@xraymen.com', 'first_name':'Curt', 'last_name':'Elsasser', 'active':True, 'admin':DEBUG, 'superuser':DEBUG },
    { 'username':'amit', 'password':'donkey', 'email':'amit@xraymen.com', 'first_name':'Amit', 'last_name':'Lissack', 'active':True, 'admin':DEBUG, 'superuser':DEBUG },
    { 'username':'xraymin', 'password':PASSWORD_INTERNAL, 'email':'xraymin@xraymen.com', 'active':DEBUG, 'admin':DEBUG, 'superuser':DEBUG },

    # internal/reserved/staff/test
    { 'username':'goggles', 'password':PASSWORD_INTERNAL, 'email':'goggles@xraymen.com', 'active':True, 'admin':False },
    { 'username':'xraymen', 'password':PASSWORD_INTERNAL, 'email':'xraymen@xraymen.com', 'active':True, 'admin':False },
    { 'username':'support', 'password':PASSWORD_INTERNAL, 'email':'support@xraymen.com', 'active':True, 'admin':False },
    { 'username':'testcase', 'password':'password', 'email':'testcase@xraymen.com', 'active':True, 'admin':False },
    { 'username':'lisa', 'password':'donkey', 'email':'lisa@xraymen.com', 'first_name':'Lisa', 'last_name':'Pope', 'active':True, 'admin':False },
    { 'username':'kerry', 'password':'donkey', 'email':'kerry@xraymen.com', 'first_name':'Kerry', 'last_name':'Goleski', 'active':True, 'admin':False },

    # prevention - misleading
    { 'username':'root', 'password':'password', 'email':'webadmin@xraymen.com', 'last_name':'_reserved', 'active':False, 'admin':False },
    { 'username':'admin', 'password':'password', 'email':'webadmin@xraymen.com', 'last_name':'_reserved', 'active':False, 'admin':False },
    { 'username':'webadmin', 'password':'password', 'email':'webadmin@xraymen.com', 'last_name':'_reserved', 'active':False, 'admin':False },
]

# amend RESERVED_USERS with categories that may come in handy for our own use
for username in ('info','information','history','sports','arts','music','theater','science','lifestyle','food','tour','travel','vacation',
                 'fact','fiction','romance','alien','pets','police','shopping'):
    RESERVED_USERS.append({ 'username':username, 'password':'_1nternal_', 'email':username+'@xraymen.com', 'active':True, 'admin':False })

DEFAULT_VIEW_SETTINGS=[
    { 'name':'radial', 'radius_horizontal':100, 'radius_vertical':100, 'min_query_interval':15, 'max_query_interval':60, 'max_messages_per_query':100},
    { 'name':'_admin', 'radius_horizontal':0, 'radius_vertical':10**6, 'min_query_interval':15, 'max_query_interval':60, 'max_messages_per_query':200},
]

RESERVED_WORDS=(
    'shit',
    'fuck',
    'fucker',
    'fucking',
    'cunt',
    'rape',
    'raper',
    'rapist',
    'kill',
    'nigger',
    'niggers',
    'spic',
    'chink',
)
