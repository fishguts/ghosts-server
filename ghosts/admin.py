"""
Created 7/2012
@author: curt
"""
from django.conf import settings
from django.contrib import admin, messages
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import models as auth_model
from django.conf.urls.defaults import patterns
from django.shortcuts import render_to_response
from django.template import RequestContext
from common.decorators import methodBooleanPropertyTrue
from common.utils import StringXT
from ghosts import models as app_model, constants
from ghosts import actions


''' Admin Support '''
def _send_action_result(request, success_count, error_count, message):
    message = message.format(success_count, error_count)
    if error_count>0:
        messages.warning(request, message, fail_silently=True)
    elif success_count>0:
        messages.info(request, message, fail_silently=True)


class ProfileInline(admin.TabularInline):
    model = app_model.Profile



''' Admin Model classes '''
class GhostsViewSettingsAdmin(admin.ModelAdmin):
    """
    View configuration management
    """
    # list display properties
    list_display = ('name', 'radius_horizontal', 'radius_vertical', 'min_query_interval', 'max_query_interval', 'max_messages_per_query')


class GhostsUserAdmin(auth_admin.UserAdmin):
    """
    Add our profile for inline editing to UserAdmin
    """
    inlines = (ProfileInline,)


class GhostsMessageAdmin(admin.ModelAdmin):
    """
    Message management
    """
    # list display properties
    list_display = ('type', 'status', 'user', 'created', 'expiration', 'expired', 'text_truncated')
    list_filter = ('type', 'status', 'created', 'expiration')
    search_fields = ('user__username', 'text')
    ordering = ('-created',)
    # edit display
    fields = ('user', 'type', 'text', ('latitude', 'longitude', 'altitude'), 'status', 'created', 'expiration')
    if not getattr(settings, 'DEBUG_SUSPEND_ADMIN_READONLY', False):
        readonly_fields = ('user', 'type')

    def text_truncated(self, obj):
        return StringXT.ellipsis(obj.text, 75)

    def get_urls(self):
        urls = super(GhostsMessageAdmin, self).get_urls()
        my_urls = patterns('',
            (r'^map/$', self.admin_site.admin_view(self.map))
        )
        return my_urls + urls

    def map(self, request):
        return render_to_response("admin/ghosts/message/map.html", context_instance=RequestContext(request))

class GhostsRatingAdmin(admin.ModelAdmin):
    """
    Message rating management
    """
    # list display properties
    list_display = ('user', 'value', 'message_truncated')
    search_fields = ('user__username', )
    list_filter = ('value',)
    # edit display properties
    fields = ('user', 'message', 'value')
    if not getattr(settings, 'DEBUG_SUSPEND_ADMIN_READONLY', False):
        readonly_fields = ('user', 'message')

    def message_truncated(self, obj):
        return StringXT.ellipsis(obj.message.text, 125)


class GhostsFlaggedAdmin(admin.ModelAdmin):
    """
    Message flag management
    """
    # list display properties
    list_display = ('user', 'processed', 'flagged_user', 'flagged_message_truncated', 'flagged_message_status', 'flagged_user_active', 'flagged_user_notes')
    search_fields = ('user__username', )
    list_filter = ('processed',)
    actions = ('action_apply_flag', 'action_reject_flag', 'action_warn_account', 'action_revoke_account')
    # edit display properties
    fields = ('user', 'message', 'processed')
    if not getattr(settings, 'DEBUG_SUSPEND_ADMIN_READONLY', False):
        readonly_fields = ('user', 'message')

    ''' Field extensions '''
    def flagged_user(self, obj):
        return obj.message.user

    @methodBooleanPropertyTrue
    def flagged_user_active(self, obj):
        return obj.message.user.is_active

    def flagged_user_notes(self, obj):
        try:
            return obj.message.user.get_profile().notes
        except:
            return None

    def flagged_message_truncated(self, obj):
        return StringXT.ellipsis(obj.message.text, 125)

    def flagged_message_status(self, obj):
        return obj.message.get_status_display()

    ''' Actions '''
    def action_apply_flag(self, request, queryset):
        success_count = 0
        error_count = 0
        for flag in queryset:
            try:
                actions.message_flag_apply(flag)
                success_count += 1
            except:
                error_count += 1
        _send_action_result(request, success_count, error_count, 'Flag applied: success count={0}, failure count={1}')

    def action_reject_flag(self, request, queryset):
        success_count = 0
        error_count = 0
        for flag in queryset:
            try:
                actions.message_flag_reject(flag)
                success_count += 1
            except:
                error_count += 1
        _send_action_result(request, success_count, error_count, 'Flag rejected: success count={0}, failure count={1}')

    def action_warn_account(self, request, queryset):
        success_count = 0
        error_count = 0
        for flag in queryset:
            try:
                # as we process these it's possible for the underlying message and owner to
                # have been updated. Make sure our object is in sync so we don't spam the guy
                flag=app_model.Flagged.objects.get(id=flag.id)
                actions.message_flag_apply_and_send_warning(flag)
                success_count += 1
            except:
                error_count += 1
        _send_action_result(request, success_count, error_count, 'Account revoked: success count={0}, failure count={1}')

    def action_revoke_account(self, request, queryset):
        success_count = 0
        error_count = 0
        for flag in queryset:
            try:
                # as we process these it's possible for the underlying message and owner to
                # have been updated. Make sure our object is in sync so we don't spam the guy
                flag=app_model.Flagged.objects.get(id=flag.id)
                actions.message_flag_apply_and_revoke_account(flag)
                success_count += 1
            except:
                error_count += 1
        _send_action_result(request, success_count, error_count, 'Account revoked: success count={0}, failure count={1}')

    ''' attach metadata '''
    action_apply_flag.short_description = "Apply selected flags"
    action_warn_account.short_description = "Apply selected flags and warn accounts"
    action_revoke_account.short_description = "Apply selected flags and revoke accounts"
    action_reject_flag.short_description = "Reject selected flags"


class GhostsFeedbackAdmin(admin.ModelAdmin):
    """
    Website/Application Feedback management
    """
    # list display properties
    list_display = ('user_description', 'origin', 'is_active_user', 'action', 'flag', 'created', 'text_truncated')
    list_filter = ('action', 'flag', 'origin', 'created')
    search_fields = ('user__username', 'email')
    ordering = ('-created',)
    actions = ('action_cancel_account', 'action_set_closed', 'action_set_replied', 'action_flag_content', 'action_flag_origin', 'action_flag_spam')
    # edit display properties
    fields = ('user', 'name', 'email', 'origin', 'device', 'text', 'flag', 'action', 'created' )
    if not getattr(settings, 'DEBUG_SUSPEND_ADMIN_READONLY', False):
        readonly_fields = ('user', 'name', 'email', 'origin', 'device', 'text', 'created' )

    ''' Field Extensions '''
    def user_description(self, obj):
        return obj.user if obj.user else obj.email

    @methodBooleanPropertyTrue
    def is_active_user(self, obj):
        return obj.user.is_active if obj.user else False

    def text_truncated(self, obj):
        return StringXT.ellipsis(obj.text, 175)

    ''' Actions '''
    def action_cancel_account(self, request, queryset):
        success_count = 0
        error_count = 0
        for feedback in queryset:
            try:
                actions.feedback_cancel_account(feedback=feedback, send_email=True)
                success_count += 1
            except:
                error_count += 1
        _send_action_result(request, success_count, error_count, 'Account canceled: success count={0}, failure count={1}')

    def action_set_closed(self, request, queryset):
        success_count, error_count = self._update_queryset(queryset, flag=constants.FLAG_NONE, action=constants.ACTION_CLOSED)
        _send_action_result(request, success_count, error_count, 'Flag content: success count={0}, failure count={1}')

    def action_set_replied(self, request, queryset):
        success_count, error_count = self._update_queryset(queryset, flag=constants.FLAG_NONE, action=constants.ACTION_REPLIED)
        _send_action_result(request, success_count, error_count, 'Flag content: success count={0}, failure count={1}')

    def action_flag_content(self, request, queryset):
        success_count, error_count = self._update_queryset(queryset, flag=constants.FLAG_CONFIRMED_CONTENT, action=constants.ACTION_CLOSED)
        _send_action_result(request, success_count, error_count, 'Flag content: success count={0}, failure count={1}')

    def action_flag_origin(self, request, queryset):
        success_count, error_count = self._update_queryset(queryset, flag=constants.FLAG_CONFIRMED_ORIGIN, action=constants.ACTION_CLOSED)
        _send_action_result(request, success_count, error_count, 'Flag origin: success count={0}, failure count={1}')

    def action_flag_spam(self, request, queryset):
        success_count, error_count = self._update_queryset(queryset, flag=constants.FLAG_CONFIRMED_SPAM, action=constants.ACTION_CLOSED)
        _send_action_result(request, success_count, error_count, 'Flag spam: success count={0}, failure count={1}')

    ''' attach metadata '''
    action_cancel_account.short_description = "Cancel selected accounts"
    action_set_closed.short_description = "Set 'closed' for selected feedback"
    action_set_replied.short_description = "Set 'replied' for selected feedback"
    action_flag_content.short_description = "Flag content for selected feedback"
    action_flag_origin.short_description = "Flag origin for selected feedback"
    action_flag_spam.short_description = "Flag spam for selected feedback"

    def _update_queryset(self, queryset, flag, action):
        success_count = 0
        error_count = 0
        for feedback in queryset:
            try:
                actions.feedback_update(feedback=feedback, flag=flag, action=action)
                success_count += 1
            except:
                error_count += 1
        return success_count, error_count



''' Admin Configuration '''
admin.site.unregister(auth_model.User)
admin.site.register(auth_model.User, GhostsUserAdmin)
admin.site.register(app_model.ViewSettings, GhostsViewSettingsAdmin)
admin.site.register(app_model.Message, GhostsMessageAdmin)
admin.site.register(app_model.Rating, GhostsRatingAdmin)
admin.site.register(app_model.Flagged, GhostsFlaggedAdmin)
admin.site.register(app_model.Feedback, GhostsFeedbackAdmin)
admin.site.register(app_model.Reserved)
