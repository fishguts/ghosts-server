"""
Created on Jul 25, 2012

@author: curt
"""
from django.conf import settings
from common import builtins
from ghosts.serializers import core as serializer_core


SERVER_VERSION=getattr(settings, 'SERVER_VERSION', '1.0')

MODEL_STATUS_OKAY='o'
MODEL_STATUS_DEAD='d'
MODEL_STATUS_FLAG='f'
MODEL_STATUS_PENDING='p'

FLAG_NONE=''
FLAG_SUSPECT_CONTENT='c'
FLAG_SUSPECT_ORIGIN='o'
FLAG_SUSPECT_SPAM='s'
FLAG_CONFIRMED_CONTENT='C'
FLAG_CONFIRMED_ORIGIN='O'
FLAG_CONFIRMED_SPAM='S'

ACTION_NONE=''
ACTION_REPLIED='r'
ACTION_FLAGGED='f'
ACTION_CLOSED='c'

FEEDBACK_ORIGIN_UNKNOWN=''
FEEDBACK_ORIGIN_APP='a'
FEEDBACK_ORIGIN_WEB='w'

''' format for codes/constants [internal code]|[response code] '''
IMAGE_TYPE_SMALL=('s', 'small')
IMAGE_TYPE_LARGE=('l', 'large')

MESSAGE_TYPE_USER=('', 'user')
MESSAGE_TYPE_WELCOME=('w', 'welcome')
MESSAGE_TYPE_NOTIFICATION=('n', 'notification')
MESSAGE_TYPE_ADVERTISEMENT=('a', 'advertisement')


''' conversion methods (todo: these guys should join their friends in serializer.core)'''
def code_to_response_image(code, unknown_handler=builtins.fail_to_warn):
    return serializer_core.code_to_response(code, (IMAGE_TYPE_SMALL, IMAGE_TYPE_LARGE), unknown_handler)

def code_to_response_message(code, unknown_handler=builtins.fail_to_warn):
    return serializer_core.code_to_response(code, (MESSAGE_TYPE_USER, MESSAGE_TYPE_WELCOME, MESSAGE_TYPE_NOTIFICATION, MESSAGE_TYPE_ADVERTISEMENT), unknown_handler)

def response_to_code_image(response, unknown_handler=builtins.fail_to_warn):
    return serializer_core.response_to_code(response, (IMAGE_TYPE_SMALL, IMAGE_TYPE_LARGE), unknown_handler)

def response_to_code_message(response, unknown_handler=builtins.fail_to_warn):
    return serializer_core.response_to_code(response, (MESSAGE_TYPE_USER, MESSAGE_TYPE_WELCOME, MESSAGE_TYPE_NOTIFICATION, MESSAGE_TYPE_ADVERTISEMENT), unknown_handler)
