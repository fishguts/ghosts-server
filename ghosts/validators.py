"""
created: 3/3/13
@author: curt

Validates our internal intermediate data objects for the presence
of data and validity of the very same data.
"""
from django.conf import settings
from common import constants
from common.exceptions import StatusException
from common.objects import Status


def validate_message_query_object(data):
    """
    Validates request for messages based on a location and a 'view' which is a ViewSettings name.
    """
    if not (data.has_key('view') and data.has_key('latitude') and data.has_key('longitude') and data.has_key('altitude')):
        raise ValueError('Message query validation failed: missing keys')


def validate_message_post_object(data):
    if not (data.has_key('text') and data.has_key('latitude') and data.has_key('longitude') and data.has_key('altitude')):
        raise ValueError('Message create validation failed: missing keys')
    # note: missing data is suspicious, data out of bounds is a legitimate error.
    length = len(data['text'])
    length_min = getattr(settings, 'MESSAGE_MIN_LENGTH', 0)
    length_max = getattr(settings, 'MESSAGE_MAX_LENGTH', 512)
    if length<length_min or length>length_max:
        raise StatusException(Status(constants.RESPONSE_CODE_FAIL, "Message is {0} characters. Must be between {1} and {2}".format(length, length_min, length_max)))


def validate_message_delete_object(data):
    if not (data.has_key('messageId')):
        raise ValueError('Message delete validation failed: missing keys')


def validate_message_rate_object(data):
    # decode and extract our data and make sure our eggs look good
    if not (data.has_key('messageId') and data.has_key('rating')):
        raise ValueError('Message rate validation failed: missing keys')


def validate_message_flag_object(data):
    if not (data.has_key('messageId')):
        raise ValueError('Message flag validation failed: missing keys')


def validate_perimeter_query_object(data):
    """
    Validates request for messages based on a location and perimeter
    """
    if not (data.has_key('latitude') and data.has_key('longitude') and data.has_key('radius')):
        raise ValueError('Message query validation failed: missing keys')
    perimeter_min=getattr(settings, 'PERIMETER_QUERY_RADIUS_MIN', 1)
    if data['radius']<perimeter_min:
        raise StatusException(Status(constants.RESPONSE_CODE_FAIL, "Radius must be greater than {0}".format(perimeter_min)))


def validate_feedback_object(data):
    if not (data.has_key('text') and data.has_key('device')):
        raise ValueError('Feedback validation failed: missing keys')
    # note: missing data is suspicious, data out of bounds is a legitimate error.
    length = len(data['text'])
    length_min = getattr(settings, 'FEEDBACK_MIN_LENGTH', 4)
    length_max = getattr(settings, 'FEEDBACK_MAX_LENGTH', 1250)
    if length<length_min or length>length_max:
        raise StatusException(Status(constants.RESPONSE_CODE_FAIL, "Feedback must be between {0} and {1} characters".format(length_min, length_max)))


def validate_login_object(data):
    if not (data.has_key('username') and data.has_key('password')):
        raise ValueError('Login validation failed: missing keys')


def validate_logout_object(data):
    if not (data.has_key('username')):
        raise ValueError('Logout validation failed: missing keys')

