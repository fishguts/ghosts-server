"""
created: 6/3/13
@author: curt
"""
from ghosts.serializers import core as serializer_core

def condition_message_post_object(data):
    # eliminate leading and trailing whitespace
    if data.has_key('text'):
        data['text']=data['text'].strip()
    return data

def condition_perimeter_messages_object(data):
    if data.has_key('expiration'):
        data['expiration']=serializer_core.string_to_datetime(data['expiration'])
