"""
Created: 11/3/12
@author: curt
"""

from django.utils import unittest
from ghosts.tests.testbehavior import GhostsBehaviorTests
from ghosts.tests.testcommands import GhostsCommandTests
from ghosts.tests.testcommon import GhostsCommonTests
from ghosts.tests.testcontact import GhostsContactTests
from ghosts.tests.testqueries import GhostsQueryTests
from ghosts.tests.testfactories import GhostsFactoryTests
from ghosts.tests.testurls import GhostsUrlTests
from ghosts.tests.testviews import GhostsViewTests
from ghosts.tests.testvalidators import GhostsValidatorTests
from ghosts.tests.testactions import GhostsActionTests
from ghosts.tests.testmodel import GhostsModelTests
from ghosts.tests.testconditioners import GhostsConditionerTests
from ghosts.tests.testsecurity import GhostsSecurityTests
from ghosts.tests.testserializers import *

def suite():
    """
    Queue up all tests in our suite.  Our motivation for building suite manually:
        - django test runner is very adamant about where test cases live and tests folder isn't one of them.
        - python unit test runner causes test to be run over our database. Django does a syncdb before each test which reverts back to the beginning of time.
    """
    suite=unittest.TestSuite()
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsCommonTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsSerializerCoreTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsSerializerCsvTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsSerializerJsonTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsValidatorTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsFactoryTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsViewTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsActionTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsCommandTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsModelTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsConditionerTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsContactTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsBehaviorTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsSecurityTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsQueryTests))
    suite.addTest(unittest.TestLoader().loadTestsFromTestCase(GhostsUrlTests))
    return suite
