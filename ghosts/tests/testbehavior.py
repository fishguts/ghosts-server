"""
created: 12/12/12
@author: curt
"""

from django.test import TestCase

from ghosts import factories
from ghosts.models import Rating, Flagged
from ghosts.tests._testfactories import GhostsTestFactories


class GhostsBehaviorTests(TestCase):
    """
    Tests for our desired behavior and preventative measures against stuff we anticipate
    """
    def test_rating_flip(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user)
        # create
        rating=factories.data_to_rating({'messageId':message.id, 'rating':1}, user)
        rating.save()
        self.assertEqual(Rating.objects.filter(message__pk=message.id).count(), 1)
        # flip
        rating=factories.data_to_rating({'messageId':message.id, 'rating':-1}, user)
        rating.save()
        self.assertEqual(Rating.objects.filter(message__pk=message.id).count(), 1)


    def test_rating_repeat(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user)
        # create
        rating=factories.data_to_rating({'messageId':message.id, 'rating':1}, user)
        rating.save()
        self.assertEqual(Rating.objects.filter(message__pk=message.id).count(), 1)
        # repeat
        rating=factories.data_to_rating({'messageId':message.id, 'rating':1}, user)
        rating.save()
        self.assertEqual(Rating.objects.filter(message__pk=message.id).count(), 1)


    def test_flag_repeated(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user)
        # create
        flag=factories.data_to_flag({'messageId':message.id}, user)
        flag.save()
        self.assertEqual(Flagged.objects.filter(message__pk=message.id).count(), 1)
        # re-apply
        flag=factories.data_to_flag({'messageId':message.id}, user)
        flag.save()
        self.assertEqual(Flagged.objects.filter(message__pk=message.id).count(), 1)
