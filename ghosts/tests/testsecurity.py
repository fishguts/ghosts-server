"""
created: 7/19/13
@author: curt
"""

from django.test import TestCase

from ghosts import security


class GhostsSecurityTests(TestCase):
    def test_restricted_user_names(self):
        self.assertTrue(security.username_contains_flagged_content('fuck'))
        for disguise in '-.@+_':
            self.assertTrue(security.username_contains_flagged_content(disguise+'fuck'))
            self.assertTrue(security.username_contains_flagged_content('fuck'+disguise))
            self.assertTrue(security.username_contains_flagged_content(disguise+'fuck'+disguise))

    def test_allowed_user_names(self):
        self.assertTrue(security.username_contains_flagged_content('shit'))
        self.assertFalse(security.username_contains_flagged_content('shitt'))
        self.assertFalse(security.username_contains_flagged_content('sshit'))
        self.assertFalse(security.username_contains_flagged_content('sshitt'))

    def test_restricted_content(self):
        self.assertTrue(security.text_contains_flagged_content('fuck'))
        for disguise in '-.@+_!#$%^&*(){}[]\'" \t\n':
            self.assertTrue(security.text_contains_flagged_content(disguise+'fuck'))
            self.assertTrue(security.text_contains_flagged_content('fuck'+disguise))
            self.assertTrue(security.text_contains_flagged_content(disguise+'fuck'+disguise))

    def test_allowed_content(self):
        self.assertTrue(security.text_contains_flagged_content('shit'))
        self.assertFalse(security.text_contains_flagged_content('shitt'))
        self.assertFalse(security.text_contains_flagged_content('sshit'))
        self.assertFalse(security.text_contains_flagged_content('sshitt'))
