"""
created: 12/12/12
@author: curt
"""
from datetime import timedelta

from django.test import TestCase
from ghosts import constants, queries, factories
from ghosts import models as model_ghosts
from ghosts.tests._testfactories import GhostsTestFactories


class GhostsQueryTests(TestCase):
    def test_get_request_messages_accuracy(self):
        settings=model_ghosts.ViewSettings.objects.get(name='radial')
        self._test_get_location_messages_accuracy(settings=settings, accuracy_method=self._test_get_request_messages_accuracy)


    def test_get_perimeter_messages_accuracy(self):
        settings=model_ghosts.ViewSettings.objects.get(name='_admin')
        settings.radius_horizontal=10000
        self._test_get_location_messages_accuracy(settings=settings, accuracy_method=self._test_get_perimeter_messages_accuracy)


    def test_get_location_message_exclusion(self):
        """
        Make sure our typed messages without locations are not picked up in our location based query
        """
        user=GhostsTestFactories.create_user_object()
        settings=GhostsTestFactories.create_settings_object()
        GhostsTestFactories.create_message_typed_object(user, type=constants.MESSAGE_TYPE_NOTIFICATION[0])
        GhostsTestFactories.create_message_typed_object(user, type=constants.MESSAGE_TYPE_ADVERTISEMENT[0])
        GhostsTestFactories.create_message_typed_object(user, type=constants.MESSAGE_TYPE_WELCOME[0])
        GhostsTestFactories.create_message_object(user=user, latitude=0, longitude=0, altitude=0)
        query_set=queries.get_location_messages(view_settings=settings, latitude=0, longitude=0, altitude=0)
        self.assertEqual(model_ghosts.Message.objects.count(), 4)
        self.assertEqual(query_set.count(), 1)


    def test_get_request_messages_with_welcome(self):
        user=GhostsTestFactories.create_user_object()
        GhostsTestFactories.create_message_object(user=user, latitude=0, longitude=0, altitude=0)
        # test with existence of welcome message but 'welcome' not in query
        GhostsTestFactories.create_message_typed_object(user, type=constants.MESSAGE_TYPE_WELCOME[0])
        query_set_request=queries.get_request_messages({'view':'radial', 'latitude':0, 'longitude':0, 'altitude':0})
        self.assertEqual(query_set_request.count(), 1)
        # test with existence of welcome message and 'welcome' not in query
        query_set_request=queries.get_request_messages({'view':'radial', 'latitude':0, 'longitude':0, 'altitude':0, 'welcome':True})
        self.assertEqual(query_set_request.count(), 2)


    def test_gets_with_no_message_expiration(self):
        user=GhostsTestFactories.create_user_object()
        settings=GhostsTestFactories.create_settings_object()
        message=GhostsTestFactories.create_message_object(user=user)
        self.assertEqual(queries.get_expired_messages().count(), 0)
        self.assertEqual(queries.get_location_messages(view_settings=settings, latitude=message.latitude, longitude=message.longitude, altitude=message.altitude).count(), 1)


    def test_gets_with_future_message_expiration(self):
        user=GhostsTestFactories.create_user_object()
        settings=GhostsTestFactories.create_settings_object()
        message=GhostsTestFactories.create_message_object(user=user, expires=factories.create_timestamp()+timedelta(days=1))
        self.assertEqual(queries.get_expired_messages().count(), 0)
        self.assertEqual(queries.get_location_messages(view_settings=settings, latitude=message.latitude, longitude=message.longitude, altitude=message.altitude).count(), 1)


    def test_gets_with_past_message_expiration(self):
        user=GhostsTestFactories.create_user_object()
        settings=GhostsTestFactories.create_settings_object()
        message=GhostsTestFactories.create_message_object(user=user, expires=factories.create_timestamp()-timedelta(days=1))
        self.assertEqual(queries.get_expired_messages().count(), 1)
        self.assertEqual(queries.get_location_messages(view_settings=settings, latitude=message.latitude, longitude=message.longitude, altitude=message.altitude).count(), 0)


    def test_gets_with_mixed_message_expiration(self):
        user=GhostsTestFactories.create_user_object()
        settings=GhostsTestFactories.create_settings_object()
        message=GhostsTestFactories.create_message_object(user=user)
        GhostsTestFactories.create_message_object(user=user, expires=factories.create_timestamp()+timedelta(days=1))
        GhostsTestFactories.create_message_object(user=user, expires=factories.create_timestamp()-timedelta(days=1))
        self.assertEqual(queries.get_expired_messages().count(), 1)
        self.assertEqual(queries.get_location_messages(view_settings=settings, latitude=message.latitude, longitude=message.longitude, altitude=message.altitude).count(), 2)


    def test_gets_with_message_flagged(self):
        user=GhostsTestFactories.create_user_object()
        settings=GhostsTestFactories.create_settings_object()
        message=GhostsTestFactories.create_message_object(user)
        # confirm positive
        query_set=queries.get_location_messages(view_settings=settings, latitude=message.latitude, longitude=message.longitude, altitude=message.altitude)
        self.assertEqual(query_set.count(), 1)
        # flip and confirm negative
        message.status=constants.MODEL_STATUS_FLAG; message.save()
        query_set=queries.get_location_messages(view_settings=settings, latitude=message.latitude, longitude=message.longitude, altitude=message.altitude)
        self.assertEqual(query_set.count(), 0)


    def test_message_query_by_type(self):
        user=GhostsTestFactories.create_user_object()
        self._test_message_type(user, constants.MESSAGE_TYPE_USER[0], constants.MESSAGE_TYPE_WELCOME[0])
        self._test_message_type(user, constants.MESSAGE_TYPE_NOTIFICATION[0], constants.MESSAGE_TYPE_ADVERTISEMENT[0])
        self._test_message_type(user, constants.MESSAGE_TYPE_ADVERTISEMENT[0], constants.MESSAGE_TYPE_NOTIFICATION[0])
        self._test_message_type(user, constants.MESSAGE_TYPE_WELCOME[0], constants.MESSAGE_TYPE_USER[0])


    ''' private support '''
    def _test_get_location_messages_accuracy(self, settings, accuracy_method):
        user=GhostsTestFactories.create_user_object()
        # cycle through the range of what is to be expected
        for latitude in [-90, -45, 0, 45, 90]:
            for longitude in [-180, -90, 0, 90, 180]:
                for altitude in [-100, 0, 100]:
                    radius_latitude=queries._get_latitude_radius(settings)
                    radius_longitude=queries._get_longitude_radius(settings, latitude)
                    radius_altitude=settings.radius_vertical
                    # test inside perimeter
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=True)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=True, delta_msg_latitude=0-radius_latitude*0.9)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=True, delta_msg_latitude=radius_latitude*0.9)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=True, delta_msg_longitude=0-radius_longitude*0.9)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=True, delta_msg_longitude=radius_longitude*0.9)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=True, delta_msg_altitude=0-radius_altitude*0.9)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=True, delta_msg_altitude=radius_altitude*0.9)
                    # test outside perimeter
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=False, delta_msg_latitude=0-radius_latitude*1.1)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=False, delta_msg_latitude=radius_latitude*1.1)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=False, delta_msg_altitude=0-radius_altitude*1.1)
                    accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=False, delta_msg_altitude=radius_altitude*1.1)
                    # longitude should not be < -180 and we will fail tests for such cases. Opting for simplicity of our query over complexity for bad data.
                    if longitude > -180:
                        accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=False, delta_msg_longitude=0-radius_longitude*1.1)
                    # longitude should not be > 180 and we will fail tests for such cases. Opting for simplicity of our query over complexity for bad data.
                    if longitude < 180:
                        accuracy_method(settings, user, latitude=latitude, longitude=longitude, altitude=altitude, testPositive=False, delta_msg_longitude=radius_longitude*1.1)


    def _test_get_request_messages_accuracy(self, settings, user, latitude, longitude, altitude, delta_msg_latitude=0, delta_msg_longitude=0, delta_msg_altitude=0, testPositive=True):
        message=GhostsTestFactories.create_message_object(user=user, latitude=latitude+delta_msg_latitude, longitude=longitude+delta_msg_longitude, altitude=altitude+delta_msg_altitude)
        query_set_request=queries.get_request_messages({'view':settings.name, 'latitude':latitude, 'longitude':longitude, 'altitude':altitude})
        if testPositive:
            self.assertEqual(query_set_request.count(), 1)
            self.assertEqual(query_set_request[0].id, message.id)
        else:
            self.assertEqual(query_set_request.count(), 0)
        message.delete()

    def _test_get_perimeter_messages_accuracy(self, settings, user, latitude, longitude, altitude, delta_msg_latitude=0, delta_msg_longitude=0, delta_msg_altitude=0, testPositive=True):
        message=GhostsTestFactories.create_message_object(user=user, latitude=latitude+delta_msg_latitude, longitude=longitude+delta_msg_longitude, altitude=altitude+delta_msg_altitude)
        query_set_request=queries.get_perimeter_messages({'latitude':latitude, 'longitude':longitude, 'radius':settings.radius_horizontal})
        if testPositive:
            self.assertEqual(query_set_request.count(), 1)
            self.assertEqual(query_set_request[0].id, message.id)
        else:
            self.assertEqual(query_set_request.count(), 0)
        message.delete()


    def _test_perimiter_accuracy(self, settings, user, latitude, longitude, altitude, delta_msg_latitude=0, delta_msg_longitude=0, delta_msg_altitude=0, testPositive=True):
        message=GhostsTestFactories.create_message_object(user=user, latitude=latitude+delta_msg_latitude, longitude=longitude+delta_msg_longitude, altitude=altitude+delta_msg_altitude)
        query_set_location=queries.get_location_messages(view_settings=settings, latitude=latitude, longitude=longitude, altitude=altitude)
        # take advantage of our opportunity and make sure our request based version always has parity (without presence typed message)
        query_set_request=queries.get_request_messages({'view':settings.name, 'latitude':latitude, 'longitude':longitude, 'altitude':altitude})
        self.assertEqual(query_set_location.count(), query_set_request.count())
        if testPositive:
            self.assertEqual(query_set_location.count(), 1)
            self.assertEqual(query_set_location[0].id, message.id)
            query_set_location.delete()
        else:
            self.assertEqual(query_set_location.count(), 0)
            message.delete()


    def _test_message_type(self, user, message_type_positive, message_type_negative):
        # test positive
        message=GhostsTestFactories.create_message_typed_object(user, type=message_type_positive)
        query_set=queries.get_typed_messages(message_type=message_type_positive)
        self.assertEqual(query_set.count(), 1)
        self.assertEqual(query_set[0].id, message.id)
        self.assertEqual(query_set[0].status, message.status)
        # test negative
        GhostsTestFactories.create_message_typed_object(user, type=message_type_negative)
        query_set=queries.get_typed_messages(message_type=message_type_positive)
        self.assertEqual(query_set.count(), 1)
        self.assertEqual(query_set[0].id, message.id)
        self.assertEqual(query_set[0].status, message.status)
        # test for both
        query_set=queries.get_typed_messages(message_type=message_type_positive) | queries.get_typed_messages(message_type=message_type_negative)
        self.assertEqual(query_set.count(), 2)
        # clean up
        query_set.delete()
