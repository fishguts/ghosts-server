"""
created: 6/3/13
@author: curt
"""
from datetime import datetime

from django.test import TestCase
from ghosts import conditioners
from ghosts.tests._testfactories import GhostsTestFactories


class GhostsConditionerTests(TestCase):
    def test_condition_message_invalid_text(self):
        """
        We don't support or allow these conditions and it's fine and expected that they fail
        """
        data=GhostsTestFactories.create_message_dictionary(text=1)
        self.assertRaises(Exception, conditioners.condition_message_post_object, data)

        data=GhostsTestFactories.create_message_dictionary(text=None)
        self.assertRaises(Exception, conditioners.condition_message_post_object, data)


    def test_condition_message_valid_text(self):
        data=GhostsTestFactories.create_message_dictionary(text='normal')
        data=conditioners.condition_message_post_object(data)
        self.assertEqual(data['text'], 'normal')

        data=GhostsTestFactories.create_message_dictionary(text='')
        data=conditioners.condition_message_post_object(data)
        self.assertEqual(data['text'], '')

        data=GhostsTestFactories.create_message_dictionary(text=' ')
        data=conditioners.condition_message_post_object(data)
        self.assertEqual(data['text'], '')

        data=GhostsTestFactories.create_message_dictionary(text='\t')
        data=conditioners.condition_message_post_object(data)
        self.assertEqual(data['text'], '')

        data=GhostsTestFactories.create_message_dictionary(text='\n')
        data=conditioners.condition_message_post_object(data)
        self.assertEqual(data['text'], '')

        data=GhostsTestFactories.create_message_dictionary(text=' abc ')
        data=conditioners.condition_message_post_object(data)
        self.assertEqual(data['text'], 'abc')

    def test_condition_get_perimeter_messages_object_invalid(self):
        # we do thorough date testing elsewhere.  Just make sure it bombs as expected.
        self.assertRaises(ValueError, conditioners.condition_perimeter_messages_object, {'expiration':'2013-01-01'})

    def test_condition_get_perimeter_messages_object_valid(self):
        data={}
        self.assertIsNone(conditioners.condition_perimeter_messages_object(data))
        self.assertFalse('expiration' in data)
        data={'expiration':'2013-01-01T12:00:00'}
        self.assertIsNone(conditioners.condition_perimeter_messages_object(data))
        self.assertEqual(data['expiration'], datetime(year=2013, month=1, day=1, hour=12))


