"""
created: 7/19/13
@author: curt
"""
from django.core import urlresolvers

from django.test import TestCase


class GhostsUrlTests(TestCase):
    def test_valid(self):
        self.assertTrue(urlresolvers.is_valid_path('/settings/get/json'))
        self.assertTrue(urlresolvers.is_valid_path('/messages/get/json'))
        self.assertTrue(urlresolvers.is_valid_path('/messages/put/json'))
        self.assertTrue(urlresolvers.is_valid_path('/messages/rate/json'))
        self.assertTrue(urlresolvers.is_valid_path('/messages/flag/json'))
        self.assertTrue(urlresolvers.is_valid_path('/messages/delete/json'))
        self.assertTrue(urlresolvers.is_valid_path('/profile/login/json'))
        self.assertTrue(urlresolvers.is_valid_path('/profile/logout/json'))
        self.assertTrue(urlresolvers.is_valid_path('/profile/update/json'))
        self.assertTrue(urlresolvers.is_valid_path('/profile/delete/json'))
        self.assertTrue(urlresolvers.is_valid_path('/profile/register/json'))
        self.assertTrue(urlresolvers.is_valid_path('/profile/activate/123'))
        self.assertTrue(urlresolvers.is_valid_path('/profile/activate/abc'))
        self.assertTrue(urlresolvers.is_valid_path('/support/feedback/json'))
        self.assertTrue(urlresolvers.is_valid_path('/support/contact/form'))
        self.assertTrue(urlresolvers.is_valid_path('/internal/admin/'))
        self.assertTrue(urlresolvers.is_valid_path('/internal/admin/messages/get/json'))

    def test_underfed(self):
        self.assertFalse(urlresolvers.is_valid_path('/settings/get/'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/get/'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/put/'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/rate/'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/flag/'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/delete/'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/login/'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/logout/'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/update/'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/delete/'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/register/'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/activate/'))
        self.assertFalse(urlresolvers.is_valid_path('/support/feedback/'))
        self.assertFalse(urlresolvers.is_valid_path('/support/contact/'))
        self.assertFalse(urlresolvers.is_valid_path('/internal/'))
        self.assertFalse(urlresolvers.is_valid_path('/internal/admin/messages/get/'))

    def test_overstuffed(self):
        self.assertFalse(urlresolvers.is_valid_path('/settings/get/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/get/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/put/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/rate/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/flag/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/messages/delete/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/login/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/logout/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/update/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/delete/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/register/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/activate/key123/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/support/feedback/json/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/support/contact/form/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/internal/admin/bones'))
        self.assertFalse(urlresolvers.is_valid_path('/internal/admin/messages/get/bones'))

    def test_invalid_characters(self):
        self.assertFalse(urlresolvers.is_valid_path('/profile/activate/a===='))
        self.assertFalse(urlresolvers.is_valid_path('/profile/activate/b@@@@'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/activate/c***'))
        self.assertFalse(urlresolvers.is_valid_path('/profile/activate/d&&&&'))

    def test_hidden(self):
        self.assertFalse(urlresolvers.is_valid_path('/admin/'))
