"""
created: 12/12/12
@author: curt
"""
from django.conf import settings
from django.contrib.auth.models import User
from ghosts import factories
from ghosts.constants import MODEL_STATUS_OKAY, FEEDBACK_ORIGIN_APP, FEEDBACK_ORIGIN_WEB
from ghosts.models import Message, ViewSettings, Rating, Flagged, Feedback
from ghosts.serializers import core as serializer_core


class GhostsTestFactories(object):
    """
    Internal factories for objects used in tests.
    """

    ''' Utilities '''
    @staticmethod
    def get_message_text_valid():
        return '1' * getattr(settings, 'MESSAGE_MIN_LENGTH')

    @staticmethod
    def get_message_text_short():
        return '1' * (getattr(settings, 'MESSAGE_MIN_LENGTH')-1)

    @staticmethod
    def get_message_text_long():
        return '1' * (getattr(settings, 'MESSAGE_MAX_LENGTH')+1)

    @staticmethod
    def get_feedback_text_valid():
        return '1' * getattr(settings, 'FEEDBACK_MIN_LENGTH')

    @staticmethod
    def get_feedback_text_short():
        return '1' * (getattr(settings, 'FEEDBACK_MIN_LENGTH')-1)

    @staticmethod
    def get_feedback_text_long():
        return '1' * (getattr(settings, 'FEEDBACK_MAX_LENGTH')+1)

    ''' Internal map representation factories '''
    @staticmethod
    def create_user_dictionary(name, email, password):
        return {
            'username':name,
            'email':email,
            'password':password,
        }

    @staticmethod
    def create_message_dictionary(text, latitude=0, longitude=0, altitude=0, created=None, expires=None):
        result={
            'text':text,
            'latitude':latitude,
            'longitude':longitude,
            'altitude':altitude,
        }
        if created:
            result['created']=serializer_core.datetime_to_string(created)
        else:
            result['created']=serializer_core.datetime_to_string(factories.create_timestamp())
        if expires:
            result['expiration']=serializer_core.datetime_to_string(expires)
        return result


    ''' DB object factories '''
    @staticmethod
    def create_user_object(name='test-user', email='test@domain.com', password='password', save=True):
        if save:
            query_set=User.objects.filter(username=name)
            query_set.delete()
            # use create_user so that password gets hashed properly
            user=User.objects.create_user(username=name, email=email, password=password)
        else:
            user=User(username=name, email=email, password=password)
        return user

    @staticmethod
    def create_staff_object(name='test-staff', email='test@domain.com', password='password', save=True):
        user=GhostsTestFactories.create_user_object(name=name, email=email, password=password, save=save)
        user.is_staff=True
        if save:
            user.save()
        return user

    @staticmethod
    def create_message_object(user, text='test: normal message', latitude=0, longitude=0, altitude=0, created=None, expires=None, status=MODEL_STATUS_OKAY, save=True):
        message=Message()
        message.user=user
        message.text=text
        message.longitude=longitude
        message.latitude=latitude
        message.altitude=altitude
        message.status=status
        message.expiration=expires
        if created:
            message.created=created
        else:
            message.created=factories.create_timestamp()
        if save:
            message.save()
        return message

    @staticmethod
    def create_message_typed_object(user, type, text='test: typed message', latitude=None, longitude=None, altitude=None, created=None, expires=None, save=True):
        message=Message()
        message.user=user
        message.text=text
        message.type=type
        message.longitude=longitude
        message.latitude=latitude
        message.altitude=altitude
        message.expiration=expires
        if created:
            message.created=created
        else:
            message.created=factories.create_timestamp()
        if save:
            message.save()
        return message

    @staticmethod
    def create_settings_object(name='radial', radius_horizontal=50, radius_vertical=100, min_query_interval=20, max_query_interval=60, max_messages_per_query=25, save=True):
        settings=ViewSettings()
        settings.name=name
        settings.radius_horizontal=radius_horizontal
        settings.radius_vertical=radius_vertical
        settings.min_query_interval=min_query_interval
        settings.max_query_interval=max_query_interval
        settings.max_messages_per_query=max_messages_per_query
        if save:
            settings.save()
        return settings

    @staticmethod
    def create_rating_object(user, message, value=1, save=True):
        rating=Rating()
        rating.message=message
        rating.user=user
        rating.value=value
        if save:
            rating.save()
        return rating

    @staticmethod
    def create_flag_object(user, message, save=True):
        flag=Flagged()
        flag.message=message
        flag.user=user
        if save:
            flag.save()
        return flag

    @staticmethod
    def create_feedback_app_object(user, text='test: app feedback', created=None, save=True):
        feedback=Feedback()
        feedback.text=text
        feedback.user=user
        feedback.origin=FEEDBACK_ORIGIN_APP
        if created:
            feedback.created=created
        else:
            feedback.created=factories.create_timestamp()
        if save:
            feedback.save()
        return feedback

    @staticmethod
    def create_feedback_web_object(name='test-name', email='test@domain.com', text='test: web feedback', created=None, save=True):
        feedback=Feedback()
        feedback.text=text
        feedback.name=name
        feedback.email=email
        feedback.origin=FEEDBACK_ORIGIN_WEB
        if created:
            feedback.created=created
        else:
            feedback.created=factories.create_timestamp()
        if save:
            feedback.save()
        return feedback
