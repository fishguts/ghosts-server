"""
created: 6/24/13
@author: curt
"""
import os
from datetime import datetime

from django.conf import settings
from common import debug, builtins, test, Singleton

from common.exceptions import StatusException
from common.objects import Status
from common.test import *
from common.utils import dateutils, StringXT, ArrayXT


class GhostsCommonTests(TestCaseDebug):
    """
    Tests for our common module
    """
    ''' test our base test case methods '''
    def test_assert_not_called(self):
        self.assertNotCalled('common.test.passthru_inner', sum, (1, 2))

    def test_assert_is_called(self):
        self.assertIsCalled('common.test.passthru_inner', passthru_outter)


    ''' builtins '''
    def test_builtin_unique(self):
        self.assertItemsEqual(builtins.unique([0,0,1,1,2,2]), (0,1,2))

    def test_builtin_fopen_failure(self):
        path=os.path.join(getattr(settings, 'PROJECT_ROOT'), 'data/test/does-not-exist')
        self.assertRaises(IOError, builtins.fopen, path=path, mode='r', callback=test.passthru_outter)

    def test_builtin_fopen_success(self):
        path=os.path.join(getattr(settings, 'PROJECT_ROOT'), 'data/test/import_message.csv')
        self.assertIsCalled('common.test.passthru_inner', builtins.fopen, path=path, mode='r', callback=test.passthru_outter)

    def test_builtin_for_each(self):
        target=[]; source=range(0,4)
        builtins.for_each(lambda i:target.append(i), source)
        self.assertSequenceEqual(source, target)

    def test_builtin_apply_each(self):
        source=builtins.apply_each(lambda i:i*i, range(0,4))
        self.assertSequenceEqual(source, [0,1,4,9])


    ''' common.debug '''
    def test_debug_assert(self):
        io=self.setupStdErr()
        self.assertEqual(io.len, 0)
        debug.cassert(False)
        self.assertGreater(io.len, 0)
        self.restoreStdErr()

    ''' common.data '''
    def test_date_YMDHM_or_YMDHMS(self):
        self.assertEqual(dateutils.YMDHM_or_YMDHMS_to_date('2000/01/02 12:00'), datetime(year=2000, month=1, day=2, hour=12))
        self.assertEqual(dateutils.YMDHM_or_YMDHMS_to_date('2000/01/02 12:00:01'), datetime(year=2000, month=1, day=2, hour=12, second=1))

    def test_date_MDYHM_or_MDYHMS(self):
        self.assertEqual(dateutils.MDYHM_or_MDYHMS_to_date('01/02/2000 12:00'), datetime(year=2000, month=1, day=2, hour=12))
        self.assertEqual(dateutils.MDYHM_or_MDYHMS_to_date('01/02/2000 12:00:01'), datetime(year=2000, month=1, day=2, hour=12, second=1))

    def test_date_text_to_date_without_default(self):
        self.assertRaises(Exception, dateutils.text_to_date, '01/02/2000')
        self.assertRaises(Exception, dateutils.text_to_date, '12:00')
        self.assertEqual(dateutils.text_to_date('01/02/2000 12:00'), datetime(year=2000, month=1, day=2, hour=12))
        self.assertEqual(dateutils.text_to_date('2000/01/02 12:00'), datetime(year=2000, month=1, day=2, hour=12))

    def test_date_text_to_date_with_default(self):
        dfault=datetime(year=2010, month=10, day=11, hour=12, minute=13, second=14)
        self.assertEqual(dateutils.text_to_date('01/02/2000', dfault), datetime(year=2000, month=1, day=2, hour=dfault.hour, minute=dfault.minute, second=dfault.second))
        self.assertEqual(dateutils.text_to_date('12:00', dfault), datetime(year=dfault.year, month=dfault.month, day=dfault.day, hour=12, minute=0, second=0))
        self.assertEqual(dateutils.text_to_date('12:00:01', dfault), datetime(year=dfault.year, month=dfault.month, day=dfault.day, hour=12, minute=0, second=1))

    ''' common.* '''
    def test_singleton(self):
        class Loner(Singleton): pass
        self.assertEqual(id(Loner()), id(Loner()))

    def test_array_safe_len(self):
        self.assertEqual(ArrayXT.safe_len(None), 0)
        self.assertEqual(ArrayXT.safe_len([]), 0)
        self.assertEqual(ArrayXT.safe_len([1]), 1)

    def test_array_safe_value(self):
        self.assertEqual(ArrayXT.safe_value(None, 0), None)
        self.assertEqual(ArrayXT.safe_value(None, 0, True), True)
        self.assertEqual(ArrayXT.safe_value([1], 0, True), 1)


    def test_string_empty(self):
        self.assertFalse(StringXT.is_empty('a'))
        self.assertTrue(StringXT.is_not_empty('a'))

        self.assertFalse(StringXT.is_empty(' '))
        self.assertTrue(StringXT.is_not_empty(' '))

        self.assertTrue(StringXT.is_empty(''))
        self.assertFalse(StringXT.is_not_empty(''))

        self.assertTrue(StringXT.is_empty(None))
        self.assertFalse(StringXT.is_not_empty(None))

    def test_string_safe(self):
        self.assertEqual(StringXT.safe('a'), 'a')
        self.assertEqual(StringXT.safe(' '), ' ')
        self.assertEqual(StringXT.safe(''), '')
        self.assertEqual(StringXT.safe('', 'a'), '')
        self.assertEqual(StringXT.safe(None), '')
        self.assertEqual(StringXT.safe(None, 'a'), 'a')

    def test_string_safe_len(self):
        self.assertEqual(StringXT.safe_len('a'), 1)
        self.assertEqual(StringXT.safe_len(' '), 1)
        self.assertEqual(StringXT.safe_len(''), 0)
        self.assertEqual(StringXT.safe_len(None), 0)

    def test_string_ellipsis(self):
        self.assertEqual(StringXT.ellipsis('1234567890', 10), '1234567890')
        self.assertEqual(StringXT.ellipsis('1234567890', 9), '123456...')
        self.assertEqual(StringXT.ellipsis('123', 3), '123')
        self.assertEqual(StringXT.ellipsis('123', 2), '...')
        self.assertEqual(StringXT.ellipsis('123', 1), '...')
        self.assertEqual(StringXT.ellipsis('123', 0), '...')

    def test_status(self):
        status=Status('code', 'text')
        self.assertEqual(status.code, 'code')
        self.assertEqual(status.text, 'text')
        try:
            raise StatusException(Status('a', 'b'))
        except StatusException as exception:
            self.assertEqual(type(exception.status), Status)
            self.assertEqual(exception.status.code, 'a')
            self.assertEqual(exception.status.text, 'b')

