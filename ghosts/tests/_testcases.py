"""
created: 7/21/13
@author: curt
"""
from datetime import timedelta

from common.test import TestCaseDebug
from ghosts import models as model_ghosts
from ghosts import constants as constants_ghosts
from ghosts.serializers import core as serializer_core


class TestCaseGhosts(TestCaseDebug):
    """
    Base class with built in test support for Ghosts
    """

    ''' Protected Support: object <--> internal representation '''
    def _assert_status_data_equals_status(self, data, status):
        self.assertEqual(status.code, data['code'])
        self.assertEqual(status.text, data['text'])

    def _assert_setting_data_equals_settings(self, data, setting):
        self.assertEqual(data['name'], setting.name)
        self.assertEqual(data['radiusHorizontal'], setting.radius_horizontal)
        self.assertEqual(data['radiusVertical'], setting.radius_vertical)
        self.assertEqual(data['queryIntervalMin'], setting.min_query_interval)
        self.assertEqual(data['queryIntervalMax'], setting.max_query_interval)
        self.assertEqual(data['messagesPerQueryMax'], setting.max_messages_per_query)

    def _assert_user_data_equals_user(self, data, user):
        self.assertEqual(data['username'], user.username)
        self.assertEqual(data['created'], serializer_core.datetime_to_string(user.date_joined))
        if data.has_key('last_login'):
            self.assertEqual(data['last_login'], serializer_core.datetime_to_string(user.last_login))
        else:
            self.assertIsNone(user.last_login)

    def _assert_message_data_equals_message(self, data, message, user):
        """
        asserts that internal representation matches a message object
        """
        self.assertIsInstance(message, model_ghosts.Message)
        self.assertEqual(message.user, user)
        self.assertIsNone(message.image_thumb)  # not supported right now
        self.assertIsNone(message.image_full)   # not supported right now
        self.assertEqual(message.text, data['text'])
        self.assertEqual(message.longitude, data['longitude'])
        self.assertEqual(message.latitude, data['latitude'])
        self.assertEqual(message.altitude, data['altitude'])
        if data.has_key('type'):
            self.assertEqual(data['type'], constants_ghosts.code_to_response_message(message.type))
        if data.has_key('expiration'):
            delta=timedelta(seconds=data['expiration'])
            self.assertEqual(message.created, message.expiration-delta)
        else:
            self.assertIsNone(message.expiration)

    def _assert_message_response_data_equals_params(self, data, message, user, rating_likes=0, rating_dislikes=0, rating_user=None, flagged_user=None):
        """
        Performs _assert_message_data_equals_message. Additionally looks for properties serialized in a get message request.
        """
        self._assert_message_data_equals_message(data=data, message=message, user=user)
        self.assertIsNotNone(data['type'])
        if data['type']==constants_ghosts.MESSAGE_TYPE_USER[1]:
            self.assertTrue(data['supportsFeedback'])
        else:
            self.assertFalse(data['supportsFeedback'])
        self.assertEqual(data['ratingLikes'], rating_likes)
        self.assertEqual(data['ratingDislikes'], rating_dislikes)
        if rating_user is None:
            self.assertFalse(data.has_key('ratingUser'))
        else:
            self.assertEqual(data['ratingUser'], rating_user)
        if flagged_user is None:
            self.assertFalse(data.has_key('flaggedUser'))
        else:
            self.assertEqual(data['flaggedUser'], flagged_user)

    def _assert_rating_data_equals_rating(self, data, rating, user):
        self.assertIsInstance(rating, model_ghosts.Rating)
        self.assertEqual(rating.message.id, data['messageId'])
        self.assertEqual(rating.value, data['rating'])
        self.assertEqual(rating.user, user)

    def _assert_flag_data_equals_flag(self, data, flag, user):
        self.assertIsInstance(flag, model_ghosts.Flagged)
        self.assertEqual(flag.message.id, data['messageId'])
        self.assertEqual(flag.user, user)

    def _assert_application_feedback_data_equals_feedback(self, data, feedback, user, flag=constants_ghosts.FLAG_NONE):
        self.assertIsInstance(feedback, model_ghosts.Feedback)
        self.assertEqual(feedback.text, data['text'])
        self.assertEqual(feedback.device, data['device'])
        self.assertEqual(feedback.flag, flag)
        self.assertEqual(feedback.user, user)

