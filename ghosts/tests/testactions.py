"""
created: 4/13/13
@author: curt
"""
from django.contrib.auth.models import User
from django.core import mail
from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase

from ghosts import actions
from ghosts import constants
from ghosts.models import Message, Flagged, Feedback
from ghosts.tests._testfactories import GhostsTestFactories


class GhostsActionTests(TestCase):
    """
    Admintool action tests
    """

    ''' User Action Tests '''
    def test_user_cancel_account(self):
        for send_email in (False, True):
            user = GhostsTestFactories.create_user_object(name='test_cancel')
            # assert starting state and run action
            self.assertTrue(user.is_active)
            actions.user_cancel_account(user=user, send_email=send_email)
            # re-fetch our objects as to make sure changes were persisted
            user = User.objects.get(id=user.id)
            self.assertFalse(user.is_active)
            self.assertEqual(user.get_profile().notes, 'User initiated cancel')
            self.assertEqual(len(mail.outbox), 1 if send_email else 0)

    def test_user_revoke_account(self):
        for send_email in (False, True):
            user = GhostsTestFactories.create_user_object(name='test_revoke')
            # assert starting state and run action
            self.assertTrue(user.is_active)
            actions.user_revoke_account(user=user, send_email=send_email)
            # re-fetch our objects as to make sure changes were persisted
            user = User.objects.get(id=user.id)
            self.assertFalse(user.is_active)
            self.assertEqual(user.get_profile().notes, 'Revoked by Goggles')
            self.assertEqual(len(mail.outbox), 1 if send_email else 0)


    ''' Message Flag Action Tests '''
    def test_message_flag_apply(self):
        for send_warning in (False, True):
            user_message = GhostsTestFactories.create_user_object(name='test_write_message')
            user_flag = GhostsTestFactories.create_user_object(name='test_flag_message')
            message = GhostsTestFactories.create_message_object(user_message)
            flag = GhostsTestFactories.create_flag_object(user_flag, message)
            # assert starting state and run action
            self.assertEqual(message.status, constants.MODEL_STATUS_OKAY)
            if send_warning:
                actions.message_flag_apply_and_send_warning(flag)
            else:
                actions.message_flag_apply(flag)
            # re-fetch our objects as to make sure changes were persisted
            user_message = User.objects.get(id=user_message.id)
            user_flag = User.objects.get(id=user_flag.id)
            message = Message.objects.get(id=message.id)
            flag = Flagged.objects.get(id=flag.id)
            self.assertTrue(user_message.is_active)
            self.assertTrue(user_flag.is_active)
            if send_warning:
                self.assertEqual(user_message.get_profile().notes, 'Warned due to flagged content')
                self.assertEqual(len(mail.outbox), 1)
            else:
                self.assertRaises(ObjectDoesNotExist, user_flag.get_profile)
                self.assertEqual(len(mail.outbox), 0)
            self.assertEqual(message.status, constants.MODEL_STATUS_FLAG)
            self.assertTrue(flag.processed)


    def test_message_flag_apply_and_revoke(self):
        user_message = GhostsTestFactories.create_user_object(name='test_write_message')
        user_flag = GhostsTestFactories.create_user_object(name='test_flag_message')
        message = GhostsTestFactories.create_message_object(user_message)
        flag = GhostsTestFactories.create_flag_object(user_flag, message)
        # assert starting state and run action
        self.assertEqual(message.status, constants.MODEL_STATUS_OKAY)
        actions.message_flag_apply_and_revoke_account(flag)
        # re-fetch our objects as to make sure changes were persisted
        user_message = User.objects.get(id=user_message.id)
        user_flag = User.objects.get(id=user_flag.id)
        message = Message.objects.get(id=message.id)
        flag = Flagged.objects.get(id=flag.id)
        self.assertFalse(user_message.is_active)
        self.assertTrue(user_flag.is_active)
        self.assertEqual(user_message.get_profile().notes, 'Revoked by Goggles')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(message.status, constants.MODEL_STATUS_FLAG)
        self.assertTrue(flag.processed)


    def test_message_flag_reject(self):
        user_message = GhostsTestFactories.create_user_object(name='test_write_message')
        user_flag = GhostsTestFactories.create_user_object(name='test_flag_message')
        message = GhostsTestFactories.create_message_object(user_message)
        flag = GhostsTestFactories.create_flag_object(user_flag, message)
        # assert starting state and run action
        self.assertEqual(message.status, constants.MODEL_STATUS_OKAY)
        actions.message_flag_reject(flag)
        # re-fetch our objects as to make sure changes were persisted
        user_message = User.objects.get(id=user_message.id)
        user_flag = User.objects.get(id=user_flag.id)
        message = Message.objects.get(id=message.id)
        flag = Flagged.objects.get(id=flag.id)
        self.assertTrue(user_message.is_active)
        self.assertTrue(user_flag.is_active)
        self.assertEqual(message.status, constants.MODEL_STATUS_OKAY)
        self.assertRaises(ObjectDoesNotExist, user_flag.get_profile)
        self.assertTrue(flag.processed)
        self.assertEqual(len(mail.outbox), 0)


    def test_message_flag_inversion(self):
        """
        Flip flop the state of message between flagged and un-flagged
        """
        user_message = GhostsTestFactories.create_user_object(name='test_write_message')
        user_flag = GhostsTestFactories.create_user_object(name='test_flag_message')
        message = GhostsTestFactories.create_message_object(user_message)
        flag = GhostsTestFactories.create_flag_object(user_flag, message)
        # assert starting state and run action
        self.assertEqual(message.status, constants.MODEL_STATUS_OKAY)
        for flag_apply in (True, False, True):
            if flag_apply:
                actions.message_flag_apply(flag)
            else:
                actions.message_flag_reject(flag)
            # re-fetch our objects as to make sure changes were persisted
            message = Message.objects.get(id=message.id)
            flag = Flagged.objects.get(id=flag.id)
            self.assertTrue(flag.processed)
            self.assertEqual(len(mail.outbox), 0)
            if flag_apply:
                self.assertEqual(message.status, constants.MODEL_STATUS_FLAG)
            else:
                self.assertEqual(message.status, constants.MODEL_STATUS_OKAY)


    def test_multiple_flags_per_message(self):
        """
        Make sure that flag is only applied once to message and that all flags are "processed"
        """
        flag_users = []
        flags_apply = []
        flags_reject = []
        user_message = GhostsTestFactories.create_user_object(name='test_write_message')
        message_clean = GhostsTestFactories.create_message_object(user_message)
        message_dirty = GhostsTestFactories.create_message_object(user_message)
        # many flags by many users over the same two messages
        for index in range(0, 5):
            flag_users.append(GhostsTestFactories.create_user_object(name='test_flag_message-{0}'.format(index)))
            flags_apply.append(GhostsTestFactories.create_flag_object(flag_users[-1], message_dirty))
            flags_reject.append(GhostsTestFactories.create_flag_object(flag_users[-1], message_clean))

        # 0->2: make sure that we are okay with multiple applications
        for index in range(0, 2):
            actions.message_flag_apply(flags_apply[0])
            actions.message_flag_reject(flags_reject[0])
            message_clean = Message.objects.get(id=message_clean.id)
            message_dirty = Message.objects.get(id=message_dirty.id)
            self.assertEqual(message_clean.status, constants.MODEL_STATUS_OKAY)
            self.assertEqual(message_dirty.status, constants.MODEL_STATUS_FLAG)
            for flag in flags_apply:
                flag = Flagged.objects.get(id=flag.id)
                self.assertTrue(flag.processed)
            for flag in flags_reject:
                flag = Flagged.objects.get(id=flag.id)
                self.assertTrue(flag.processed)


    def test_flag_escalation_sequence(self):
        """
        Warn -> Revoke and then repeat sequence and make sure nothing is reverted or added
        """
        user_message = GhostsTestFactories.create_user_object(name='test_write_message')
        user_flag = GhostsTestFactories.create_user_object(name='test_flag_message')
        message = GhostsTestFactories.create_message_object(user_message)
        flag = GhostsTestFactories.create_flag_object(user_flag, message)

        # make sure that nothing changes should updates attempt to get re-applied
        for index in range(0, 2):
            actions.message_flag_apply_and_send_warning(flag=flag)
            if index==0:
                self.assertEqual(user_message.get_profile().notes, 'Warned due to flagged content')
                self.assertEqual(len(mail.outbox), 1)
            actions.user_revoke_account(user=user_message, send_email=True)
            self.assertEqual(user_message.get_profile().notes, 'Warned due to flagged content|Revoked by Goggles')
            self.assertEqual(len(mail.outbox), 2)


    ''' Feedback Action Tests '''
    def test_feedback_update(self):
        user = GhostsTestFactories.create_user_object()
        for flag in (constants.FLAG_CONFIRMED_CONTENT, constants.FLAG_CONFIRMED_SPAM, constants.FLAG_CONFIRMED_ORIGIN):
            for action in (constants.ACTION_CLOSED, constants.ACTION_FLAGGED, constants.ACTION_NONE, constants.ACTION_REPLIED):
                for index in range(0, 2):
                    if index==0:
                        feedback = GhostsTestFactories.create_feedback_app_object(user)
                    else:
                        feedback = GhostsTestFactories.create_feedback_web_object()
                    actions.feedback_update(feedback=feedback, flag=flag, action=action)
                    feedback = Feedback.objects.get(id=feedback.id)
                    self.assertEqual(feedback.flag, flag)
                    self.assertEqual(feedback.action, action)


    def test_feedback_cancel_account(self):
        user = GhostsTestFactories.create_user_object()
        # web-based: should not be able to cancel web based feedback (no account)
        feedback = GhostsTestFactories.create_feedback_web_object()
        self.assertRaises(Exception, actions.feedback_cancel_account, feedback=feedback)
        self.assertEqual(feedback.action, constants.ACTION_NONE)
        self.assertEqual(len(mail.outbox), 1)   # should sent an admin email

        # app-based: 1st time
        feedback = GhostsTestFactories.create_feedback_app_object(user)
        actions.feedback_cancel_account(feedback=feedback, send_email=True)
        feedback = Feedback.objects.get(id=feedback.id)
        user = User.objects.get(id=user.id)
        self.assertEqual(feedback.action, constants.ACTION_CLOSED)
        self.assertEqual(user.get_profile().notes, 'User initiated cancel')
        self.assertEqual(len(mail.outbox), 2)

        # app-based: 2nd time - make sure it doesn't harass user
        actions.feedback_cancel_account(feedback=feedback, send_email=True)
        feedback = Feedback.objects.get(id=feedback.id)
        self.assertEqual(feedback.action, constants.ACTION_CLOSED)
        self.assertEqual(user.get_profile().notes, 'User initiated cancel')
        self.assertEqual(len(mail.outbox), 2)

