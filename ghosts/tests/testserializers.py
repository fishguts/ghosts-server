"""
created: 12/12/12
@author: curt
"""

from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist

from common import objects as common_objects, builtins
from common import constants as common_constants
from ghosts import constants, factories
from ghosts.serializers import core as serializer_core
from ghosts.serializers import csv as serializer_csv
from ghosts.serializers import json as serializer_json
from ghosts.serializers import model as serializer_model
from ghosts.tests._testcases import TestCaseGhosts
from ghosts.tests._testfactories import GhostsTestFactories

class GhostsSerializerCoreTests(TestCaseGhosts):
    """
    Test core fellas
    """
    def test_date_serialize_none(self):
        self.assertRaises(Exception, serializer_core.datetime_to_string, None)

    def test_date_serialize_12(self):
        # note: make sure microseconds are stripped off
        date=datetime(year=2000, month=5, day=10, hour=10, minute=30, second=45, microsecond=12)
        text=serializer_core.datetime_to_string(date)
        self.assertEqual(text, '2000-05-10T10:30:45')

    def test_date_serialize_24(self):
        # note: make sure microseconds are stripped off
        date=datetime(year=2000, month=12, day=31, hour=22, minute=0, second=0, microsecond=12)
        text=serializer_core.datetime_to_string(date)
        self.assertEqual(text, '2000-12-31T22:00:00')

    def test_date_deserialize_invalid(self):
        self.assertRaises(TypeError, serializer_core.string_to_datetime, None)
        self.assertRaises(ValueError, serializer_core.string_to_datetime, 'invalid')
        self.assertRaises(ValueError, serializer_core.string_to_datetime, '20-05-10T10:30:45')
        self.assertRaises(ValueError, serializer_core.string_to_datetime, '2000-00-10T10:30:45')
        self.assertRaises(ValueError, serializer_core.string_to_datetime, '2000-01-00T10:30:45')

    def test_date_deserialize_12(self):
        self.assertEqual(serializer_core.string_to_datetime('2000-05-10T10:30:45'),
            datetime(year=2000, month=5, day=10, hour=10, minute=30, second=45))

    def test_date_deserialize_24(self):
        self.assertEqual(serializer_core.string_to_datetime('2000-05-10T13:00:00'),
            datetime(year=2000, month=5, day=10, hour=13, minute=0, second=0))

    def test_date_serialize_deserialize_parity(self):
        date_now=factories.create_timestamp()
        date_parsed=serializer_core.string_to_datetime(serializer_core.datetime_to_string(date_now))
        self.assertEqual(date_now.year, date_parsed.year)
        self.assertEqual(date_now.month, date_parsed.month)
        self.assertEqual(date_now.day, date_parsed.day)
        self.assertEqual(date_now.hour, date_parsed.hour)
        self.assertEqual(date_now.minute, date_parsed.minute)
        self.assertEqual(date_now.second, date_parsed.second)


    def test_code_to_response_message(self):
        self.assertEqual(constants.code_to_response_message(constants.MESSAGE_TYPE_ADVERTISEMENT[0]), constants.MESSAGE_TYPE_ADVERTISEMENT[1])
        self.assertEqual(constants.code_to_response_message(constants.MESSAGE_TYPE_NOTIFICATION[0]), constants.MESSAGE_TYPE_NOTIFICATION[1])
        self.assertEqual(constants.code_to_response_message(constants.MESSAGE_TYPE_USER[0]), constants.MESSAGE_TYPE_USER[1])
        self.assertEqual(constants.code_to_response_message(constants.MESSAGE_TYPE_WELCOME[0]), constants.MESSAGE_TYPE_WELCOME[1])
        self.assertIsNone(constants.code_to_response_message('-'))
        self.assertRaises(ValueError, constants.code_to_response_message, code='-', unknown_handler=builtins.fail_to_value_error)

    def test_response_to_code_message(self):
        self.assertEqual(constants.response_to_code_message(constants.MESSAGE_TYPE_ADVERTISEMENT[1]), constants.MESSAGE_TYPE_ADVERTISEMENT[0])
        self.assertEqual(constants.response_to_code_message(constants.MESSAGE_TYPE_NOTIFICATION[1]), constants.MESSAGE_TYPE_NOTIFICATION[0])
        self.assertEqual(constants.response_to_code_message(constants.MESSAGE_TYPE_USER[1]), constants.MESSAGE_TYPE_USER[0])
        self.assertEqual(constants.response_to_code_message(constants.MESSAGE_TYPE_WELCOME[1]), constants.MESSAGE_TYPE_WELCOME[0])
        self.assertIsNone(constants.response_to_code_message('-'))
        self.assertRaises(ValueError, constants.response_to_code_message, response='-', unknown_handler=builtins.fail_to_value_error)

    def test_expiration_code_to_value(self):
        self.assertEqual(serializer_core.expiration_code_to_duration('hour'), 60*60)
        self.assertEqual(serializer_core.expiration_code_to_duration('day'), 24*60*60)
        self.assertEqual(serializer_core.expiration_code_to_duration('month'), 31*24*60*60)
        self.assertEqual(serializer_core.expiration_code_to_duration('year'), 365*24*60*60)
        self.assertRaises(ValueError, serializer_core.expiration_code_to_duration, code='-', unknown_handler=builtins.fail_to_value_error)


class GhostsSerializerCsvTests(TestCaseGhosts):
    def test_to_messages_none(self):
        self.assertRaises(Exception, serializer_csv.to_messages, None)

    def test_to_messages_invalid(self):
        self.assertRaises(ValueError, serializer_csv.to_messages, [""])

    def test_to_messages_unknown_type(self):
        data=[
            "testcase,Testcase: no punctuation,-40.1,30.1,0,unknown,,"
        ]
        self.assertRaises(ValueError, serializer_csv.to_messages, rows=data)

    def test_to_messages_no_header(self):
        data=[
            "testcase,Testcase: no punctuation,-40.1,30.1,0,user,,"
        ]
        messages=serializer_csv.to_messages(rows=data, header=False)
        self.assertEqual(len(messages), 1); message=messages[0]
        self.assertEqual(message['ownerId'], 'testcase')
        self.assertEqual(message['text'], 'Testcase: no punctuation')
        self.assertEqual(message['latitude'], -40.1)
        self.assertEqual(message['longitude'], 30.1)
        self.assertEqual(message['altitude'], 0)
        self.assertEqual(message['type'], constants.MESSAGE_TYPE_USER[0])
        self.assertFalse('created' in message)
        self.assertFalse('expiration' in message)

    def test_to_messages_with_header(self):
        data=[
            "User,Message,Latitude,Longitude,Altitude,[Type],[Date-created],[Date-expires]",
            "testcase,Testcase: no punctuation,-40.1,30.1,0,user,,"
        ]
        messages=serializer_csv.to_messages(rows=data, header=True)
        self.assertEqual(len(messages), 1); message=messages[0]

    def test_to_messages_no_header_with_comma(self):
        data=[
            'testcase,"Testcase: middle, comma",-40.3,30.3,2,user,9/01/2011 12:00:00,hour'
        ]
        messages=serializer_csv.to_messages(rows=data, header=False)
        self.assertEqual(len(messages), 1); message=messages[0]
        self.assertEqual(message['text'], 'Testcase: middle, comma')
        self.assertEqual(message['created'], datetime(year=2011, month=9, day=1, hour=12))
        self.assertEqual(message['expiration'], 60*60)



class GhostsSerializerJsonTests(TestCaseGhosts):
    """
    Tests JSON and model stuff
    """
    ''' VO and Model serialize/deserialize '''
    def test_status_serialize_to_map(self):
        status=common_objects.Status(common_constants.RESPONSE_CODE_FAIL, 'failure text')
        data=serializer_model.status_to_map(status)
        self._assert_status_data_equals_status(data=data, status=status)

    def test_status_serialize(self):
        status=common_objects.Status(common_constants.RESPONSE_CODE_FAIL, 'failure text')
        data=serializer_json.to_object(serializer_json.from_status(status))['status']
        self._assert_status_data_equals_status(data=data, status=status)

    def test_settings_serialize_to_map(self):
        setting=GhostsTestFactories.create_settings_object()
        data=serializer_model.setting_to_map(setting)
        self._assert_setting_data_equals_settings(data=data, setting=setting)

    def test_settings_serialize(self):
        setting=GhostsTestFactories.create_settings_object()
        data=serializer_json.to_object(serializer_json.from_setting(setting, common_constants.STATUS_OKAY))['settings'][0]
        self._assert_setting_data_equals_settings(data=data, setting=setting)

    def test_user_serialize_to_map(self):
        user=GhostsTestFactories.create_user_object()
        data=serializer_model.user_to_map(user)
        self._assert_user_data_equals_user(data=data, user=user)

    def test_user_serialize(self):
        user=GhostsTestFactories.create_user_object()
        data=serializer_json.to_object(serializer_json.from_user(user, common_constants.STATUS_OKAY))['profile']
        self._assert_user_data_equals_user(data=data, user=user)

    def test_typed_message_serialize_to_map(self):
        self._test_typed_message_serialize_to_map(constants.MESSAGE_TYPE_NOTIFICATION[0])
        self._test_typed_message_serialize_to_map(constants.MESSAGE_TYPE_ADVERTISEMENT[0])
        self._test_typed_message_serialize_to_map(constants.MESSAGE_TYPE_WELCOME[0])

    def test_typed_message_serialize(self):
        self._test_typed_message_serialize(constants.MESSAGE_TYPE_NOTIFICATION[0])
        self._test_typed_message_serialize(constants.MESSAGE_TYPE_ADVERTISEMENT[0])
        self._test_typed_message_serialize(constants.MESSAGE_TYPE_WELCOME[0])

    def test_user_message_serialize_bare_to_map(self):
        """
        Performing one map test for messages.  Will perform the rest of the map tests through the public interface.
        """
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user, latitude=1.0, longitude=2.0, altitude=3.0)
        data=serializer_model.message_to_map(message=message, current_user=user)
        self._assert_message_response_data_equals_params(data=data, message=message, user=user, rating_likes=0, rating_dislikes=0, rating_user=None, flagged_user=False)

    def test_user_message_serialize_bare(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user, latitude=1.0, longitude=2.0, altitude=3.0)
        data=serializer_json.to_object(serializer_json.from_message(message=message, current_user=user, status=common_constants.STATUS_OKAY))['messages'][0]
        self._assert_message_response_data_equals_params(data=data, message=message, user=user, rating_likes=0, rating_dislikes=0, rating_user=None, flagged_user=False)

    def test_user_message_serialize_like(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user)
        GhostsTestFactories.create_rating_object(user=user, message=message, value=1)
        data=serializer_json.to_object(serializer_json.from_message(message=message, current_user=user, status=common_constants.STATUS_OKAY))['messages'][0]
        self._assert_message_response_data_equals_params(data=data, message=message, user=user, rating_likes=1, rating_dislikes=0, rating_user=1, flagged_user=False)

    def test_user_message_serialize_dislike(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user, latitude=1.0, longitude=2.0, altitude=3.0)
        GhostsTestFactories.create_rating_object(user=user, message=message, value=-1)
        data=serializer_json.to_object(serializer_json.from_message(message=message, current_user=user, status=common_constants.STATUS_OKAY))['messages'][0]
        self._assert_message_response_data_equals_params(data=data, message=message, user=user, rating_likes=0, rating_dislikes=1, rating_user=-1, flagged_user=False)

    def test_user_message_serialize_3rd_party_rating(self):
        user_message=GhostsTestFactories.create_user_object('bones')
        user_rating=GhostsTestFactories.create_user_object('jones')
        message=GhostsTestFactories.create_message_object(user=user_message, latitude=1.0, longitude=2.0, altitude=3.0)
        GhostsTestFactories.create_rating_object(user=user_rating, message=message, value=1)
        data=serializer_json.to_object(serializer_json.from_message(message=message, current_user=user_message, status=common_constants.STATUS_OKAY))['messages'][0]
        self._assert_message_response_data_equals_params(data=data, message=message, user=user_message, rating_likes=1, rating_dislikes=0, rating_user=None, flagged_user=False)

    def test_user_message_serialize_flagged(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user, latitude=1.0, longitude=2.0, altitude=3.0)
        GhostsTestFactories.create_flag_object(user=user, message=message)
        data=serializer_json.to_object(serializer_json.from_message(message=message, current_user=user, status=common_constants.STATUS_OKAY))['messages'][0]
        self._assert_message_response_data_equals_params(data=data, message=message, user=user, rating_likes=0, rating_dislikes=0, rating_user=None, flagged_user=True)


    ''' Deserialization tests: deserialization of model objects is a stage of operations. Make sure our fundamental operations is working. '''
    def test_deserialize_invalid_text(self):
        self.assertRaises(ValueError, serializer_json.to_object, '{invalid}')

    def test_message_post_deserialize(self):
        text='{"text":"Savory snacks","altitude":0,"expiration":2678400,"ownerId":"curt","longitude":-73.989,"latitude":40.7414,"created":"2013-07-21T18:30:30"}'
        data=serializer_json.to_object(json_text=text)
        self.assertEqual(data['text'], "Savory snacks")
        self.assertEqual(data['latitude'], 40.7414)
        self.assertEqual(data['longitude'], -73.989)
        self.assertEqual(data['altitude'], 0)
        self.assertEqual(data['ownerId'], "curt")
        self.assertEqual(data['created'], "2013-07-21T18:30:30")
        self.assertEqual(data['expiration'], 2678400)

    def test_rating_deserialize_like(self):
        text='{"messageId":2, "rating":1}'
        data=serializer_json.to_object(text)
        self.assertEqual(data['messageId'], 2)
        self.assertEqual(data['rating'], 1)
        # flip rating and verify results
        text='{"messageId":2, "rating":1}'
        data=serializer_json.to_object(text)
        self.assertEqual(data['messageId'], 2)
        self.assertEqual(data['rating'], 1)

    def test_rating_deserialize_dislike(self):
        text='{"messageId":2, "rating":-1}'
        data=serializer_json.to_object(text)
        self.assertEqual(data['messageId'], 2)
        self.assertEqual(data['rating'], -1)

    def test_flag_deserialize(self):
        text='{"messageId":2}'
        data=serializer_json.to_object(text)
        self.assertEqual(data['messageId'], 2)

    def test_feedback_deserialize(self):
        text='{"text":"test", "device":"platform|type|version"}'
        data=serializer_json.to_object(text)
        self.assertEqual(data['text'], "test")
        self.assertEqual(data['device'], "platform|type|version")


    ''' private utilities '''
    def _test_typed_message_serialize_to_map(self, type):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_typed_object(user=user, type=type)
        data=serializer_model.message_to_map(message=message, current_user=user, latitude=1.0, longitude=2.0, altitude=3.0)
        self.assertTrue(data['type'], constants.code_to_response_message(type))
        self.assertEqual(message.user, user)
        self.assertIsNone(message.image_thumb)
        self.assertIsNone(message.image_full)
        self.assertEqual(message.text, data['text'])
        self.assertEqual(data['latitude'], 1.0)
        self.assertEqual(data['longitude'], 2.0)
        self.assertEqual(data['altitude'], 3.0)
        self.assertFalse(data['supportsFeedback'])
        self.assertFalse(data['flaggedUser'])
        self.assertEqual(data['ratingLikes'], 0)
        self.assertEqual(data['ratingDislikes'], 0)

    def _test_typed_message_serialize(self, type):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_typed_object(user=user, type=type)
        data=serializer_json.to_object(serializer_json.from_message(message, user, common_constants.STATUS_OKAY))['messages'][0]
        self.assertTrue(data['type'], constants.code_to_response_message(type))
        self.assertEqual(message.user, user)
        self.assertIsNone(message.image_thumb)
        self.assertIsNone(message.image_full)
        self.assertIsNone(data['latitude'])
        self.assertIsNone(data['longitude'])
        self.assertIsNone(data['altitude'])
        self.assertEqual(message.text, data['text'])
        self.assertFalse(data['supportsFeedback'])
        self.assertFalse(data['flaggedUser'])
        self.assertEqual(data['ratingLikes'], 0)
        self.assertEqual(data['ratingDislikes'], 0)
