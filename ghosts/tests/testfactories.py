"""
created: 12/12/12
@author: curt
"""
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from ghosts import factories, models
from ghosts.constants import FLAG_SUSPECT_CONTENT
from ghosts.tests._testcases import TestCaseGhosts
from ghosts.tests._testfactories import GhostsTestFactories


class GhostsFactoryTests(TestCaseGhosts):
    """
    Tests our factories
    """
    ''' Primitive tests '''
    def test_timestamp_factory(self):
        self.assertIsInstance(factories.create_timestamp(), datetime)
        delta=factories.create_timestamp()-datetime.utcnow()
        self.assertLess(abs(delta.total_seconds()), 1)

    def test_clone_name(self):
        self.assertEqual(factories.create_clone_name(''), ' clone')
        self.assertEqual(factories.create_clone_name('Bill'), 'Bill clone')


    ''' Internal to model tests '''
    def test_message_post_factory_no_expiration(self):
        user=GhostsTestFactories.create_user_object()
        data={'text':'test', 'longitude':1, 'latitude':2, 'altitude':3}
        message=factories.data_to_message_post(data=data, current_user=user)
        self._assert_message_data_equals_message(data=data, message=message, user=user)

    def test_message_post_factory_with_expiration(self):
        user=GhostsTestFactories.create_user_object()
        data={'text':'test', 'longitude':1, 'latitude':2, 'altitude':3, 'expiration':60}
        message=factories.data_to_message_post(data=data, current_user=user)
        self._assert_message_data_equals_message(data=data, message=message, user=user)

    def test_message_post_factory_with_text_coordinates(self):
        """
        We don't expect non numeric types, nonetheless there is nothing to stop them so
        it will be good to know what behavior to expect
        """
        user=GhostsTestFactories.create_user_object()
        data={'text':'test', 'longitude':'1', 'latitude':'2', 'altitude':'3'}
        message=factories.data_to_message_post(data=data, current_user=user)
        message.save(); message=models.Message.objects.get(pk=message.pk)
        self.assertIsInstance(message.longitude, float)
        self.assertEqual(message.longitude, 1.0)
        self.assertIsInstance(message.latitude, float)
        self.assertEqual(message.latitude, 2.0)
        self.assertIsInstance(message.altitude, float)
        self.assertEqual(message.altitude, 3.0)

    def test_message_delete_factory_not_exist(self):
        user=GhostsTestFactories.create_user_object(name='curt')
        self.assertRaises(ObjectDoesNotExist, factories.data_to_message_delete, data={'messageId':1}, current_user=user)

    def test_message_delete_factory_wrong_user(self):
        user1=GhostsTestFactories.create_user_object(name='curt')
        user2=GhostsTestFactories.create_user_object(name='burt')
        message1=GhostsTestFactories.create_message_object(user=user1)
        self.assertEqual(models.Message.objects.all().count(), 1)
        self.assertRaises(Exception, factories.data_to_message_delete, data={'messageId':message1.id}, current_user=user2)

    def test_message_delete_factory_success(self):
        user1=GhostsTestFactories.create_user_object(name='curt')
        message1=GhostsTestFactories.create_message_object(user=user1)
        self.assertEqual(models.Message.objects.all().count(), 1)
        result=factories.data_to_message_delete(data={'messageId':message1.id}, current_user=user1)
        self.assertIsInstance(result, models.Message)
        self.assertEqual(message1.id, result.id)

    def test_rating_factory_not_exist(self):
        user=GhostsTestFactories.create_user_object()
        data={'messageId':-1, 'rating':1}
        rating=factories.data_to_rating(data, user)
        self.assertRaises(IntegrityError, rating.save)

    def test_rating_factory_like(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user)
        data={'messageId':message.id, 'rating':1}
        rating=factories.data_to_rating(data, user)
        self._assert_rating_data_equals_rating(data=data, rating=rating, user=user)

    def test_rating_factory_dislike(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user)
        data={'messageId':message.id, 'rating':-1}
        rating=factories.data_to_rating(data, user)
        self._assert_rating_data_equals_rating(data=data, rating=rating, user=user)

    def test_flag_factory_not_exist(self):
        user=GhostsTestFactories.create_user_object()
        data={'messageId':-1}
        flag=factories.data_to_flag(data, user)
        self.assertRaises(IntegrityError, flag.save)

    def test_flag_factory_success(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user=user)
        data={'messageId':message.id}
        flag=factories.data_to_flag(data, user)
        self._assert_flag_data_equals_flag(data=data, flag=flag, user=user)

    def test_feedback_factory_not_suspicious(self):
        user=GhostsTestFactories.create_user_object()
        data={'text':'test', 'device':'platform|type|version'}
        feedback=factories.data_to_feedback(data, user)
        self._assert_application_feedback_data_equals_feedback(data=data, feedback=feedback, user=user)

    def test_feedback_factory_suspicious(self):
        user=GhostsTestFactories.create_user_object()
        data={'text':'you fuck donkeys', 'device':'ios|iPhone|6.1'}
        feedback=factories.data_to_feedback(data, user)
        self._assert_application_feedback_data_equals_feedback(data=data, feedback=feedback, user=user, flag=FLAG_SUSPECT_CONTENT)
