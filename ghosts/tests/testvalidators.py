"""
created: 3/24/13
@author: curt
"""

from django.test import TestCase
from common.exceptions import StatusException
from ghosts import validators
from ghosts.tests._testfactories import GhostsTestFactories


class GhostsValidatorTests(TestCase):
    ''' Tests '''
    def test_message_query_object_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_message_query_object, None)
        self.assertRaises(ValueError, validators.validate_message_query_object, {})
        self.assertRaises(ValueError, validators.validate_message_query_object, {'latitude':0, 'longitude':0, 'altitude':0})
        self.assertRaises(ValueError, validators.validate_message_query_object, {'view':'radial', 'longitude':0, 'altitude':0})
        self.assertRaises(ValueError, validators.validate_message_query_object, {'view':'radial', 'latitude':0, 'altitude':0})
        self.assertRaises(ValueError, validators.validate_message_query_object, {'view':'radial', 'latitude':0, 'longitude':0})

    def test_message_query_object_valid(self):
        self.assertIsNone(validators.validate_message_query_object({'view':'radial', 'latitude':0, 'longitude':0, 'altitude':0}))

    def test_message_post_object_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_message_post_object, None)
        self.assertRaises(ValueError, validators.validate_message_post_object, data={})
        self.assertRaises(ValueError, validators.validate_message_post_object, {'latitude':0, 'longitude':0, 'altitude':0})
        self.assertRaises(ValueError, validators.validate_message_post_object, {'text':'message', 'longitude':0, 'altitude':0})
        self.assertRaises(ValueError, validators.validate_message_post_object, {'text':'message', 'latitude':0, 'altitude':0})
        self.assertRaises(ValueError, validators.validate_message_post_object, {'text':'message', 'latitude':0, 'longitude':0})

    def test_message_post_object_exceeds_limits(self):
        self.assertRaises(StatusException, validators.validate_message_post_object,
            data={'text':GhostsTestFactories.get_message_text_short(), 'latitude':0, 'longitude':0, 'altitude':0})
        self.assertRaises(StatusException, validators.validate_message_post_object,
            data={'text':GhostsTestFactories.get_message_text_long(), 'latitude':0, 'longitude':0, 'altitude':0})

    def test_message_post_object_valid(self):
        self.assertIsNone(validators.validate_message_post_object({'text':GhostsTestFactories.get_message_text_valid(), 'latitude':0, 'longitude':0, 'altitude':0}))

    def test_message_delete_object_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_message_delete_object, None)
        self.assertRaises(ValueError, validators.validate_message_delete_object, data={})

    def test_message_delete_object_valid(self):
        self.assertIsNone(validators.validate_message_delete_object({'messageId':'dummy'}))

    def test_message_rate_object_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_message_rate_object, None)
        self.assertRaises(ValueError, validators.validate_message_rate_object, data={})
        self.assertRaises(ValueError, validators.validate_message_rate_object, data={'messageId':'dummy'})
        self.assertRaises(ValueError, validators.validate_message_rate_object, data={'rating':1})

    def test_message_rate_object__valid(self):
        self.assertIsNone(validators.validate_message_rate_object({'messageId':'dummy', 'rating':1}))

    def test_message_flag_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_message_flag_object, None)
        self.assertRaises(ValueError, validators.validate_message_flag_object, data={})

    def test_message_flag_object_valid(self):
        self.assertIsNone(validators.validate_message_flag_object({'messageId':'dummy'}))

    def test_perimeter_query_object_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_perimeter_query_object, None)
        self.assertRaises(ValueError, validators.validate_perimeter_query_object, {})
        self.assertRaises(ValueError, validators.validate_perimeter_query_object, {'latitude':0, 'longitude':0})
        self.assertRaises(ValueError, validators.validate_perimeter_query_object, {'latitude':0, 'radius':0})
        self.assertRaises(ValueError, validators.validate_perimeter_query_object, {'longitude':0, 'radius':0})
        self.assertRaises(StatusException, validators.validate_perimeter_query_object, {'latitude':0, 'longitude':0, 'radius':0})

    def test_perimeter_query_object_missing_valid(self):
        self.assertIsNone(validators.validate_perimeter_query_object({'latitude':0, 'longitude':0, 'radius':100}))

    def test_feedback_object_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_feedback_object, None)
        self.assertRaises(ValueError, validators.validate_feedback_object, data={})
        self.assertRaises(ValueError, validators.validate_feedback_object, data={'text':'feedback'})
        self.assertRaises(ValueError, validators.validate_feedback_object, data={'device':'test'})

    def test_feedback_object_text_exceeds_limits(self):
        self.assertRaises(StatusException, validators.validate_feedback_object,
            data={'text':GhostsTestFactories.get_feedback_text_short(), 'device':'test'})
        self.assertRaises(StatusException, validators.validate_feedback_object,
            data={'text':GhostsTestFactories.get_feedback_text_long(), 'device':'test'})

    def test_feedback_object_valid(self):
        self.assertIsNone(validators.validate_feedback_object(
            {'text':GhostsTestFactories.get_feedback_text_valid(), 'device':'test'}))

    def test_login_object_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_login_object, None)
        self.assertRaises(ValueError, validators.validate_login_object, data={})
        self.assertRaises(ValueError, validators.validate_login_object, data={'username':'dummy'})
        self.assertRaises(ValueError, validators.validate_login_object, data={'password':'dummy'})

    def test_login_object_valid(self):
        self.assertIsNone(validators.validate_login_object({'username':'dummy', 'password':'dummy'}))

    def test_logout_object_missing_required(self):
        self.assertRaises(AttributeError, validators.validate_logout_object, None)
        self.assertRaises(ValueError, validators.validate_logout_object, data={})

    def test_logout_object(self):
        self.assertIsNone(validators.validate_logout_object({'username':'dummy'}))
