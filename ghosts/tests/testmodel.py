"""
created: 4/14/13
@author: curt
"""
from datetime import timedelta
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from django.test import TestCase
from ghosts import factories
from ghosts.models import Profile
from ghosts.tests._testfactories import GhostsTestFactories


class GhostsModelTests(TestCase):
    """
    Tests functionality on model objects
    """
    def test_user_profile(self):
        user = GhostsTestFactories.create_user_object()
        self.assertRaises(ObjectDoesNotExist, user.get_profile)
        profile = Profile.get_user_profile(user)
        self.assertEqual(profile.user, user)
        # save and re-query so we can verify all is persisted
        profile.save()
        user = User.objects.get(id=user.id)
        self.assertEqual(user.get_profile(), profile)

    def test_profile_notes(self):
        user = GhostsTestFactories.create_user_object()
        profile = Profile.get_user_profile(user)
        # test notes
        self.assertIsNone(profile.notes)
        profile.add_note('1')
        self.assertEqual(profile.notes, '1')
        profile.add_note('2')
        self.assertEqual(profile.notes, '1|2')

    def test_message_expiration(self):
        user = GhostsTestFactories.create_user_object()
        message = GhostsTestFactories.create_message_object(user=user, expires=None)
        self.assertFalse(message.expired())
        message = GhostsTestFactories.create_message_object(user=user, expires=factories.create_timestamp()+timedelta(minutes=1))
        self.assertFalse(message.expired())
        message = GhostsTestFactories.create_message_object(user=user, expires=factories.create_timestamp()-timedelta(minutes=1))
        self.assertTrue(message.expired())
