"""
created: 12/14/12
@author: curt
"""
import os
from StringIO import StringIO
from datetime import timedelta, datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.core import management, mail

from common.test import TestCaseDebug
from ghosts import factories
from ghosts.models import Message, Feedback, Flagged
from ghosts.tests._testfactories import GhostsTestFactories
from registration import RegistrationProfile


class GhostsCommandTests(TestCaseDebug):
    """
    Tests our command line/manage.py issued commands
    """
    def setUp(self):
        super(GhostsCommandTests, self).setUp()
        self._stock_user_count=User.objects.count()


    ''' Test import message command '''
    def test_import_messages_command_options_missing(self):
        buffer=StringIO(); self.setupStdErr(buffer)
        management.call_command('importmessages', verbosity=3)
        self.assertGreater(buffer.len, 0)

    def test_import_messages_command_file_missing(self):
        self.assertRaises(SystemExit, management.call_command, 'importmessages', file='missing.txt', verbosity=3)

    def test_import_messages_command(self):
        path=os.path.join(getattr(settings, 'PROJECT_ROOT'), 'data/test/import_messages.csv')
        # test dry run
        management.call_command('importmessages', file=path, dry_run=True, verbosity=3)
        self._assert_db_counts(message=0)
        # test wet run
        management.call_command('importmessages', file=path, dry_run=False, verbosity=3)
        self._assert_db_counts(message=8)
        # test duplicate prevention
        management.call_command('importmessages', file=path, dry_run=False, verbosity=3)
        self._assert_db_counts(message=8)
        # test delete dry run
        management.call_command('importmessages', file=path, dry_run=True, delete=True, verbosity=3)
        self._assert_db_counts(message=8)
        # test delete wet run
        management.call_command('importmessages', file=path, dry_run=False, delete=True, verbosity=3)
        self._assert_db_counts(message=0)

    def test_import_messages_command_date_override(self):
        path=os.path.join(getattr(settings, 'PROJECT_ROOT'), 'data/test/import_message.csv')
        management.call_command('importmessages', file=path, created='01/02/2003 04:05:06', verbosity=3)
        self._assert_db_counts(message=1)
        message=Message.objects.all()[0]
        self.assertEqual(message.created, datetime(year=2003, month=1, day=2, hour=4, minute=5, second=6))


    ''' Test cleanup message command '''
    def test_clean_up_messages_command(self):
        date_now=factories.create_timestamp()
        death_row_days=getattr(settings, 'MESSAGE_POST_EXPIRATION_DAYS', 7)
        user=GhostsTestFactories.create_user_object()
        GhostsTestFactories.create_message_object(user, expires=None)
        GhostsTestFactories.create_message_object(user, expires=date_now)
        GhostsTestFactories.create_message_object(user, expires=date_now-timedelta(days=death_row_days, minutes=-1))
        GhostsTestFactories.create_message_object(user, expires=date_now-timedelta(days=death_row_days, minutes=1))
        self.assertEqual(Message.objects.count(), 4)
        # test dry run
        management.call_command('cleanupmessages', dry_run=True, verbosity=2)
        self.assertEqual(Message.objects.count(), 4)
        # test wet run
        management.call_command('cleanupmessages', dry_run=False, verbosity=2)
        self.assertEqual(Message.objects.count(), 3)


    ''' Test create data command '''
    def test_create_test_data_command_invalid_configuration(self):
        management.call_command('createtestdata')
        self._assert_db_counts()
        management.call_command('createtestdata', messages="True")
        self._assert_db_counts()
        management.call_command('createtestdata', flagged=True)
        self._assert_db_counts()
        management.call_command('createtestdata', feedback=True)
        self._assert_db_counts()
        management.call_command('createtestdata', messages=True, flagged=True)
        self._assert_db_counts()
        management.call_command('createtestdata', messages=True, profiles=True, feedback=True, delete=True)
        self._assert_db_counts()


    def test_create_test_data_and_report_usage_command(self):
        """
        consolidating create test data, report and delete test as this is rather slow
        """
        management.call_command('createtestdata', registration=True, profiles=True, messages=True, flagged=True, feedback=True, verbosity=3)
        self._assert_db_counts(user=20, registration=10, message=30, flagged=15, feedback=10)
        management.call_command('reportusage')
        self.assertEqual(len(mail.outbox), 0)
        management.call_command('reportusage', hours=24*365)
        self.assertEqual(len(mail.outbox), 0)
        management.call_command('reportusage', email=True)
        self.assertEqual(len(mail.outbox), 1)
        email_text=str(mail.outbox[0].body)
        self.assertTrue('total new: 30' in email_text)
        # Generally tests results to make sure we are generating relevant stuff:
        self.assertTrue('Usage summary:' in email_text)
        self.assertTrue('site:' in email_text)
        self.assertTrue('date range:' in email_text)
        # Messages:
        self.assertTrue('Messages:' in email_text)
        self.assertTrue('total new: 30' in email_text)
        self.assertTrue('total expired: 10' in email_text)
        self.assertTrue('flagged count: 15' in email_text)
        self.assertTrue('suspicious count: 0' in email_text)
        # users - ignoring details as numbers are in flux because of our preconfigured users
        self.assertTrue('Users:' in email_text)
        # Registration:
        self.assertTrue('Registration:' in email_text)
        self.assertTrue('total open: 5' in email_text)
        self.assertTrue('total expired: 5' in email_text)
        # Feedback
        self.assertTrue('Feedback:' in email_text)
        self.assertTrue('total count: 10' in email_text)
        self.assertTrue('website count: 5' in email_text)
        self.assertTrue('application count: 5' in email_text)
        self.assertTrue('suspicious count: 0' in email_text)
        # Log
        self.assertTrue('Log (' in email_text)
        self.assertTrue('size:' in email_text)
        # clean up
        management.call_command('createtestdata', delete=True, verbosity=3)
        self._assert_db_counts()


    ''' Test render templates command '''
    def test_render_templates_command(self):
        self.assertEqual(len(mail.outbox), 0)
        management.call_command('rendertemplates', email='user@domain.com', verbosity=3)
        self.assertEqual(len(mail.outbox), 5)


    ''' Test benchmark command '''
    def test_benchmark_command_options_missing(self):
        buffer=StringIO(); self.setupStdErr(buffer)
        management.call_command('benchmark', verbosity=3)
        self.assertGreater(buffer.len, 0)

    def test_benchmark_command(self):
        management.call_command('benchmark', iterations=1, message_read=True, count=25, verbosity=3)
        # make sure we didn't leave anything behind
        self._assert_db_counts()


    ''' Internal Interface '''
    def _assert_db_counts(self, user=0, registration=0, message=0, feedback=0, flagged=0):
        user+=self._stock_user_count
        self.assertEqual(User.objects.count(), user)
        self.assertEqual(RegistrationProfile.objects.count(), registration)
        self.assertEqual(Message.objects.count(), message)
        self.assertEqual(Feedback.objects.count(), feedback)
        self.assertEqual(Flagged.objects.count(), flagged)
