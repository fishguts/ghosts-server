"""
created: 6/24/13
@author: curt
"""
from django.core import mail
from django.test import TestCase
from common.db import get_project_site

from ghosts import contact
from ghosts.tests._testfactories import GhostsTestFactories


class GhostsContactTests(TestCase):
    def test_email_activation(self):
        user=GhostsTestFactories.create_user_object()
        self.assertEqual(len(mail.outbox), 0)
        contact.send_activation_email(user, 'dummy_activation_key', get_project_site())
        self.assertEqual(len(mail.outbox), 1)

    def test_email_activated(self):
        user=GhostsTestFactories.create_user_object()
        self.assertEqual(len(mail.outbox), 0)
        contact.send_activated_email(user, get_project_site())
        self.assertEqual(len(mail.outbox), 1)

    def test_email_account_canceled(self):
        user=GhostsTestFactories.create_user_object()
        self.assertEqual(len(mail.outbox), 0)
        contact.send_account_canceled_email(user)
        self.assertEqual(len(mail.outbox), 1)

    def test_email_account_warning(self):
        user=GhostsTestFactories.create_user_object()
        self.assertEqual(len(mail.outbox), 0)
        contact.send_account_warning_email(user)
        self.assertEqual(len(mail.outbox), 1)

    def test_email_account_revoked(self):
        user=GhostsTestFactories.create_user_object()
        self.assertEqual(len(mail.outbox), 0)
        contact.send_account_revoked_email(user)
        self.assertEqual(len(mail.outbox), 1)
