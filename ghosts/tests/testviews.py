"""
created: 12/17/12
@author: curt
"""
import json
from datetime import datetime, timedelta

from django.conf import settings
from django.http import HttpResponseNotAllowed, HttpResponseBadRequest, HttpResponse
from django.test import Client
from django.contrib.auth import models as model_auth

from ghosts import constants
from ghosts import models as model_ghosts
from ghosts.serializers import core as serializer_core
from ghosts.tests._testcases import TestCaseGhosts
from ghosts.tests._testfactories import GhostsTestFactories
from common.constants import RESPONSE_CODE_AUTH, RESPONSE_CODE_OKAY, STATUS_OKAY


class GhostsViewTests(TestCaseGhosts):
    """
    Application view tests
    """
    def test_settings_get_invalid(self):
        self._test_request_method_get_not_allowed('/settings/get/json')
        self._test_unsupported_user_agent('/settings/get/json')

    def test_settings_get_valid(self):
        view=model_ghosts.ViewSettings.objects.get(name='radial')
        result=self._test_successful_post('/settings/get/json', '{}')
        result_data=json.loads(result.content)
        self.assertEqual(result_data['server']['version'], constants.SERVER_VERSION)
        self.assertEqual(len(result_data['settings']), 1)
        self._assert_setting_data_equals_settings(result_data['settings'][0], view)

    def test_message_get_invalid(self):
        self._test_request_method_get_not_allowed('/messages/get/json')
        self._test_unsupported_user_agent('/messages/get/json')
        self._test_no_post_data('/messages/get/json')
        self._test_invalid_post_data('/messages/get/json')

    def test_message_get_success(self):
        user=GhostsTestFactories.create_user_object('public_user')
        message=GhostsTestFactories.create_message_object(user)
        result=self._test_successful_post('/messages/get/json',
            '{"view":"radial","latitude":%f,"longitude":%f,"altitude":%f}'%(message.latitude, message.longitude, message.altitude))
        result_data=json.loads(result.content)
        self.assertEqual(len(result_data['profiles']), 1)
        self.assertEqual(len(result_data['messages']), 1)
        self._assert_user_data_equals_user(data=result_data['profiles'][0], user=user)
        self._assert_message_data_equals_message(data=result_data['messages'][0], message=message, user=user)

    def test_perimeter_get_invalid(self):
        self._test_request_method_get_not_allowed('/internal/admin/messages/get/json')
        self._test_unauthenticated_failure('/internal/admin/messages/get/json')
        self._test_not_staff_failure('/internal/admin/messages/get/json')
        self._test_no_post_data('/internal/admin/messages/get/json', login=True, staff=True)
        self._test_invalid_post_data('/internal/admin/messages/get/json', login=True, staff=True)

    def test_perimeter_get_minus_expiration_valid(self):
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user)
        result=self._test_successful_post('/internal/admin/messages/get/json',
            '{"latitude":%f,"longitude":%f,"radius":10}'%(message.latitude, message.longitude),
            login=True, staff=True,
            user_agent='Mozilla/1.0')
        result_data=json.loads(result.content)
        self.assertEqual(len(result_data['profiles']), 1)
        self.assertEqual(len(result_data['messages']), 1)
        self._assert_user_data_equals_user(data=result_data['profiles'][0], user=user)
        self._assert_message_data_equals_message(data=result_data['messages'][0], message=message, user=user)

    def test_perimeter_get_plus_expiration_valid(self):
        expiration_date=datetime(year=2000, month=1, day=1)
        expiration_text=serializer_core.datetime_to_string(expiration_date+timedelta(days=1))
        user=GhostsTestFactories.create_user_object()
        message=GhostsTestFactories.create_message_object(user, expires=expiration_date)
        result=self._test_successful_post('/internal/admin/messages/get/json',
            '{"latitude":%f,"longitude":%f,"radius":10,"expiration":"%s"}'%(message.latitude, message.longitude, expiration_text), login=True, staff=True)
        result_data=json.loads(result.content)
        self.assertEqual(len(result_data['profiles']), 0)
        self.assertEqual(len(result_data['messages']), 0)

    def test_message_post_invalid(self):
        self._test_request_method_get_not_allowed('/messages/put/json')
        self._test_unauthenticated_failure('/messages/put/json')
        self._test_unsupported_user_agent('/messages/put/json', login=True)
        self._test_no_post_data('/messages/put/json', login=True)
        self._test_invalid_post_data('/messages/put/json', login=True)

    def test_message_post_valid(self):
        result=self._test_successful_post('/messages/put/json',
            '{"text":"godzilla is a softy","latitude":10.0,"longitude":20.2,"altitude":30.3}', login=True)
        result_data=json.loads(result.content)
        self.assertEqual(len(result_data['profiles']), 1)
        self.assertEqual(len(result_data['messages']), 1)
        self.assertEqual(result_data['profiles'][0]['username'], '_private_user_')
        self.assertEqual(result_data['messages'][0]['text'], 'godzilla is a softy')
        self.assertEqual(result_data['messages'][0]['latitude'], 10.0)
        self.assertEqual(result_data['messages'][0]['longitude'], 20.2)
        self.assertEqual(result_data['messages'][0]['altitude'], 30.3)
        # do a thorough scrubbing....
        user=model_auth.User.objects.get(username='_private_user_')
        message=model_ghosts.Message.objects.get(pk=result_data['messages'][0]['id'])
        self._assert_user_data_equals_user(data=result_data['profiles'][0], user=user)
        self._assert_message_data_equals_message(data=result_data['messages'][0], message=message, user=user)

    def test_message_rate_invalid(self):
        self._test_request_method_get_not_allowed('/messages/rate/json')
        self._test_unauthenticated_failure('/messages/rate/json')
        self._test_unsupported_user_agent('/messages/rate/json', login=True)
        self._test_no_post_data('/messages/rate/json', login=True)
        self._test_invalid_post_data('/messages/rate/json', login=True)

    def test_message_rate_like_valid(self):
        user=GhostsTestFactories.create_user_object('public_user')
        message=GhostsTestFactories.create_message_object(user)
        result=self._test_successful_post('/messages/rate/json', data='{"messageId":%d,"rating":1}'%(message.id,), login=True)
        result_data=json.loads(result.content)
        self.assertEqual(len(result_data['profiles']), 1)
        self.assertEqual(len(result_data['messages']), 1)
        self._assert_user_data_equals_user(data=result_data['profiles'][0], user=user)
        self._assert_message_response_data_equals_params(
            data=result_data['messages'][0], message=message, user=user, rating_likes=1, rating_dislikes=0, rating_user=1, flagged_user=False)

    def test_message_rate_dislike_valid(self):
        user=GhostsTestFactories.create_user_object('public_user')
        message=GhostsTestFactories.create_message_object(user)
        result=self._test_successful_post('/messages/rate/json', data='{"messageId":%d,"rating":-1}'%(message.id,), login=True)
        result_data=json.loads(result.content)
        self.assertEqual(len(result_data['profiles']), 1)
        self.assertEqual(len(result_data['messages']), 1)
        self._assert_user_data_equals_user(data=result_data['profiles'][0], user=user)
        self._assert_message_response_data_equals_params(
            data=result_data['messages'][0], message=message, user=user, rating_likes=0, rating_dislikes=1, rating_user=-1, flagged_user=False)

    def test_message_flag_invalid(self):
        self._test_request_method_get_not_allowed('/messages/flag/json')
        self._test_unauthenticated_failure('/messages/flag/json')
        self._test_unsupported_user_agent('/messages/flag/json', login=True)
        self._test_no_post_data('/messages/flag/json', login=True)
        self._test_invalid_post_data('/messages/flag/json', login=True)

    def test_message_flag_valid(self):
        user=GhostsTestFactories.create_user_object('public_user')
        message=GhostsTestFactories.create_message_object(user)
        result=self._test_successful_post('/messages/flag/json', data='{"messageId":%d}'%(message.id,), login=True)
        result_data=json.loads(result.content)
        self.assertEqual(len(result_data['profiles']), 1)
        self.assertEqual(len(result_data['messages']), 1)
        self._assert_user_data_equals_user(data=result_data['profiles'][0], user=user)
        self._assert_message_response_data_equals_params(
            data=result_data['messages'][0], message=message, user=user, rating_likes=0, rating_dislikes=0, rating_user=None, flagged_user=True)

    def test_message_delete_invalid(self):
        self._test_request_method_get_not_allowed('/messages/delete/json')
        self._test_unauthenticated_failure('/messages/delete/json')
        self._test_unsupported_user_agent('/messages/delete/json', login=True)
        self._test_no_post_data('/messages/delete/json', login=True)
        self._test_invalid_post_data('/messages/delete/json', login=True)

    def test_message_delete_valid(self):
        user=GhostsTestFactories.create_user_object('public_user')
        message=GhostsTestFactories.create_message_object(user)
        self.assertEqual(model_ghosts.Message.objects.all().count(), 1)
        self._test_successful_post('/messages/delete/json', data='{"messageId":%d}'%(message.id,), login=True, user=user)
        self.assertEqual(model_ghosts.Message.objects.all().count(), 0)

    def test_profile_login_invalid(self):
        self._test_request_method_get_not_allowed('/profile/login/json')
        self._test_unsupported_user_agent('/profile/login/json', login=True)
        self._test_no_post_data('/profile/login/json', login=True)
        self._test_invalid_post_data('/profile/login/json', login=True)

    def test_profile_login_valid(self):
        user=GhostsTestFactories.create_user_object('public_user', password='password')
        result=self._test_successful_post('/profile/login/json', '{"username":"%s", "password":"password"}'%user.username)
        result_data=json.loads(result.content)
        # re-query user 'cause we are periodically getting last logged in discrepancies
        self._assert_user_data_equals_user(data=result_data['profile'], user=model_auth.User.objects.get(pk=user.pk))

    def test_profile_logout_invalid(self):
        self._test_request_method_get_not_allowed('/profile/logout/json')
        self._test_unauthenticated_failure('/profile/logout/json')
        self._test_unsupported_user_agent('/profile/logout/json', login=True)
        self._test_no_post_data('/profile/logout/json', login=True)
        self._test_invalid_post_data('/profile/logout/json', login=True)

    def test_profile_logout_valid(self):
        # we are letting _test_successful_post log us in and then we will log that same user out
        self._test_successful_post('/profile/logout/json', '{"username":"_private_user_"}', login=True)

    def test_profile_register_invalid(self):
        self._test_request_method_get_not_allowed('/profile/register/json')
        self._test_unsupported_user_agent('/profile/register/json')
        self._test_no_post_data('/profile/register/json')
        self._test_invalid_post_data('/profile/register/json', usesFormValidation=True)

    def test_profile_register_valid(self):
        result=self._test_successful_post('/profile/register/json', '{"username":"public_user", "password":"password", "email":"curt@burt.com"}')
        result_data=json.loads(result.content)
        # double check results to make sure username was properly written. Let our core test do the rest.
        self.assertEqual(result_data['profile']['username'], 'public_user')
        self._assert_user_data_equals_user(data=result_data['profile'], user=model_auth.User.objects.get(username='public_user'))

    def test_profile_update_invalid(self):
        self._test_request_method_get_not_allowed('/profile/update/json')
        self._test_unauthenticated_failure('/profile/update/json')
        self._test_unsupported_user_agent('/profile/update/json', login=True)

    def test_profile_update_valid(self):
        pass

    def test_profile_delete_invalid(self):
        self._test_request_method_get_not_allowed('/profile/delete/json')
        self._test_unauthenticated_failure('/profile/delete/json')
        self._test_unsupported_user_agent('/profile/delete/json', login=True)

    def test_profile_delete_valid(self):
        pass

    def test_support_feedback_invalid(self):
        self._test_request_method_get_not_allowed('/support/feedback/json')
        self._test_unauthenticated_failure('/support/feedback/json')
        self._test_unsupported_user_agent('/support/feedback/json', login=True)
        self._test_no_post_data('/support/feedback/json', login=True)

    def test_support_feedback_valid(self):
        self._test_invalid_post_data('/support/feedback/json', login=True)
        self._test_successful_post('/support/feedback/json', '{"text":"unicorn", "device":"ios|iPhone|5.1"}', login=True)

    def test_support_contact_invalid(self):
        self._test_request_method_get_not_allowed('/support/contact/form')
        self._test_invalid_post_data('/support/contact/form', usesFormValidation=True)

    def test_support_contact(self):
        self._test_successful_post('/support/contact/form', 'name=curt&email=c@b.com&message=bones', login=False)


    ''' private support '''
    def _log_client_in(self, client, user):
        setattr(settings, 'PASSWORD_HASHERS', ['django.contrib.auth.hashers.MD5PasswordHasher'])
        client.login(username=user.username, password='password')

    def _create_and_log_client_in(self, client, staff=False):
        if staff:
            user=GhostsTestFactories.create_staff_object(name='_private_user_', password='password')
        else:
            user=GhostsTestFactories.create_user_object(name='_private_user_', password='password')
        self._log_client_in(client=client, user=user)
        return user

    ''' private tests '''
    def _test_request_method_get_not_allowed(self, path):
        response=Client(HTTP_USER_AGENT='Goggles/1.0').get(path)
        self.assertEqual(response.status_code, HttpResponseNotAllowed.status_code)
        return response

    def _test_unauthenticated_failure(self, path):
        response=Client(HTTP_USER_AGENT='Goggles/1.0').post(path)
        response_data=json.loads(response.content)
        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response_data['status']['code'], RESPONSE_CODE_AUTH)
        return response

    def _test_not_staff_failure(self, path):
        client=Client(HTTP_USER_AGENT='Goggles/1.0')
        self._create_and_log_client_in(client)
        response=client.post(path)
        response_data=json.loads(response.content)
        self.assertEqual(response.status_code, HttpResponse.status_code)
        self.assertEqual(response_data['status']['code'], RESPONSE_CODE_AUTH)
        return response

    def _test_unsupported_user_agent(self, path, login=False):
        client=Client(HTTP_USER_AGENT='Mozilla/1.0')
        if login:
            self._create_and_log_client_in(client)
        response=client.post(path)
        self.assertEqual(response.status_code, HttpResponseBadRequest.status_code)
        return response

    def _test_no_post_data(self, path, login=False, staff=False):
        client=Client(HTTP_USER_AGENT='Goggles/1.0')
        if login:
            self._create_and_log_client_in(client, staff)
        response=client.post(path)
        self.assertEqual(response.status_code, HttpResponseBadRequest.status_code)
        return response

    def _test_invalid_post_data(self, path, login=False, staff=False, usesFormValidation=False):
        """
         Currently we have some form validation (registration).  It fails more gracefully.  Once
         everybody is on board we will be able to change this test to assume the more him.
        """
        client=Client(HTTP_USER_AGENT='Goggles/1.0')
        if login:
            self._create_and_log_client_in(client, staff)
        response=client.post(path, data='{"nonsense":"value"}', content_type='text/plain')
        if not usesFormValidation:
            self.assertEqual(response.status_code, HttpResponseBadRequest.status_code)
        else:
            response_data=json.loads(response.content)
            self.assertEqual(response.status_code, HttpResponse.status_code)
            self.assertTrue(len(response_data['status']['code'])>0)
            self.assertNotEqual(response_data['status']['code'], RESPONSE_CODE_OKAY)
        return response

    def _test_successful_post(self, path, data, login=False, staff=False, user=None, user_agent='Goggles/1.0'):
        client=Client(HTTP_USER_AGENT=user_agent)
        if login:
            if user is None:
                self._create_and_log_client_in(client, staff)
            else:
                self._log_client_in(client, user)
        response=client.post(path, data=data, content_type='text/plain')
        response_data=json.loads(response.content)
        self.assertEqual(response.status_code, HttpResponse.status_code)
        self._assert_status_data_equals_status(response_data['status'], STATUS_OKAY)
        return response
