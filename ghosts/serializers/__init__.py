"""
created: 8/16/13
@author: curt

Serializers are utilizing a little bit of a different naming strategy (because of collisions with core modules).
The idea is to use them as an interface such that they can be swapped in and out interchangeably. Method names
should not specify the encoding. Stick to to_[object] and from_[object]
Example: json:
    - to_user(json)
    - from_user(user)

"""

