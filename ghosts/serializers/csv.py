"""
created: 8/16/13
@author: curt
"""

csv_core = __import__("csv")
from common import builtins
from common.utils import dateutils, ArrayXT, StringXT
from ghosts import constants, factories
from ghosts.serializers import core as serializer_core


''' Public API '''
def to_messages(rows, header=False):
    """
    :param rows: iterable collection of rows
    :param header: is there a header to toss away?
    :return: list :raise: ValueError
    """
    result = []
    reader = csv_core.reader(rows)
    date = factories.create_timestamp()
    if header:
        reader.next()
    for row in reader:
        if len(row) < 6:
            raise ValueError(
                'Unable to parse csv to messages. Improper column format. Expecting: username, message, latitude, longitude, altitude, type [,created, expired]')
        try:
            coords = _parse_coordinates(row[2], row[3])
            data = {
                'ownerId': row[0],
                'text': row[1],
                'latitude': coords[0],
                'longitude': coords[1],
                'altitude': float(row[4]) if row[4] else 0,
                'type': constants.response_to_code_message(response=row[5].lower(), unknown_handler=builtins.fail_to_value_error)
            }
            column = ArrayXT.safe_value(row, 6)
            if column:
                data['created'] = dateutils.text_to_date(column, dfault=date)
            column = ArrayXT.safe_value(row, 7)
            if column:
                data['expiration'] = serializer_core.expiration_code_to_duration(column,
                                                                                 unknown_handler=builtins.fail_to_value_error)
            result.append(data)
        except Exception as exception:
            # if the whole row is empty then let it go
            if any(row):
                raise ValueError("Problem parsing '{0}': {1}".format(StringXT.ellipsis(row[1], 64), exception))
    return result


''' Private '''
def _parse_coordinates(col1, col2):
    try:
        col1, col2 = col1.split(',')
    except:
        try:
            col1, col2 = col2.split(',')
        except:
            pass
    return float(col1), float(col2)


