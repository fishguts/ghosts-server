"""
created: 8/16/13
@author: curt

Intermediate representations of our model objects to and from which things are serialized
"""

from django.core.exceptions import ObjectDoesNotExist
from ghosts import constants
from ghosts.serializers import core as serializer_core


def status_to_map(status):
    return {'code':status.code, 'text':status.text}


def message_to_map(message, current_user, latitude=None, longitude=None, altitude=None):
    """
    @message - source
    @current_user - for feedback
    @longitude - used for coordinate-less messages that follow people (welcome)
    @latitude - used for coordinate-less messages that follow people (welcome)
    @altitude - used for coordinate-less messages that follow people (welcome)
    """
    result={}
    result['id']=message.id
    result['ownerId']=message.user.username
    result['text']=message.text
    if message.image_thumb:
        result['imageUrlThumb']=message.image_thumb.url
    if message.image_full:
        result['imageUrlFull']=message.image_full.url
    if message.latitude is not None:
        result['latitude']=message.latitude
    else:
        result['latitude']=latitude
    if message.longitude is not None:
        result['longitude']=message.longitude
    else:
        result['longitude']=longitude
    if message.altitude is not None:
        result['altitude']=message.altitude
    else:
        result['altitude']=altitude
    result['created']=serializer_core.datetime_to_string(message.created)
    if message.expiration:
        result['expiration']=serializer_core.datetime_to_string(message.expiration)
    result['type']=constants.code_to_response_message(message.type)

    # feedback - only allow feedback on user messages
    if message.type!=constants.MESSAGE_TYPE_USER[0]:
        result['supportsFeedback']=False
        result['ratingLikes']=0
        result['ratingDislikes']=0
        result['flaggedUser']=False
    else:
        ratings=message.rating_set.all()
        result['supportsFeedback']=True
        result['ratingLikes']=len(filter(lambda rating:rating.value>0, ratings))
        result['ratingDislikes']=ratings.count()-result['ratingLikes']
        if current_user.is_authenticated():
            try:
                result['ratingUser']=ratings.get(user=current_user).value
            except ObjectDoesNotExist:
                pass

            # flagged
            try:
                message.flagged_set.get(message=message, user=current_user)
                result['flaggedUser']=True
            except ObjectDoesNotExist:
                result['flaggedUser']=False

    return result


def user_to_map(user):
    result={}
    result['username']=user.username
    result['created']=serializer_core.datetime_to_string(user.date_joined)
    # note: email should never ever go client side
    result['last_login']=serializer_core.datetime_to_string(user.last_login)
    try:
        profile=user.get_profile()
        if profile.latitude:
            result['latitude']=profile.latitude
        if profile.longitude:
            result['longitude']=profile.longitude
        if profile.altitude:
            result['altitude']=profile.altitude
        if profile.image_thumb:
            result['imageUrlThumb']=profile.image_thumb.url
        if user.image_full:
            result['imageUrlFull']=profile.image_full.url
    except ObjectDoesNotExist:
        # no extended information - it's okay
        pass
    return result


def setting_to_map(setting):
    result={}
    result['name']=setting.name
    result['radiusHorizontal']=setting.radius_horizontal
    result['radiusVertical']=setting.radius_vertical
    result['queryIntervalMin']=setting.min_query_interval
    result['queryIntervalMax']=setting.max_query_interval
    result['messagesPerQueryMax']=setting.max_messages_per_query
    return result


