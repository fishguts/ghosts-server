"""
Created on Jul 25, 2012

@author: curt

See notes on maintaining an "interface" in __init__.py

"""

json_core=__import__('json')

from common import builtins
from ghosts import constants
from ghosts.serializers import model as serializer_model


''' JSON to internal object representation decoders '''
def to_object(json_text):
    """
    General purpose parser that will catch exceptions and rethrow them with some info.
    Intended for requests that will be further validated by forms.
    """
    try:
        return json_core.loads(json_text)
    except Exception:
        raise ValueError('Failed to parse JSON: {0}'.format(json_text))


''' object to JSON encoders '''
def from_messages(messages, current_user, status, latitude=None, longitude=None, altitude=None):
    """
    @messages - source
    @current_user - logged in guy if logged in
    @longitude - used for messages that follow people (welcome)
    @latitude - used for messages that follow people (welcome)
    @altitude - used for messages that follow people (welcome)
    """
    users=builtins.unique([message.user for message in messages])
    mapped_status=serializer_model.status_to_map(status)
    mapped_messages=[serializer_model.message_to_map(message, current_user, latitude, longitude, altitude) for message in messages]
    mapped_users=[serializer_model.user_to_map(user) for user in users]
    return json_core.dumps({'status':mapped_status, 'profiles':mapped_users, 'messages':mapped_messages})


def from_message(message, current_user, status, latitude=None, longitude=None, altitude=None):
    # to be consistent we map this guy the same as with a sequence of messages
    return from_messages([message], current_user, status, latitude, longitude, altitude)


def from_user(user, status):
    return json_core.dumps({'status':serializer_model.status_to_map(status), 'profile':serializer_model.user_to_map(user)})


def from_status(status):
    mapped_status=serializer_model.status_to_map(status)
    return json_core.dumps({'status':mapped_status})


def from_setting(setting, status):
    return from_settings([setting], status)


def from_settings(settings, status):
    mapped_status=serializer_model.status_to_map(status)
    mapped_settings=[serializer_model.setting_to_map(setting) for setting in settings]
    return json_core.dumps({'status':mapped_status, 'server':{'version':constants.SERVER_VERSION}, 'settings':mapped_settings})
