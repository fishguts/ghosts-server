"""
created: 8/16/13
@author: curt

Serialization common to all of our more specific encodings
"""
from datetime import datetime
from common import builtins


def datetime_to_string(date):
    # get rid of microseconds as it makes the format variable HH:MM:SS.micros or HH:MM:SS if none.
    return date.replace(microsecond=0).isoformat()

def string_to_datetime(text):
    return datetime.strptime(text, '%Y-%m-%dT%H:%M:%S')


def code_to_response(code, items, unknown_handler=builtins.fail_to_warn):
    """
    Looks among items to find a pair with the code 'code'
    :param code: to find
    :param items: to search through: assumes item[0] is code and item[1] is response
    :param unknown_handler: function to handle not found
    :return: response
    """
    for item in items:
        if code==item[0]:
            return item[1]
    unknown_handler("Unknown code '{0}'".format(code), module=__name__)

def response_to_code(response, items, unknown_handler=builtins.fail_to_warn):
    """
    Looks among items to find a pair with the code 'code'
    :param response: to find
    :param items: to search through: assumes item[0] is code and item[1] is response
    :param unknown_handler: function to handle not found
    :return: code
    """
    for item in items:
        if response==item[1]:
            return item[0]
    unknown_handler(text="Unknown response '{0}'".format(response), module=__name__)


def expiration_code_to_duration(code, unknown_handler=builtins.fail_to_warn):
    if code=='hour':
        return 60*60
    elif code=='day':
        return 24*60*60
    elif code=='month':
        return 31*24*60*60
    elif code=='year':
        return 365*24*60*60
    unknown_handler(text="Unknown expiration '{0}'".format(code), module=__name__)
