"""
created: 2/9/13
@author: curt
"""
from django.conf import settings
from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(min_length=getattr(settings, 'CONTACT_NAME_MIN_LENGTH', 2),
                           max_length=getattr(settings, 'CONTACT_NAME_MAX_LENGTH', 50))
    email = forms.EmailField(max_length=getattr(settings, 'EMAIL_MAX_LENGTH', 50))
    message = forms.CharField(min_length=getattr(settings, 'FEEDBACK_MIN_LENGTH', 4),
                              max_length=getattr(settings, 'FEEDBACK_MAX_LENGTH', 1250))
