"""
created: 2/2/13
@author: curt
"""
import re
from ghosts.models import Reserved


def username_contains_flagged_content(username):
    reserved_rows=Reserved.objects.all()
    for reserved_row in reserved_rows:
        match=re.search('(^|[-.@+_])'+reserved_row.word+'($|[-.@+_])', username, re.IGNORECASE)
        if match is not None:
            return True
    return False

def text_contains_flagged_content(text):
    reserved_rows=Reserved.objects.all()
    for reserved_row in reserved_rows:
        match=re.search('(^|[^a-zA-Z])'+reserved_row.word+'($|[^a-zA-Z])', text, re.IGNORECASE)
        if match is not None:
            return True
    return False

