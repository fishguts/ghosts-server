"""
Created on Jul 25, 2012

@author: curt
"""
import math

from django.db.models import Q

from ghosts import constants, factories
import ghosts.models as model_ghosts


''' Constants '''
WORLD_CIRCUMFERENCE_METERS=40075.017*1000


''' Public interface '''
def get_request_messages(data):
    """
    Builds a QuerySet matching the params in data.

    :param data: expects properties 'view', 'latitude', 'longitude' and 'altitude'
    :return: QuerySet
    """
    view_settings=model_ghosts.ViewSettings.objects.get(name=data['view'])
    query_set=get_location_messages(view_settings=view_settings, latitude=data['latitude'], longitude=data['longitude'], altitude=data['altitude'])
    if 'welcome' in data:
        query_set=query_set | get_typed_messages(message_type=constants.MESSAGE_TYPE_WELCOME[0])
    return query_set


def get_perimeter_messages(data):
    """
    Builds a QuerySet matching the params in data. As you can see there is no view.
    View is optional. We use a predefined view named 'admin' to get all params that are not overridden in data.

    :param data: expects properties 'latitude', 'longitude', 'radius' and [optional] 'altitude'
    :return: QuerySet
    """
    altitude=data.get('altitude', 0)
    expiration=data.get('expiration', None)
    view=model_ghosts.ViewSettings.objects.get(name='_admin')
    view.radius_horizontal=data['radius']
    query_set=get_location_messages(view_settings=view, latitude=data['latitude'], longitude=data['longitude'], altitude=altitude, expiration_date=expiration)
    return query_set


def get_location_messages(view_settings, latitude, longitude, altitude, expiration_date=None):
    """
    Finds all messages that fall into the perimeter rules for view_settings
    """
    if not expiration_date:
        expiration_date=factories.create_timestamp()
    latitude_radius=_get_latitude_radius(view_settings)
    longitude_radius=_get_longitude_radius(view_settings, latitude)

    # perimeter testing is within a rectangular grid
    altitude_from=altitude-view_settings.radius_vertical
    altitude_to=altitude+view_settings.radius_vertical
    latitude_from=latitude-latitude_radius
    latitude_to=latitude+latitude_radius
    longitude_from=longitude-longitude_radius
    longitude_to=longitude+longitude_radius

    # build a query set of all objects.  We will add filters (filters are and'ed together by default)
    query_set=model_ghosts.Message.objects.filter(status=constants.MODEL_STATUS_OKAY)

    # altitude
    query_set=query_set.filter(altitude__gte=altitude_from)
    query_set=query_set.filter(altitude__lte=altitude_to)

    # latitude - it's theoretically impossible that they would be seeking beyond the polar caps
    # (<-90 or >90) but technically it's very possible and that's okay and we allow for it.
    query_set=query_set.filter(latitude__gte=latitude_from)
    query_set=query_set.filter(latitude__lte=latitude_to)

    # longitude
    if longitude_from>=-180:
        if longitude_to<=180:
            query_set=query_set.filter(longitude__gte=longitude_from)
            query_set=query_set.filter(longitude__lte=longitude_to)
        else:
            query_set=query_set.filter(Q(longitude__gte=longitude_from) | Q(longitude__lte=longitude_to-360))
    else:
        query_set=query_set.filter(Q(longitude__gt=360+longitude_from) | Q(longitude__lte=longitude_to))

    # expiration
    query_set=query_set.filter(Q(expiration__isnull=True) | Q(expiration__gt=expiration_date))

    # print query_set.query
    return query_set


def get_typed_messages(message_type, expiration_date=None):
    """
    Returns messages with a type match.  Does not filter by location.
    """
    if not expiration_date:
        expiration_date=factories.create_timestamp()
    query_set=model_ghosts.Message.objects.filter(type=message_type)
    query_set=query_set.filter(Q(expiration__isnull=True) | Q(expiration__gt=expiration_date))
    return query_set


def get_typed_location_messages(message_type, latitude, longitude, altitude, view_settings, expiration_date=None):
    """
    Finds all messages of specific type that fall into the perimeter rules for view_settings
    """
    query_set=get_location_messages(view_settings=view_settings, latitude=latitude, longitude=longitude, altitude=altitude, expiration_date=expiration_date)
    query_set=query_set.filter(type=message_type)
    return query_set


def get_expired_messages(expiration_date=None):
    if not expiration_date:
        expiration_date=factories.create_timestamp()
    query_set=model_ghosts.Message.objects.filter(Q(expiration__isnull=False) & Q(expiration__lte=expiration_date))
    return query_set



''' Internal methods '''
def _get_latitude_radius(view_settings):
    # fraction of a degree: 360*(view_settings.radius/WORLD_CIRCUMFERENCE_METERS)
    return (360*view_settings.radius_horizontal)/WORLD_CIRCUMFERENCE_METERS

def _get_longitude_radius(view_settings, latitude):
    # keep our cosine calculation from going to 0 for our polar pals
    if latitude<-89.9:
        latitude=-89.9
    elif latitude>89.9:
        latitude=89.9
    latitude_circumference=math.cos(math.radians(latitude))*WORLD_CIRCUMFERENCE_METERS
    # fraction of a degree: 360*(view_settings.radius/latitude_circumference)
    return (360*view_settings.radius_horizontal)/latitude_circumference
